export type FieldLabels<T> = { [key in keyof T]: string }

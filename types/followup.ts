export enum FollowupPeriodFilter {
  ThisYear = 'ThisYear',
  LastYear = 'LastYear',
  ThisMonth = 'ThisMonth',
  LastMonth = 'LastMonth',
  Custom = 'Custom'
}

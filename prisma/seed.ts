/* eslint-disable no-console */
import { PrismaClient } from '@prisma/client'
import {
  fixtureStructures,
  fixturesPartnerOrganizations,
  fixturesPartnerOrganizationContacts,
  fixturesSocialSupportSubjects,
  fixturesUsers,
  fixturesCGUHistory,
  fixturesFamilyFiles,
  fixturesBeneficiaries,
  fixturesBeneficiaryRelatives,
  fixturesSocialSupports,
  fixturesBudgets,
  fixturesBudgetIncomes,
  fixturesBudgetExpenses
} from './fixtures'

const prismaClient = new PrismaClient()

function loadFixtureStructure() {
  console.log('🏛 Structure...')

  return prismaClient.structure.createMany({
    data: fixtureStructures,
    skipDuplicates: true
  })
}

function loadFixturesSocialSupportSubjects() {
  console.log('🔷 Social support subjects...')

  return prismaClient.socialSupportSubject.createMany({
    data: fixturesSocialSupportSubjects,
    skipDuplicates: true
  })
}

function loadFixturesUsers() {
  console.log('👤 Users...')

  return prismaClient.user.createMany({
    data: fixturesUsers,
    skipDuplicates: true
  })
}

function loadFixturesCGUHistory() {
  console.log('👩‍⚖️ CGU History for users...')

  return prismaClient.cGUHistory.createMany({
    data: fixturesCGUHistory,
    skipDuplicates: true
  })
}

function loadFixturesFamilyFiles() {
  console.log('👨‍👩‍👧‍👧 Family Files...')

  // Use upsert instead of createMany as createMany does not allow *-to-many relations to be initialized
  return fixturesFamilyFiles.map(({ id, ...data }) => {
    return prismaClient.familyFile.upsert({
      create: { id, ...data },
      where: { id },
      update: {}
    })
  })
}

function loadFixturesBeneficiaries() {
  console.log('👥 Beneficiaries...')

  // Use upsert instead of createMany as createMany does not allow *-to-many relations to be initialized
  return fixturesBeneficiaries.map(({ id, ...data }) => {
    return prismaClient.beneficiary.upsert({
      create: { id, ...data },
      where: { id },
      update: {}
    })
  })
}

function loadFixturesSocialSupports() {
  console.log('🤝 Social supports...')

  // Use upsert instead of createMany as createMany does not allow *-to-many relations to be initialized
  return fixturesSocialSupports.map(({ id, ...data }) => {
    return prismaClient.socialSupport.upsert({
      create: { id, ...data },
      where: { id },
      update: {}
    })
  })
}

function loadFixturesBeneficiaryRelatives() {
  console.log('🫂 Beneficiary Relatives...')

  return prismaClient.beneficiaryRelative.createMany({
    data: fixturesBeneficiaryRelatives,
    skipDuplicates: true
  })
}

function loadFixturesBudgets() {
  console.log('📊 Budgets...')

  return prismaClient.budget.createMany({
    data: fixturesBudgets,
    skipDuplicates: true
  })
}

function loadFixturesBudgetIncomes() {
  console.log('💰 Budget Incomes...')

  // Use upsert instead of createMany as createMany does not allow *-to-many relations to be initialized
  return fixturesBudgetIncomes.map(item => {
    return prismaClient.budgetIncome.upsert({
      create: item,
      where: { id: item.id },
      update: {}
    })
  })
}

function loadFixturesBudgetExpenses() {
  console.log('💸 Budget Expenses...')

  // Use upsert instead of createMany as createMany does not allow *-to-many relations to be initialized
  return fixturesBudgetExpenses.map(item => {
    return prismaClient.budgetExpenses.upsert({
      create: item,
      where: { id: item.id },
      update: {}
    })
  })
}

function loadFixturesPartnerOrganizations() {
  console.log('🏙️ Partner Organizations...')

  return prismaClient.partnerOrganization.createMany({
    data: fixturesPartnerOrganizations,
    skipDuplicates: true
  })
}

function loadFixturesPartnerOrganizationContacts() {
  console.log('🕴️ Partner Organization Contacts...')

  return prismaClient.partnerOrganizationContact.createMany({
    data: fixturesPartnerOrganizationContacts,
    skipDuplicates: true
  })
}

function loadFixtures() {
  return [
    loadFixtureStructure(),
    loadFixturesSocialSupportSubjects(),
    loadFixturesUsers(),
    loadFixturesCGUHistory(),
    ...loadFixturesFamilyFiles(),
    ...loadFixturesBeneficiaries(),
    loadFixturesPartnerOrganizations(),
    loadFixturesPartnerOrganizationContacts(),
    ...loadFixturesSocialSupports(),
    loadFixturesBeneficiaryRelatives(),
    loadFixturesBudgets(),
    ...loadFixturesBudgetIncomes(),
    ...loadFixturesBudgetExpenses()
  ]
}

async function main() {
  console.log('⏱️ Loading fixtures...')

  await prismaClient.$transaction(loadFixtures())

  console.log('👍 Done!')
}

main()
  .then(async () => {
    await prismaClient.$disconnect()
  })
  .catch(async e => {
    console.error(e)

    await prismaClient.$disconnect()

    process.exit(1)
  })

// https://github.com/prisma/prisma/issues/12504#issuecomment-1372635653
import { createRequire } from 'module'
import { MAX_UPLOAD_SIZE_BYTES } from './utils/constants'

const prismaClientPath = createRequire(import.meta.url).resolve(
  '@prisma/client'
)

const connectSrcCsp = [
  "'self'",
  'https://api-adresse.data.gouv.fr',
  'https://geo.api.gouv.fr',
  // FIXME: Use runtime config once it is allowed by nuxt-security - See https://github.com/Baroshem/nuxt-security/pull/233
  'https://matomo.dev.incubateur.anct.gouv.fr',
  'https://matomo.incubateur.anct.gouv.fr',
  'https://sentry.incubateur.net',
  'https://conversations-widget.brevo.com/'
]

export default defineNuxtConfig({
  // https://nuxt.com/blog/v3-8
  typescript: {
    tsConfig: {
      compilerOptions: {
        verbatimModuleSyntax: false
      }
    }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // DSFR STYLES
    '@gouvfr/dsfr/dist/core/core.min.css',
    '@gouvfr/dsfr/dist/dsfr.print.css',
    '@gouvfr/dsfr/dist/component/accordion/accordion.min.css',
    '@gouvfr/dsfr/dist/component/alert/alert.min.css',
    '@gouvfr/dsfr/dist/component/badge/badge.min.css',
    '@gouvfr/dsfr/dist/component/breadcrumb/breadcrumb.min.css',
    '@gouvfr/dsfr/dist/component/button/button.min.css',
    '@gouvfr/dsfr/dist/component/callout/callout.min.css',
    '@gouvfr/dsfr/dist/component/card/card.min.css',
    '@gouvfr/dsfr/dist/component/checkbox/checkbox.min.css',
    '@gouvfr/dsfr/dist/component/content/content.min.css',
    '@gouvfr/dsfr/dist/component/download/download.min.css',
    '@gouvfr/dsfr/dist/component/footer/footer.min.css',
    '@gouvfr/dsfr/dist/component/form/form.min.css',
    '@gouvfr/dsfr/dist/component/header/header.min.css',
    '@gouvfr/dsfr/dist/component/input/input.min.css',
    '@gouvfr/dsfr/dist/component/link/link.min.css',
    '@gouvfr/dsfr/dist/component/logo/logo.min.css',
    '@gouvfr/dsfr/dist/component/modal/modal.min.css',
    '@gouvfr/dsfr/dist/component/navigation/navigation.min.css',
    '@gouvfr/dsfr/dist/component/pagination/pagination.min.css',
    '@gouvfr/dsfr/dist/component/quote/quote.min.css',
    '@gouvfr/dsfr/dist/component/radio/radio.min.css',
    '@gouvfr/dsfr/dist/component/search/search.min.css',
    '@gouvfr/dsfr/dist/component/select/select.min.css',
    '@gouvfr/dsfr/dist/component/sidemenu/sidemenu.min.css',
    '@gouvfr/dsfr/dist/component/tab/tab.min.css',
    '@gouvfr/dsfr/dist/component/table/table.min.css',
    '@gouvfr/dsfr/dist/component/tag/tag.min.css',
    '@gouvfr/dsfr/dist/component/toggle/toggle.min.css',
    '@gouvfr/dsfr/dist/component/tooltip/tooltip.min.css',
    '@gouvfr/dsfr/dist/component/notice/notice.min.css',
    '@gouvfr/dsfr/dist/component/segmented/segmented.min.css',

    // DSFR ICONS
    '@gouvfr/dsfr/dist/utility/icons/icons-buildings/icons-buildings.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-business/icons-business.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-communication/icons-communication.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-design/icons-design.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-document/icons-document.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-media/icons-media.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-system/icons-system.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-user/icons-user.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-map/icons-map.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-device/icons-device.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-finance/icons-finance.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-weather/icons-weather.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-others/icons-others.min.css',

    // GLOBAL STYLES
    '@/assets/style/main.scss',
    '@/assets/style/button.scss',
    '@/assets/style/icons.css',
    '@/assets/style/tab.scss',
    '@/assets/style/stats.scss',
    '@/assets/style/form.scss',
    '@/assets/style/menu.scss'
  ],

  // Modules: https://v3.nuxtjs.org/api/configuration/nuxt.config#modules
  modules: [
    '@vee-validate/nuxt',
    'nuxt-scheduler',
    'nuxt-security',
    'vue-dsfr-nuxt-module',
    '@nuxt/eslint'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  vite: {
    server: {
      hmr: {
        // Fix websocket errors cf. https://github.com/nuxt/nuxt/discussions/27779
        clientPort: 3000
      }
    },
    // Adds sourcemaps for Sentry (may not work)
    build: {
      sourcemap: true
    },
    // https://github.com/nuxt/nuxt/issues/24196
    optimizeDeps: {
      include: [
        '@prisma/client',
        'vee-validate',
        '@vee-validate/zod',
        'zod',
        'vue-scrollto',
        'node-fetch-native',
        'uuid',
        '@trpc/client',
        '@gouvminint/vue-dsfr',
        'html-to-image',
        '@zip.js/zip.js',
        '@tiptap/vue-3',
        '@tiptap/starter-kit',
        '@tiptap/extension-link',
        '@tiptap/pm/model',
        '@braintree/sanitize-url'
      ]
    },
    css: {
      preprocessorOptions: {
        scss: {
          includePaths: ['./node_modules', './node_modules/@gouvfr/dsfr'],
          // Note: will be migrated to modern-compiled with sass-embedded when dsfr will be migrated to sass 2.0
          // https://vite.dev/config/shared-options.html#css-preprocessoroptions
          api: 'legacy',
          silenceDeprecations: ['legacy-js-api'],
          // Note: The syntax in sass 2.0 will have to be migrated in dsfr. Warnings are suppressed in the mean time.
          // See https://sass-lang.com/documentation/breaking-changes/import/
          quietDeps: true
        }
      }
    },
    resolve: {
      alias: {
        '.prisma/client/index-browser': prismaClientPath.replace(
          '@prisma/client/default.js',
          '.prisma/client/index-browser.js'
        )
      }
    }
  },

  build: {
    transpile: ['trpc-nuxt']
  },

  runtimeConfig: {
    auth: {
      jwtKey: process.env.AUTH_JWT_KEY || 'secretJWT',
      sessionSecret:
        process.env.AUTH_SESSION_SECRET || 'sessionSecretsessionSecretsession',
      proconnectIssuer: process.env.AUTH_PROCONNECT_ISSUER_URL,
      proconnectClientId: process.env.AUTH_PROCONNECT_CLIENT_ID,
      proconnectClientSecret: process.env.AUTH_PROCONNECT_CLIENT_SECRET,
      proconnectProvider: 'ProConnect',
      proconnectLabel: 'proconnect'
    },
    rdvsp: {
      issuer: process.env.RDVSP_ISSUER || '',
      clientId: process.env.RDVSP_CLIENT_ID || '',
      clientSecret: process.env.RDVSP_CLIENT_SECRET || ''
    },
    brevo: {
      apiKey: process.env.BREVO_API_KEY || ''
    },
    email: {
      smtpHost: process.env.EMAIL_SMTP_HOST || '',
      smtpPassword: process.env.EMAIL_SMTP_PASSWORD || '',
      smtpPort: parseInt(process.env.EMAIL_SMTP_PORT || '587'),
      smtpSecure: (process.env.EMAIL_SMTP_SECURE || 'false') === 'true',
      smtpUser: process.env.EMAIL_SMTP_USER || ''
    },
    public: {
      version: {
        commitSha: process.env.COMMIT_SHA || '',
        commitRefName: process.env.COMMIT_REF_NAME || '',
        commitTag: process.env.COMMIT_TAG || ''
      },
      cguVersion: '1.1',
      appUrl: process.env.APP_URL,
      auth: {
        cookieKey: 'MSS_SESSION_TOKEN',
        tokenValidationInMinutes: parseInt(
          process.env.TOKEN_VALIDATION_IN_MINUTES || '30'
        )
      },
      disconnect: {
        threshold: parseInt(process.env.DISCONNECT_THRESHOLD || '30'),
        alertThreshold: parseInt(process.env.DISCONNECT_ALERT_THRESHOLD || '5')
      },

      matomo: {
        host: process.env.MATOMO_HOST,
        siteId: parseInt(process.env.MATOMO_SITE_ID || '1')
      },
      mssEnvironment: process.env.MSS_ENVIRONMENT || 'local',
      email: {
        from: process.env.EMAIL_FROM || ''
      },
      sentry: {
        dsn: process.env.SENTRY_DSN,
        debug: !!process.env.SENTRY_DEBUG,
        tracesSampleRate: parseFloat(
          process.env.SENTRY_TRACE_SAMPLE_RATE || '0.1'
        )
      },
      metabase: {
        publicUrl: process.env.METABASE_PUBLIC_URL
      }
    }
  },

  // Prevent nuxt to export types.ts files as components
  components: [
    {
      path: '~/components',
      pathPrefix: false,
      extensions: ['.vue']
    }
  ],

  // Adds sourcemaps for Sentry (may not been working until releases and auth token are used)
  sourcemap: {
    client: true,
    server: true
  },

  security: {
    removeLoggers: false,
    headers: {
      contentSecurityPolicy:
        process.env.NODE_ENV === 'production'
          ? { 'connect-src': connectSrcCsp }
          : false,
      crossOriginEmbedderPolicy: false
    },
    rateLimiter: { tokensPerInterval: 600, interval: 300000 },
    requestSizeLimiter: {
      // 1M
      maxRequestSizeInBytes: 1048576,
      // 20M
      maxUploadFileRequestInBytes: MAX_UPLOAD_SIZE_BYTES,
      throwError: false
    }
  },

  routeRules: {
    '/api/document/upload': {
      security: {
        xssValidator: false
      }
    }
  },

  compatibilityDate: '2024-08-30'
})

export const structureFieldLabels = {
  name: 'Raison sociale',
  zipcode: 'Code postal',
  city: 'Ville',
  address: 'Adresse',
  phone: 'Téléphone',
  email: 'Email',
  type: 'Type',
  inhabitantsNumber: "Nombre d'habitants",
  inseeCode: 'Code INSEE',
  siret: 'SIRET',
  dpoFirstName: 'Prénom DPO',
  dpoLastName: 'Nom DPO',
  dpoEmail: 'Email DPO'
}

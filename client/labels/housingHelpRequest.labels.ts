export const housingHelpRequestFieldLabels = {
  firstOpeningDate: 'Date de la première demande',
  askedHousing: 'Logement demandé',
  reason: 'Motif de la demande de logement',
  uniqueId: 'Numéro unique de suivi',
  taxHouseholdAdditionalInformation: 'Précisions sur la composition du foyer',
  maxRent: 'Montant maximum du loyer',
  maxCharges: 'Montant maximum des charges',
  housingType: 'Typologie du logement',
  lowIncomeHousingType: 'Logement à loyer modéré',
  roomCount: 'Nombre de pièces',
  isPmr: "Besoin d'un logement PMR",
  instructorOrganizationContactId: "Contact de l'organisme instructeur"
}

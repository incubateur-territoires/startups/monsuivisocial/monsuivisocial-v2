const relativeFieldLabels = {
  id: 'Identifiant',
  lastName: 'Nom',
  firstName: 'Prénom',
  phone: 'Téléphone',
  email: 'Email',
  birthDate: 'Date de naissance',
  relationship: 'Lien de la relation',
  hosted: 'Vit au domicile du bénéficiaire',
  caregiver: 'Aidant familial',
  additionalInformation: 'Informations complémentaires',
  city: 'Ville'
}

export { relativeFieldLabels }

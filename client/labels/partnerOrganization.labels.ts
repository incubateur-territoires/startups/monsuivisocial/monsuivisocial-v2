export const partnerOrganizationFieldLabels = {
  name: 'Nom ou raison sociale',
  siret: 'Numéro de SIRET',
  address: 'Adresse',
  city: 'Ville ou commune',
  zipcode: 'Code postal',
  phone1: 'Numéro de téléphone',
  phone2: 'Numéro de téléphone 2',
  email: 'Email',
  workingHours: 'Horaires de permanence',
  contactName: 'Nom',
  contactPhone: 'Numéro de téléphone',
  contactEmail: 'Email'
}

export const partnerOrganizationContactFieldLabels = {
  name: 'Nom',
  phone: 'Numéro de téléphone',
  email: 'Email'
}

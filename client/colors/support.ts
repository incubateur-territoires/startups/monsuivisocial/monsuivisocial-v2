export enum SupportColor {
  helpRequests = 'var(--color-help-request)',
  followups = 'var(--color-followup)'
}

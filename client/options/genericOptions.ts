import { labelsToOptions } from '~/utils/options'

export const yesNoOptions = labelsToOptions({
  true: 'Oui',
  false: 'Non'
})

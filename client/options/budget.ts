import { buildKeys } from '~/utils/keys/keyBuilder'

export const budgetExpensesKeys = buildKeys({
  rent: 'Loyer',
  expenses: 'Charges',
  water: 'Eau',
  electricity: 'Électricité',
  gas: 'Gaz',
  fuelOrOther: 'Fuel (ou autre mode de chauffage)',

  // Impôts
  housingTax: "Taxe d'habitation",
  propertyTax: 'Taxe foncière',
  incomeTax: 'Impôt sur le revenu',

  // Assurances
  carInsurance: 'Assurance voiture',
  homeInsurance: 'Assurance habitation',
  deathInsurance: 'Assurance décès',
  retirementInsurance: 'Assurance veillesse',
  otherInsurance: 'Autres assurances',

  // Autres charges
  remittedAlimony: 'Pension alimentaire versée',
  healthInsurance: 'Mutuelle',
  phoneInternet: 'Téléphone/internet',
  schoolFees: 'Frais de garde ou de scolarité',
  schoolCanteen: 'Cantine',
  other: 'Autres charges et frais divers'
})

export const budgetLoansKeys = buildKeys({
  type: 'Type de crédit',
  amountTotal: 'Montant total',
  amountPerMonth: 'Montant mensuel',
  dueDate: 'Échéance du crédit'
})

export const budgetDebtsKeys = buildKeys({
  type: 'Type de dette',
  amountTotal: 'Montant total',
  amountPerMonth: 'Montant mensuel',
  dueDate: 'Échéance de la dette'
})

export const budgetCustomExpensesKeys = buildKeys({
  type: 'Type de dépense',
  amount: 'Montant'
})
export const budgetCustomIncomeKeys = buildKeys({
  type: 'Type de ressource',
  amount: 'Montant'
})

export const budgetIncomeKeys = buildKeys({
  // Travail
  salary: 'Salaire',
  primeActivite: "Prime d'activité",

  // Pensions
  retirement: 'Retraite',
  alimony: 'Pension alimentaire reçue',
  disability: "Pension d'invalidité",

  // Allocations
  unemployment: 'Allocations chômage - ARE',
  ass: 'Allocation spécifique de solidarité - ASS',
  rsa: 'Revenu de solidarité active - RSA',
  apl: 'Allocations logement - APL',
  family: 'Allocations familiales',
  aah: 'Allocation adulte handicapé - AAH',
  aspa: 'Allocation de solidarité aux personnes âgées - ASPA',

  // Autre ressources
  dailyAllowance: 'Indemnités journalières',
  annuity: 'Rentes',
  other: 'Autre'
})

# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.40.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.40.0...v1.40.1) (2025-03-07)


### Bug Fixes

* **archive:** Modale s'affiche une seule fois à la connexion ([93202b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/93202b2e733fd4020aa519ee9589e45315eba369))

## [1.40.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.39.0...v1.40.0) (2025-03-06)


### Features

* **api:** ajout de la ville à l'api indicateurs ([f12fb4b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f12fb4b711bc0696b720110b01a1c96b29185e55))
* **archivage:** notifications ([e9d1e88](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e9d1e88ba4abd5f01eb131192bcb7b95cdec622f)), closes [#719](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/719)
* **archive:** ajout du bouton archive sur page bénéficiaire ([6a3494d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6a3494d10abb83c3f5967b020aff24f6676b5f33))
* **export:** journalise les exports bénéficiaires ([687069c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/687069c59c94a36123f1c81f1adeee9c0583b21e)), closes [#559](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/559)
* **rdvsp:** trace la modification du rdv dans matomo ([1a3746f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1a3746f12d85db68246fc98664a1454f470f21b9)), closes [#744](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/744)
* **sso:** suppression d'inclusion connect ([90119f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/90119f71df40dcffb273cf3d820f8bcfc65ab2ea)), closes [#727](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/727)
* **sso:** suppression du menu mon compte à la suite de la suppression d'IC ([b40c4f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b40c4f783b048081ab7970d834334831a23ae65b)), closes [#727](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/727)
* **structure:** anonymisation des données relatives aux utilisateurs d'une structure désactivée ([52d2d9c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/52d2d9cd48bbbe2c59e8c4f5eb059b8119afe779)), closes [#745](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/745)


### Bug Fixes

* **archive:** ajout d'éléments manquants dans l'export ([0d508e7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0d508e739df8b52ac726326f3d8a032b1646ef08))
* **archive:** ajout des champs manquants à l'export du bénéficiaire pour l'archive ([d767569](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d767569a5dd66c2631b9e53967cfa082afc3e67b)), closes [#725](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/725)
* **archive:** exclue les rendez-vous de l'export du bénéficiaire ([a789e64](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a789e64e04f5a158172d911757b5d2441143e2f1)), closes [#725](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/725)
* **archive:** exclusion des agents d'accueil des notifications ([8768c45](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8768c454a1ac90283bb7052ae8c300f107a26e58))
* **archive:** fix des permissions de l'export d'archive ([8dab91a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8dab91a99ce7ab2a6c8e4db8799e8970a76ec8bc))
* **archive:** n'affiche pas la section Actions si non nécessaire ([34081bc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/34081bc28f43b76d3f28629b71af9b8dfed5bd01)), closes [#725](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/725)
* **archive:** permissions de l'export ([2904cc8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2904cc866abd62b901f6e39881775fa0fd894cd5))

## [1.39.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.38.0...v1.39.0) (2025-02-20)


### Features

* **archivage:** pas d'option de conservation nom et prénom pour Ministères ([0ae1160](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0ae116034595d867d1318fc00e33cf4cea15f3ba)), closes [#730](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/730)
* **rdvsp:** ajout squelette de modale dans le cas d'un foyer de plus d'une personne ([ef74ae8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ef74ae83fc64a68514df8483f5d437f9d8de505e)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)


### Bug Fixes

* **documents:** ajout de plusieurs documents à un suivi ([5a5897d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5a5897d1321344fb6a3be21dcf84d06a7a7e1bda)), closes [#738](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/738)
* **rdvsp:** alignement modale de prise de rdv ([ee746d8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ee746d861cc95df8c07b704f90aca29e1974fae8)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **statistiques:** adaptation à la nouvelle version de l'export de données ([444d6f4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/444d6f46001d66d3f57a2d3885484879460ad4da)), closes [#739](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/739)
* **statistiques:** correction du copier-coller des images ([0ae7e01](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0ae7e015cfab3ec22154d6c99820e93d31ed5fd8)), closes [#739](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/739)
* **structure:** activation conditionnelle du bouton de désactivation ([d477215](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d4772157a19f03e4baf22b6ca6df5396764c6edd)), closes [#676](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/676)
* **structure:** corrections de l'action de désactivation de structure ([b526d85](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b526d8568eda7d63fa15ed5f2091befd2becef7c)), closes [#676](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/676)
* **structure:** permettre la désactivation de structure à tout moment ([5d38965](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5d389652602a884840baae1c4d77db895f10a7a6)), closes [#676](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/676)

## [1.38.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.37.0...v1.38.0) (2025-02-16)


### Features

* **archivage:** ajout des documents et budget à l'archive ([15757b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/15757b29ebac1fad39c3251104c2d993528add42)), closes [#709](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/709)
* **archivage:** conserver le dossier 1 an supplémentaire ([6184189](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/618418982deef731be9c28d5a365464bcf51470d)), closes [#717](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/717)
* **archivage:** creation des permissions ([919a9d5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/919a9d516574b1badf22707b66881baa74ed4931)), closes [#710](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/710)
* **archivage:** export des dossier en pdf ([9b6e410](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9b6e41021fc14ea1645dbff9b9c5a95d513bbae8)), closes [#695](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/695)
* **archivage:** historique des dates de conservation ([789205f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/789205f0cb45913bf8245839aa44e4ba2e8519bf)), closes [#723](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/723)
* **archivage:** l'action 'supprimer le dossier' supprime sans archive ([ae44a58](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ae44a584b5dbe8d7fc941b8f11e0aa8b08438fa6))
* **archivage:** suppression du bénéficiaire de la bdd ([47580e8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/47580e8229d56b3ff3d27356127f8fbc3dd8f5b1)), closes [#713](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/713)
* **archive:** export des fiches en page HTML ([82b1987](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82b19870a758871953b13e3d39c066e8420774c3))
* **archive:** garder des infos pour statistique dans table archive ([87dc2ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/87dc2ca4a2badc6f12d1d4aee6414abb0840a2fe))
* **bénéficiaire:** enlève les documents de l'export bénéficiaires ([5953c66](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5953c66789498f3c79e20f672955ddbceb44cd3a)), closes [#707](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/707)
* **rdvsp:** ajout de l'heure du rendez-vous dans l'historique du bénéficiaire ([af98fac](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/af98fac0a70d3e2cc57c124e937357dab17c104d)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** ajout des rendez-vous à l'historique du bénéficiaire ([641f322](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/641f322ea091bd5e47b521701717443f91e4698c)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** ajout du statut à venir ou passé du rendez-vous ([a53d662](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a53d662955b275a5463ea6b846450e943769fc46)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** prise de rendez-vous avec RDVSP (V0) ([49efc49](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/49efc49eb01cb7ce4e5b026d8c90905aaf5e1238)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** redirection vers l'historique du bénéficiaire à l'issue de la prise de rdv ([16c67a9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/16c67a982ee5db4ad545d3bfa4f8dfe6a764d23e)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** ui de la page de redirection améliorée ([3821656](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/38216562cb450a3377d29ddb138f5c175e4769ce)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)


### Bug Fixes

* amélioration de l'affichage des filtres ([c064d61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c064d612657336057d4dc2df58c7d35e53543f63)), closes [#708](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/708)
* **annuaire:** champ libre du téléphone dans les organismes partenaires provenant de Data Inclusion ([7d1fbc2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7d1fbc2ea5bff0a3d0f01b779d4d75dbeb0266c3)), closes [#715](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/715)
* archive permissions + nvl fixture ([137b9b5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/137b9b5e89ad46ba955a7062bb80eb63b762d20e))
* **beneficiaire:** ameliore formattage et validation du numéro de téléphone ([cff3e2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cff3e2e1988bd8e4a030fc571bef1041626b833b)), closes [#715](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/715)
* **beneficiaire:** améliore performances export bénéficiaires ([c4ecfc0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c4ecfc08db9e4cd3dbe87bc2a849308d00b81626)), closes [#707](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/707)
* **bénéficiaires:** amélioration du bloc filtres ([d65e704](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d65e7044a070c7321dd562fd6839666c9c27bd5e))
* **document:** enlève la possibilité de téléverser des svg ([3eaa1f1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3eaa1f1c44bcdfa76a7a5a7a988ecefe0b7cb57f))
* **email:** fix envoi mail à la création structure, utilisateur ([1f2e59c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1f2e59c67390d03460c07d493619ca3c002bb615)), closes [#706](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/706)
* **qpv:** le filtre QPV doit toujours être visible ([da7ab31](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/da7ab3162bd076b5c9bf09b5416f85eb46728539))
* **rdvsp:** ajout snackbar,matomo,correction typo ([1425344](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/142534432f2b563811dcfe14771fb50ad751e82e)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** améliorations ui tuile rdv ([4218f47](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4218f47bd8cbc6b8e4aef48610a67796fcf2745b)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** avant première connexion d'un utilisateur renvoyer une erreur... ([58d0a57](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/58d0a5750b30684dde21705b81a8338350fb3f88)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** css de la carte de la demande de logement ([19c3dd9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/19c3dd9e9f020fee0a4924eccd0ccc815ebc446f)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** déplacement du bouton de prise de rdv ([7835724](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/783572476e6afb72208ce7f15fedcd0f28b03a1a)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* **rdvsp:** suppression de l'encart des rdv sur la page d'accueil ([c8cd775](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c8cd775d5812ac70d1c096d23391bce544a29c45)), closes [#526](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/526)
* typo objet d'accompagnement ([ec47c86](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ec47c86a2d4480a702ccd39aeb74f1b49a6238e5))
* **ui:** format du texte du bandeau d'info après migration dsfr 1.13 ([1756f8e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1756f8e91069eb94645dfb738a895fb682cdc666))

## [1.37.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.36.0...v1.37.0) (2025-01-07)


### Features

* **archivage:** création d'une page archivage et ses composants ([a49d6ef](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a49d6efe5d3407fd9d43d26ba8c5e23882a662a1)), closes [#687](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/687)
* **ministres:** mise à jour ministres jan24 ([081a368](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/081a36840641e4dbb3cd91c6880a8089d082c7b1)), closes [#700](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/700)


### Bug Fixes

* **brevo:** déasactivation de brevo chatbot ([fb8fa99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fb8fa995f5ba8d92c65a8a4813ebbf69ad24fd97)), closes [#696](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/696)
* migration ministres ([a4393d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a4393d78d4f81cf9d3ccaae12d40291e48cd2d01)), closes [#700](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/700)
* **ministres:** rajout gouvernement et réordonnancement ([5f5d285](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f5d285c6a22c2f0ccf2b97734871da0c84f06f1)), closes [#700](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/700)

## [1.36.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.34.0...v1.36.0) (2024-12-11)


### Features

* **budget:** ajout de charges et revenus personnalisés ([c53b927](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c53b927b0bf568ac2fef658e5f1944a4140e5902)), closes [#660](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/660)
* **budget:** défilement de la page vers l'élément sélectionné du formulaire charge du foyer ([1ba0215](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1ba02155f7082f34c39bef6acfd4fda8147f3f59)), closes [#654](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/654)
* **budget:** exporter le budget en doc excel ([6f972d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6f972d6dcfe0f2140d0f7f94d0298b149d80522b)), closes [#504](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/504)
* **dossier:** amélioration des champs adresse du formulaire ([ebb67ce](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ebb67ce716892485612dfa881e24ed4885df17e9)), closes [#520](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/520)
* **statistiques:** ajout du filtre ministre ([7f15b70](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7f15b70f46dad825f5122dd49b7cb41b0bf80dbe)), closes [#672](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/672)
* **statistiques:** ajout nombre de foyers concernés pour les stats beneficiaire ([d85732a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d85732ad7e121cadb01e219324c7db3f0789d1f6)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** Carte durée des entretiens ([2c78a3e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c78a3e6cf53fdb23d376c75b3ae89a4017d801d)), closes [#668](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/668)
* **statistiques:** carte durée moyenne d’un entretien ([14dd33c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/14dd33c48e077083ba4df7dc62100272a6b5c73f)), closes [#668](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/668)


### Bug Fixes

* **bénéficiaire:** ajout de permission au module budget ([125b3ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/125b3adc0a6cbee73ebd3ef234168b8f48750fe0))
* **bénéficiaire:** champs département et région visibles ([038fe80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/038fe806653c7086ef8d15f737578812b186fad8))
* **statistiques:** ajout des nouveaux bénéficiaires créés au filtre période ([4b7b273](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b7b273de26ca6364023b5d6f4db80e84d1a6cba)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** ajoute le nombre de bénéficiaires créés dans la période ([0f8198e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f8198e8d710a96b031ae7dc5b9e206c4c03045b)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** corrections d'erreurs dans les exports ([556b406](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/556b40602e88ff9f9bcfc6ca31b5cf040935663a)), closes [#683](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/683)
* **statistiques:** filtre ministre pris en compte dans la requête d'interpellation ([f144612](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f1446121a348d1482dbf995a690f61aa5757b7de)), closes [#672](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/672)
* **statistiques:** le filtre période filtre sur la date de création des bénéficiaires ([5d2c570](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5d2c5708314fe8b187fd650291a8226b7cc0c70d)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** les filtres sont tous visibles par défaut ([4983736](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/498373615626e6712dd1ccbcb3441a703244148e))
* **statistiques:** pas d'affichage des stats si le nombre de bénéficiaires est 0 ([83ce4f3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/83ce4f3cc6597ee7917a5c84df71e96113b0fca0)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** requête d'interpellation sélectionne uniquement les ministres filtrés ([d7245a0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7245a0bb0b52ad222e7226b5c07a0f4341ec4c0)), closes [#672](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/672)
* **utilisateurs:** correction ligne module budget dans tableau des rôles et habilitations ([27c9868](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/27c986808e9e02504db7b266417ab9bee1a61e7a))

## [1.35.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.34.0...v1.35.0) (2024-12-11)


### Features

* **budget:** ajout de charges et revenus personnalisés ([c53b927](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c53b927b0bf568ac2fef658e5f1944a4140e5902)), closes [#660](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/660)
* **budget:** défilement de la page vers l'élément sélectionné du formulaire charge du foyer ([1ba0215](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1ba02155f7082f34c39bef6acfd4fda8147f3f59)), closes [#654](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/654)
* **budget:** exporter le budget en doc excel ([6f972d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6f972d6dcfe0f2140d0f7f94d0298b149d80522b)), closes [#504](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/504)
* **dossier:** amélioration des champs adresse du formulaire ([ebb67ce](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ebb67ce716892485612dfa881e24ed4885df17e9)), closes [#520](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/520)
* **statistiques:** ajout du filtre ministre ([7f15b70](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7f15b70f46dad825f5122dd49b7cb41b0bf80dbe)), closes [#672](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/672)
* **statistiques:** ajout nombre de foyers concernés pour les stats beneficiaire ([d85732a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d85732ad7e121cadb01e219324c7db3f0789d1f6)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** Carte durée des entretiens ([2c78a3e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c78a3e6cf53fdb23d376c75b3ae89a4017d801d)), closes [#668](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/668)
* **statistiques:** carte durée moyenne d’un entretien ([14dd33c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/14dd33c48e077083ba4df7dc62100272a6b5c73f)), closes [#668](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/668)


### Bug Fixes

* **bénéficiaire:** ajout de permission au module budget ([125b3ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/125b3adc0a6cbee73ebd3ef234168b8f48750fe0))
* **bénéficiaire:** champs département et région visibles ([038fe80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/038fe806653c7086ef8d15f737578812b186fad8))
* **statistiques:** ajout des nouveaux bénéficiaires créés au filtre période ([4b7b273](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b7b273de26ca6364023b5d6f4db80e84d1a6cba)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** ajoute le nombre de bénéficiaires créés dans la période ([0f8198e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f8198e8d710a96b031ae7dc5b9e206c4c03045b)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** corrections d'erreurs dans les exports ([556b406](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/556b40602e88ff9f9bcfc6ca31b5cf040935663a)), closes [#683](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/683)
* **statistiques:** filtre ministre pris en compte dans la requête d'interpellation ([f144612](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f1446121a348d1482dbf995a690f61aa5757b7de)), closes [#672](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/672)
* **statistiques:** le filtre période filtre sur la date de création des bénéficiaires ([5d2c570](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5d2c5708314fe8b187fd650291a8226b7cc0c70d)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** les filtres sont tous visibles par défaut ([4983736](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/498373615626e6712dd1ccbcb3441a703244148e))
* **statistiques:** pas d'affichage des stats si le nombre de bénéficiaires est 0 ([83ce4f3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/83ce4f3cc6597ee7917a5c84df71e96113b0fca0)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)
* **statistiques:** requête d'interpellation sélectionne uniquement les ministres filtrés ([d7245a0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7245a0bb0b52ad222e7226b5c07a0f4341ec4c0)), closes [#672](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/672)
* **utilisateurs:** correction ligne module budget dans tableau des rôles et habilitations ([27c9868](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/27c986808e9e02504db7b266417ab9bee1a61e7a))

## [1.34.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.33.0...v1.34.0) (2024-11-28)


### Features

* **brevo:** ajout chatbot sur la page d'accueil ([bd5ebfd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bd5ebfdf9f8ef415d818c6ddae2838e0c0056193)), closes [#669](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/669)
* **security:** ajout hook pre-commit de détection de secret ([8bead26](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8bead260e562977a38eeff3f7d01ca3f9c534c22)), closes [#667](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/667)
* **sso:** désactivation d'Inclusion Connect ([b30c915](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b30c915547e23b5cf6183c0d689d08cc057fb1de)), closes [#640](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/640)
* **statistiques:** ajout filtre tranche d'âge ([c1267a5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c1267a5201496895389042da56bc7bf68d745ef7)), closes [#661](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/661)


### Bug Fixes

* **auth:** redirection pour comptes désactivés ([6a919f6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6a919f680e3a5aec2877ebe5076033790ba92ce3)), closes [#658](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/658)
* **statistiques:** nouvelle librairie d'export d'image ([66105c3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66105c3a31aa37f10addf9b396999843d64c4787)), closes [#663](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/663)

## [1.33.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.32.0...v1.33.0) (2024-11-25)


### Features

* **brevo:** ajout date de première connexion & utilisateur/structure avec >=1 suivi ([0dac351](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0dac3517f9aa1a2ca5235ee6afd90535ec40d2ba)), closes [#626](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/626)
* **sso:** suppression d'inclusion connect ([c687cf3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c687cf3b8fc91df957dd3d2b93cc8effaf84d532)), closes [#640](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/640)
* **stat:** exporter les graphiques ([5eadb4f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5eadb4f9d717a37ea0c052df0c3be08b3ff98991)), closes [#629](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/629)
* **statistiques:** ajout des filtres non renseigné pour les entretiens ([cb12f05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cb12f05ff0100104999255fbdf7bb586d5df0361)), closes [#489](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/489)
* **statistiques:** ajout des montants des aides délivrées ([510293b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/510293b8c23def5a7d0af8d92669d82da08bce85)), closes [#517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/517)
* **statistiques:** ajout filtre non renseigné sur les instructions de dossier ([79b725f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/79b725f6ac97b022e85f72656a7ba26d9e3f7c12)), closes [#489](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/489)
* **statistiques:** ajout montant accordé par objet d'accompagnement ([47cdd44](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/47cdd444ebec0dc42112d6b8bac94ef4b4cd703f)), closes [#517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/517)
* **statistiques:** ajout montants alloués par organisme instructeur ([fac7737](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fac7737df43626090c1a66568c0e94e1c251a8e1)), closes [#517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/517)
* **statistiques:** filtre les dossiers non renseignés dans les statistiques bénéficiaires ([4376431](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4376431e278d30639903f3806a27e71db7b89fce)), closes [#489](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/489)
* **statistiques:** filtre non renseigné pour les stats ministère ([4eb4ee1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4eb4ee1afd0a99224c05d4deba67a5a09fd9f94e))
* **statistiques:** mode d'hébergement pour tous les types de structure ([2aa25cc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2aa25cc4474d8fe89321aa79f4f4c56892b1c879)), closes [#648](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/648)
* **ui:** dissociation des onglets accompagnements, entretiens et instructions ([edc3aae](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/edc3aae876bf1dc9f5424569275222d44065fa59)), closes [#634](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/634)


### Bug Fixes

* **brevo:** suppression sur la page de login ([fd37587](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fd37587470f033ec3b7a7e1878b5f9c9ef66767d)), closes [#652](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/652)
* carte instructions, le total doit toujours etre à 100% ([35e07b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/35e07b4d3962925f4c51652d4b44192c01279b4e)), closes [#650](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/650)
* **emails:** cesse les envois d'emails aux utilisateurs désactivés ([fc789fb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fc789fbf9ec18180cb640fc702c6c4a0d4205142)), closes [#630](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/630)
* **security:** validation des types de fichiers avant téléversement des documents ([cbe400e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cbe400e291fe5c437dfaad507dea64d732fcc9f2)), closes [#602](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/602)
* **statistiques:** cohérence filtres et hauteur minimale tabs ([d2db83c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d2db83c9394292a16af635da1c50d86fea145802)), closes [#517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/517)
* **statistiques:** export aides financières ([7665452](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/766545273dd91fb9956b243084fba8d3cb8403be)), closes [#656](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/656)
* **statistiques:** filtre non renseigné sur les org prescripteurs ([bdf3698](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bdf3698388094ae8054ee7d61c8283cbd0c53b8e))
* **statistiques:** inversion des abscisses et ordonnées des exports Excel ([ac13a09](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ac13a091db1663b58194f14bc68dfb62ed3041c4)), closes [#636](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/636)
* **statistiques:** Retours de tests ([118718c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/118718c021d3a122ade733d3ab3733ccce39f749)), closes [#657](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/657)
* **stats:** ajout demande de logement dans carte instruction/objets ([c713222](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c713222075a6f581efdb54c1317c547973ce6636)), closes [#645](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/645)
* **stats:** ajout des cartes manquantes à l'export ([b3c09e0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b3c09e068523f906f9a6c6587b1bc620c952931b)), closes [#517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/517)
* **stats:** ajout du pourcentage d'instruction de dossier avec prescripteur. Suppression de la carte Organisme instructeur. ([8467b91](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8467b917f127ac76e3256812c0526d424d3ed944)), closes [#644](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/644)
* **stats:** ajout pourcentage entretiens avec prescripteurs ([2a8e783](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2a8e7837340e068e22e6db57915b801acfce1ae8)), closes [#643](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/643)
* **stats:** bug export des images coupées ([3e75cc3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3e75cc367015177beb1bd690491749e64d824131)), closes [#629](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/629)
* **stats:** objet d'accompagnement désactivé dans logement ([a6809c4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a6809c468572ea21e5a89b2d525ed5e8d0c65e12)), closes [#642](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/642)
* **stats:** résolution du bug d'affichage des icones ([abfe214](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/abfe214f1737fe2a0dbe013218451eb2081826a2))
* **stats:** téléchargement des images en zip ([d486966](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d486966bdb7bf042bca9bc9695d0378516d93b94)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/629#note_2199214738](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/629/issues/note_2199214738) [#629](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/629)
* **suivi:** ajout de documents pour un suivi sélectionne le doc ajouté ([d041367](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d0413672e5d09d4132e8a2be12a333e396eaf26b)), closes [#635](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/635)
* **suivi:** les agents d'accueil peuvent voir les commentaires des suivis ([22f9222](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/22f92220b03154a13a3f8d3df8e540200ce8c3dc)), closes [#619](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/619)
* **ui:** préserve les valeurs des filtres repliés ([645ada3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/645ada3d15c84a37f1b9a51cb883f6dba6ee80ae)), closes [#489](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/489)

## [1.32.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.31.0...v1.32.0) (2024-10-30)


### Features

* **brevo:** ajout du siret à l'export brevo ([8b511fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b511fe99f8d8a2b97f6c40b231c2d668a489cf2)), closes [#624](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/624)
* **security:** nettoyage des noms des fichiers téléversés ([8211a35](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8211a356e66acfe125e809d08e87efbf78ce99e7)), closes [#598](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/598)
* **sso:** authentification possible avec Proconnect ou InclusionConnect ([30a9d1d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/30a9d1dcd6d6c4aad65ab40b509960d992d322c5)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** mails d'ajout de compte utilisateur avec ProConnect ([31972a5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/31972a5ac4573e0aa59dabd774364fc9c1427af2)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** modification de l'annonce de transition ProConnect ([c1e6f67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c1e6f67708d430118a55a6f0cc400102c1b4431e)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** supprime la possibilité de changer de mot de passe avec proconnect ([616618b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/616618b1f8d31dfd5e7da59953ffb183f962eea6)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** utilisation exclusive de proconnect pour la création de compte ([8b6892c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b6892c044e4b6e170be634d044cf5040b3aba05)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **support:** ajout d'un chatbot de support sur la page de login ([d01cfe5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d01cfe5e0e266fe4edfb8933e95b0007b807ae4d)), closes [#625](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/625)
* **ui:** refonte du design du composant filtre multi-sélection ([794a078](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/794a078df9a3533c8ca8a546d5401b1aeb95ad2d)), closes [#583](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/583)
* **wysiwyg editor:** création d'un composant de texte personnalisé ([3f8ff46](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f8ff46c896a594db18b6c0dacc3561dcb8e5cdb)), closes [#539](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/539)


### Bug Fixes

* **accompagnements:**  Pouvoir désélectionner des objets d'accompagnements supprimés ([03a10ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/03a10ca713e1cceba74975a9b58c2544fecbf398)), closes [#596](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/596)
* **bénéficiaire:** la modale de doublon détectait le bénéficiaire courant ([adec00a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/adec00a6cc12e356cd3e97f20890c41ba200502e)), closes [#623](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/623)
* **champ multiselect:** amélioration de la navigation clavier ([e928fe1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e928fe12a50f5c5eef1d82bc5b3eb13732bd6f7b)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/583#note_2172883367](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/583/issues/note_2172883367)
* **éditeur texte riche:** amélioration du contraste des boutons sélectionnés ([8f6b8d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f6b8d60edb9305bdd16e8e6580cd404e240e8b7)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/539#note_2169582131](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/539/issues/note_2169582131) [#539](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/539)
* **editeur:** ouverture des liens au clic et formattage des urls externes ([f0467fd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f0467fdd25e8ce141a784fd764bce8e7638f3569)), closes [#539](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/539)
* **editeur:** supprime les chevrons au copier-coller ([15504ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/15504ca48b53327cf7084946692f69ab45789368)), closes [#539](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/539)
* **editeur:** supprime les chevrons dans l'éditeur riche au copier-coller ([851a25e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/851a25e719cf81b539f99c1240ac15b02b7b65c7)), closes [#539](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/539)
* **impression:** afficher la synthèse avant de déclencher l'impression ([3b06ac9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3b06ac97a8712d63f03a8dc87673219a84fb33d3)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/539#note_2169371620](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/539/issues/note_2169371620) [#539](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/539)
* **multiselect:** le dropdown superposable devient une option ([888111c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/888111c8a537e58198dbff31dbead9493c05fe8f)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/583#note_2184363282](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/583/issues/note_2184363282) [#583](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/583)
* **security:** liens externes sécurisés dans le mailing proconnect ([75e2241](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/75e22419c8da8ab367f890c782799449f241785d)), closes [#603](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/603)
* **sso:** déconnexion proconnect lors de l'inactivité ([e6e91f3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e6e91f3a41ff6ce78bdf3b4c801ca0bb2de33179)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** redirection après connexion stockée dans la session serveur ([a0e3bce](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a0e3bce1d0653ba767c84ad778d71e2ac28f482c)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** redirection après connexion stockée dans la session serveur (2/2) ([5356f73](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5356f738d7451a0017b3e71752b386d242238f5f)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **stats:** hauteur minimale des onglets ([7c41723](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7c4172329db7e83f90747db3cdaf230e5a73f6dc)), closes [#573](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/573)
* **synthesis:** si pas de texte 'riche' alors on charge le texte brut ([fb5f1cf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fb5f1cfa5506010ac8b495a08ef3e7a7b4fc49ab))
* **ui:** ajout d'espace sur les contenus d'onglet coupés ([ca4763c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ca4763c757cf68c6e522adb9fb8baaffa8c6a977))

## [1.31.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.29.0...v1.31.0) (2024-10-03)


### Features

* **accompagnements:** ajout d'un filtre 'période' ([d7cb6a1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7cb6a1e4ab31b5a5946b6c0fce13cd41312c15f)), closes [#572](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/572)
* **bénéficiaire:** détection doublon sur nom de naissance ([a3350d3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a3350d3416907f7cf3af48e764936287d9381f33)), closes [#488](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/488)
* **bénéficiaire:** les agents d'accueil peuvent voir et modifier la situation familiale ([f4a755f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f4a755fba969f72631a571335e79f99d853d680f)), closes [#586](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/586)
* **foyer:** finalisation de la dissociation d'un membre du foyer ([100461b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/100461bd9b8e4808b21252b0d83dd0848a0407e4)), closes [#563](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/563)
* **foyer:** première version de la dissociation d'un membre du foyer ([b8c282e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8c282ef594e165cd6e407ceb3597e87f7ae18db)), closes [#563](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/563)
* **instruction-dossier:** ajout de nouveaux champ dans l'export ([b25ded1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b25ded1aac9c521ac9933b1aee2fb36cd89e5204)), closes [#552](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/552)
* **instruction-dossier:** ajout des champs organismes prescripteurs et contacts à la demande de logement ([ce5becd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ce5becd63258b7348927eb7766077c5eae97df30)), closes [#579](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/579)
* **ministère:** ajout des ministres sept 24 ([971ae6f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/971ae6f3dc5da2b670cebebb254eb295ca918a6b)), closes [#585](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/585)
* **role:** mise à jour de la table des roles ([d69da3f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d69da3f645c1eb8b83af3fc2892910c46fd54ffa))
* **security:** double la limite de requêtes du rate-limiter ([d2ac8dc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d2ac8dc58cc405902e76c022a126e20ac4dfe6eb))
* **sso:** migration de IC vers Pro Connect ([2ef79df](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2ef79df2512bf953a29380d0333511d23c681fd9)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** utilisation de la session ([4928f4e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4928f4e202615a8461238d58037f54b0c7e23817))
* **sso:** utilise le bon client ([c5c2e21](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c5c2e21709fb1e4f5c30e0f6d568a98b18639218))
* **suivi:** modification de la permission d'édition pour les synthèses d'entretien ([5024a18](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5024a186fa5b393a98ecfbc6f3b54445d763c8c4))
* **suivi:** modification des suivis par les référents ([6aa79da](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6aa79da458e858eebd8d3cf1c37d4c500b942e1f))
* **textarea:** remplace < et > sur l'évènement paste par * ([27be9da](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/27be9daaf0d68900718cc1e4107da9c18966c26b))


### Bug Fixes

* **add beneficiary form:** détection de doublon sur nom et prénom ([130e847](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/130e847f62af28772b50386b3da37c985d1c01c2)), closes [#488](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/488)
* **annuaire:** amélioration de l'autocomplete ([1446313](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/14463133fb0f5b37cb96c5cad12a06843eed5355)), closes [#571](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/571)
* **bénéficiaire:** ajout des valeurs manquantes ([a2b810b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a2b810bc9d07714bf30700e7fe0c908633d07663))
* **bénéficiaire:** ajouter la détection de doublon sur le formulaire complet ([630575c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/630575cfa338d8fd451775a72eba59a1acac4833)), closes [#551](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/551)
* **bénéficiaire:** le filtre sur les objets d'acc. doit être associé au bénéficiaire et non au foyer ([ba70d2f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ba70d2fa98101b9b6adb6b7fb6b2952f1b895b9b)), closes [#578](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/578)
* **beneficiaires:** chargement des infos bénéficiaire après suppression d'un accompagnement ([fb7330f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fb7330f49c6c8226b10f9bed28eb6d1b78ed3f60)), closes [#581](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/581)
* **bénéficiaires:** filtre objets d'accompagnements inactifs dans les bénéficiaires ([48e0ce7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/48e0ce703e8aab0b5a581faf6f0dc8662571c188)), closes [#578](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/578)
* **beneficiary:** améliorations ui ([86a707b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/86a707be4da96313e9ca7d044cf77f78d04e93f7)), closes [#592](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/592)
* **beneficiary:** ne pas afficher d'erreur dans formulaire complet si bénéficiaire initial identique ([39a761a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/39a761aa3b41f8faa775446201229f04e9eb085e))
* **beneficiary:** ne pas afficher les sections 'Situation familiale' vides ([b24b21e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b24b21e2f76ff091533243f3f315940438d30a83)), closes [#592](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/592)
* **filters:** ne change pas d'état au changement d'onglet ([c6b443a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c6b443a7a359c262e68210f4f4813679d8d3adbe)), closes [#574](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/574)
* **formulaires:** les dates vides à la nav clavier sont transformées en null ([59ceb03](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/59ceb03df247b0a3d9f138e35718b93cb4ce3b93)), closes [#375](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/375)
* **foyer:** recharge les données du foyer après dissociation ([8516a3c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8516a3c03179bf4fef5888c08347fa863140f941)), closes [#563](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/563)
* **foyer:** statut du dossier manquant ([89aa572](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/89aa572726839046bb9012d7240cd763593f3e46)), closes [#576](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/576)
* limite les années à 4 chiffres dans le champ date ([c7ce5d8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c7ce5d8e9dbe79528a76f4653aee69ea74a5dccd)), closes [#375](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/375)
* missing types ([e91c779](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e91c77922e1dbecf3e0772e13793a3c87c5f1679))
* **statistiques:** ajout org. prescripteurs des aides logement dans les stats des accompagnements ([d88cfcb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d88cfcbb7ba8fa3324c5ae6fa21b00b5d06ee475)), closes [#579](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/579)
* **suivi:** supprime des filtres un objet d'accompagnement inactif qui n'a jamais été utilisé ([78f504f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/78f504f7600e5d08510de05380b9686cde62480f)), closes [#564](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/564)
* **textarea:** remplace tous les < et > sur l'évènement paste par * ([7b75515](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7b755157ed3a5d7d1145f708e9da4db64f0f17d8))
* **ui:** copier coller dans le champ texte libre ne remplace plus le texte présent ([452465e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/452465e1b76511ec18e3a45ab131f7a3b5cf0037)), closes [#594](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/594)

## [1.30.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.29.0...v1.30.0) (2024-09-17)


### Features

* **sso:** migration de IC vers Pro Connect ([8d1b4c2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8d1b4c27c565b860b6f1d06db79a5d314f07458f)), closes [#485](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/485)
* **sso:** utilisation de la session ([10d22e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/10d22e4a2cf709892edca582384d7b68daae46d0))
* **sso:** utilise le bon client ([78f3666](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/78f36667cd0420f4c96f823086b10924360f09cf))

## [1.29.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.28.0...v1.29.0) (2024-09-04)


### Features

* **admin:** ajout de la colonne création de la structure ([b1b567a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b1b567aa83d527bff59420c0dadc0ff3caed3d7c))
* **brevo:** ajout de données dans l'export ([e18690f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e18690f1dd1bb094838cd0ce1b11f299e23b0fa3))
* **foyer:** ajout filtre par membre dans onglet documents ([73ccd34](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/73ccd34b3b3a493ae81f4506b423e4595fcee2ce)), closes [#565](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/565)
* **foyer:** détection de doublon lors de la création d'un membre du foyer ([1776af9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1776af9c9532e4bce6f00ad326dceb62463d5c51))


### Bug Fixes

* **email:** Vérifie la date de création avant d'envoyer l'email de désactivation ([93a77e3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/93a77e3b3ca12a97d78bb76183cc8b6b68fad597))
* **stats:** filtre agent ([12c322b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/12c322ba1323c07587485142f859bb14f47cc7c6))

## [1.28.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.27.0...v1.28.0) (2024-09-03)


### Features

* **brevo:** export des contacts ([8b575b9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b575b93ba3129677014b1f21700ebea0d213c40))
* **budget:** dissociation des revenues et de l'entourage ([4bbd056](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4bbd056b912a76bba7895860d43356c3e5b844ae))
* **budget:** possibilité de supprimer les revenus ([09d1d8b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/09d1d8bd66fdb62fba3f03b0f10fd7d73bb6996e))
* **dependance:** mise à jour de eslint ([d6bb86a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d6bb86ad6c5432bd934571e7da12f7a72271090f))
* **dependance:** mise à jour de nuxt ([eda5b75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/eda5b75216ee570181a013b3594020def913e251))
* **dependance:** mise à jour de prisma ([3f92c90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f92c9096ee6ce5834d16cccba7bc98d818bf702))
* **dependances:** @gouvminint/vue-dsfr 5.19.2 ([2e6eeb5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2e6eeb59be5363abb0aa530ae61ce6e04b9b26b9))
* **dependances:** @types/node 22.2.0 ([7218cec](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7218cecb2b80a38b56a6eed5c287a9cb27233bdd))
* **dependances:** prisma 5.18.0 ([71368e0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/71368e0c922ff297ccba99bf5d41f63492d5c6d3))
* **dependances:** sentry 5.25.0 ([1bfba75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1bfba750a07d6d1f46f4a6759ed9c7c0d2546c02))
* **dependances:** styleint 16.8.1 ([e043e25](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e043e259527e5062814028f8b420f6aaf2086aa5))
* formulaire "Ajouter un membre au dossier" ([783a9ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/783a9ad4a819e2d7a6d3364ce858120e5e35100d)), closes [#508](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/508)
* Formulaire ajout d'un membre au foyer ([336ff6c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/336ff6c30fe98df8ba398d23af52494d1ed99055)), closes [#509](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/509)
* **foyer:** adaptation du budget au foyer (2/2) ([100f4cf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/100f4cfa6a8e4867fe71a8a7cf921ea28574d4b3)), closes [#549](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/549)
* **foyer:** affectation d'une demande d'aide à un bénéficiaire d'un dossier (2/3) ([2bc4fc5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2bc4fc5ed04e588cb8c62f338726a8e72e027d1e)), closes [#510](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/510)
* **foyer:** ajout breadcrumbs foyer ([fbab95d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fbab95d2616fdeb064f14592a86936e254cb3d08)), closes [#537](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/537)
* **foyer:** ajout d'un membre au foyer (1/2) ([687836f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/687836fd5d30e1f67b4e8d122fb16dfebb7f2024)), closes [#509](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/509)
* **foyer:** ajout de filtres par bénéficiaire pour l'historique du foyer ([9541b56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9541b563650a697b95f0549baae913b0daff0c76)), closes [#510](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/510)
* **foyer:** ajout des fils d'Ariane du formulaire bénéficiaire ([9cae920](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9cae9209d5fc269b79e5aefda376d20b6da7d575)), closes [#536](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/536)
* **foyer:** ajout des référents au foyer et du lien avec le bénéficiaire principal (2/2) ([8a1aef3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8a1aef30c08b30a8fbf0feb07fea02e3559580fb)), closes [#509](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/509)
* **foyer:** ajoute le lien de relation dans le foyer dans les infos minimales ([14ad8a0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/14ad8a0e43f2881e4191e52dbfca6ce17e49ae90)), closes [#505](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/505)
* **foyer:** associe le document du suivi au bénéficiaire du suivi ([8c3ad79](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8c3ad79f39ab3d99d074f92753c8ddd1b9e6cd46)), closes [#510](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/510)
* **foyer:** attribution d'un document à un bénéficiaire ([efedd0d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/efedd0d2da934bcdf49c510247e8fd5760343747)), closes [#546](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/546)
* **foyer:** finalisation UI de l'affichage du foyer et refacto du formulaire bénéficiaire ([060d5a0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/060d5a0378ca3e461da1b3d07d872ae816c9a596)), closes [#505](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/505)
* **foyer:** maj des routes et fils d'ariane pour les demandes d'aide ([a5cee4c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a5cee4cf51c08be0d698eff308943f568e6635c2)), closes [#546](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/546)
* **foyer:** mise à jour des routes et breadcrumbs pour les synthèses d'entretien ([b476205](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b476205342485e43261b22a2ff95a245149d6387)), closes [#546](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/546)
* **foyer:** première version d'affectation d'un entretien à un bénéficiaire d'un dossier (1/3) ([d6179ea](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d6179ea158e9c637c1e7d71047a4f818d59de83d)), closes [#510](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/510)
* **foyer:** pt d'entrée API pour les informations minimales du foyer ([7b450ed](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7b450ed66bd0e085e2eb7571507475f404a5dafd)), closes [#505](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/505)
* **foyer:** refactoring et ajout de fonctionnalités du formulaire bénéficiaire ([a55a932](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a55a9329fee9ba3744efe09b7ce2ac5336ce42a3)), closes [#536](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/536)
* Page dossier foyer ([7fc655e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7fc655eaeeb3c84e0e7341159caf7f8430c4824a))
* **stats:** ajout de filtre Ministrer ([64d1efc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/64d1efc65ccbffd76f06d1fee91b1674ba2b8166)), closes [#518](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/518)
* **ui:** ajout contrôle segmenté pour les bénéficiaires du foyer ([a8632ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a8632adc356fc31e10b28a5ed34b6b6238d356ae)), closes [#544](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/544)


### Bug Fixes

* **accessibilité:** comportement au zoom ([b8b55ec](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8b55ecef77b35623fd56262f5354f98e48d3aa7))
* **beneficiaire:** ajout d'erreur lors de l'échec de la validation du fomulaire bénéficiaire ([8a0eed5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8a0eed55b50e7e060edbdf23f39c8bc6cd7ec764)), closes [#515](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/515)
* **bénéficiaire:** Ajout de la civilité ([2931404](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/29314046a6d1aabe0efbfc8d3e211de655051068)), closes [#519](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/519)
* **bénéficiaire:** amélioration de la page d'impression ([d16135f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d16135f7d8ae78965b05f5d60f2443da0b95ec10)), closes [#558](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/558)
* **beneficiaire:** rajoute le champ date de naissance dans l'entourage puisque fusionné avec foyer fiscal ([acbab70](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/acbab7010071ed15d9bc6d5afedc0148d910c76a)), closes [#541](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/541)
* **bénéficiaire:** recharge les données après suppression d'un membre du foyer ([2514130](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2514130debe2f2e2177e6e6c9635dbb54161185c))
* **beneficiaire:** réordonnancement des champs dans entourage ([e9aba52](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e9aba5265cf3859a10fc757b84c9008fef94c8db)), closes [#541](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/541)
* **beneficiaire:** sélection du segment à la création d'un membre du foyer ([83dfff9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/83dfff9ea51a55de55a45bdef57882b6bfa2c87f))
* **beneficiaire:** suppression du bénéficiaire était cassée après passage au foyer ([8060912](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8060912e8ec3397d376e5b0a65369ba14f4c08e3)), closes [#505](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/505)
* **bénéficiaire:** ville de naissance augmentation du nombre de caractères ([f847340](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f847340e99ac5b9eac7128e8fbf0d251a9b49ef8)), closes [#521](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/521)
* **budget:** affiche le label de la relation ([582e812](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/582e812d84426eb914244d29ed267251d1dfd0e6))
* **budget:** associe le bénéficiaire et les revenus à la création ([4e04b1a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4e04b1a373bf44f1fcfab656c2979bbd035246e6))
* **budget:** modale ajout revenu choisir un membre du foyer ([5c8fd5c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5c8fd5ccecf0980233b108c403207e7679c129ef)), closes [#567](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/567)
* **budget:** rafraichit le budget après l'ajout d'un membre au foyer ([737be7e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/737be7e9212d1325cb804b22964d0d73ccf61dd6))
* **document:** choix des bénéficiaires horizontal ([a809ec9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a809ec937dccd997a699bb08f4927c7920280a15))
* **dossier:** rechargement de la page après ajout d'une personne ([c8c61be](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c8c61be31d210b941a83032333d942bd31d38264))
* **entretiens:** redirection vers la synthèse à l'issue de la création ([cd2a97d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cd2a97db1d3efd4e65ec741e964c59b94dfdebde)), closes [#522](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/522)
* **foyer:** retours de tests 1 ([9645c2a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9645c2ab026522588459075c3125ada3decbdc19))
* **matomo:** suivi des impressions ([1415fd9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1415fd946b66bfe8abdb2cc5519c10ebfd2144e4)), closes [#527](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/527)
* redirections apres ajout/suppression beneficiaire ([7b04e9d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7b04e9dccde07fcea9016a10a1fbe83c06508288))
* **structure:** désactivation autorisée uniquement pour les structures anciennes ([526f177](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/526f1778863d7a25b39a3a0c885113ae4679570b)), closes [#547](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/547)
* websocket connection errors ([c4f8bfd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c4f8bfdfab50e18e3c9b5b3f677e519d79ef98f6))

## [1.27.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.26.0...v1.27.0) (2024-07-22)


### Features

* **api:** ajout d'une api indicateurs des structures ([9aa033b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9aa033be31c71f4d552fa43ba36ff5d4afd396f2))
* **creation beneficiaire:** ajout d'un bandeau info ([50a50e9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/50a50e9b60b4911d48d88bfb62c90bc27a5dca39))
* **dependance:** mises à jour des versions majeures des dépendances ([1be5f9b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1be5f9bc656fd79aec50d041a90ae18b42fc7d5d))
* **dependance:** revert nuxt version ([0f1a7d3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f1a7d3178a3e185a0566d28ac8b0a08f801c6ee))
* **Dossier foyer:** Introduction du concept ([00ba156](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/00ba1569e45ea5e5a3bdc710384d18432856c70a))
* **dossier logement:** ajout des types de HLM ([5264b6f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5264b6f082c943e1e6ab6d87c9a8836f200120b5))
* **entretien:** impression d'une synthèse d'entretien ([76dc721](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/76dc721eedfb46e487a5d20eaedcabe25b779109)), closes [#358](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/358)
* **entretiens:** ajout de l'id de dosser dans le formulaire de synthèse d'entretien ([11f600e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/11f600e0baa2e6b8ab6d68068aed21239ec4050a))
* **historique:** imprimer un dossier d'instruction ([9a51c11](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9a51c116aa02e8fa0ec4c21f11caa9242df8f854))
* **instruction-dossier:** ajout de l'id de dossier dans le formulaire d'instruction de dossier ([98c9dd3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/98c9dd39107bc54d033514be42546f9b79c34958))
* **instruction-dossier:** ajout modèle logement à loyer modéré ([46d0241](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/46d024122b208c284600a2203d3831f8f0155b8b)), closes [#502](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/502)
* **statistiques:** instructions de dossier ont plus d'un objet d'accompagnement en prod ([e1a5a38](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e1a5a38e22c77c4a888f343bc68c626c20a65a30)), closes [#495](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/495)
* **structure:** affiche une modale en page d'accueil pour ajout dpo ([debd460](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/debd460d594fa0dc2fbbee5acd19e810212aab7e))
* **structure:** ajout de dpo au formulaire ([d4ef795](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d4ef795d4252cd728ddedf5ce54c7a6b24e1c341))
* **structure:** ajout des champs du DPO dans la structure ([66b8de9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66b8de962e3a98bca16555ecba05c1acd6acdd84)), closes [#500](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/500)
* **structure:** ajout du point d'entrée API pour le statut de remplissage du DPO ([5f42328](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f42328f49f3a16cef9d1558ccf053a2fa0a72da)), closes [#500](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/500)
* **structure:** désactive modale DPO pour les comptes administrateurs ([51882ff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/51882ff6896b45526f85f24985d70b84f2222cba)), closes [#532](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/532)
* **structure:** envoi automatique des emails avant désactivation ([b6cd21d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b6cd21d9b37adcec0f91607fd8d93e4dff412e3c))


### Bug Fixes

* ajout d'info de version ([ee4109e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ee4109e07742a39aa6aa311105d7826cd281c619))
* ajout enfant mineur dans bénéficiaire et futur nouveau breadcrumb ([07d4ace](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07d4ace133eb452a8c2ac7ee9608c83a770971a5)), closes [#525](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/525)
* **beneficiaire:** amélioration des permissions sur les données de santé ([1f271f9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1f271f98526e7efd1389ed2eb05a5be470271056)), closes [#503](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/503)
* **beneficiaire:** bénéficiaires similaires et redirection du formulaire simplifié vers complet ([4e5c136](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4e5c136287c6af5935d45ee1062df7a96c81e105)), closes [#525](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/525)
* **beneficiaires:** rajoute la colonne des objets d'accompagnement ([c803b19](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c803b1963b0a1132f594f80b1a806d437db95015))
* **beneficiaire:** supression mention champ requis dans le foyer fiscal ([7c75c14](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7c75c14017e5a57ac25a7504c72dcbdc2130831f))
* **budget:** update routes for budget with new file structure ([7c6965e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7c6965e1ecfcb11404328596153bb97a353593b2)), closes [#498](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/498)
* **data-inclusion:** suppression des données à la fermeture de la modale ([5406c54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5406c540dd9a0e3d19c1dc91d33b191c129071ce)), closes [#435](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/435)
* **data-inclusion:** supression de la recherche par SIRET ([a914141](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a91414115c850f3ecf1fa6132483368e753146b1)), closes [#486](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/486)
* **document:** encodage du nom des documents dans le header d'accès au contenu ([584cdfe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/584cdfef1ab8ce714c6ab41b436e2eabaf806ef4)), closes [#525](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/525)
* **Dossier foyer:** Possibilité de gérer les documents ([9051494](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/90514943816ed48e37bef530cdd1ec66c5da97e7))
* **Dossier foyer:** redirection de la page foyer vers la page bénéficiaire ([f98d0c3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f98d0c3603d3d41a91cc34f5c1c47e7012511f86))
* impression de [RETOURS TESTS] ([3fe4e8d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3fe4e8d428726a06aa291eacc3524b84b9abb586))
* ouvre le bon item après avoir imprimé ([94d4ae9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94d4ae9f07197ecbe1084ede005608ecf04fc8a4))
* **structure:** change la fréquence d'envoi des emails ([0e48078](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0e48078c233f3672f09a1a3a93bba09e3760b207))
* **structure:** désactivation d'une structure ([496d552](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/496d552736199f2a8a9c29ab9cc1ffece77d2f7e))

## [1.26.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.25.0...v1.26.0) (2024-06-19)


### Features

* **admin:** gestion de la désactivation d'une structure ([9fabff8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9fabff8f1a0e71f0bdcede3ed2ab7cd39a1bc422))
* **cgu:** mise en ligne et activation des CGU et de la politique de confidentialité ([2c24eb6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c24eb6dcfb515e8c9a88790190f662d2d5ad1d4)), closes [#100](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/100)
* **e2e:** ajout de tests d'authentification et intégration à la CI ([d9521d4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d9521d4daa4e885f8d6e88abe964b43790ffe8d3))
* **statistiques:** ajout des totaux dans les listes statistiques ([956a34a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/956a34a8ffc90c03e3ec89972def4d5a7ef132a5)), closes [#481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/481)
* **statistiques:** tests API instructions de dossier - objets d'accompagnement & aides financières ([86c0349](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/86c0349c54b372aecaee378f2d21accbbfe9df23)), closes [#481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/481)
* **structure:** gestion de la dernière date de connexion d'une structure ([c8785d1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c8785d1ff281dac6b997728ba28083ad5a260957))


### Bug Fixes

* **notification:** corrige la création de notification en tache de fond ([fff0586](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fff058647fb8a9e33de514d6fba6e719325d3aba))
* **statistiques:** corrections organismes instructeurs et actions entrepriseq & ajout de tests d'API ([e2ae557](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e2ae5576b3f10dffeb424b3fb9a7dd67bf11d3d9)), closes [#481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/481)
* **statistiques:** montant total des objets d'accompagnement incorrect ([913592d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/913592de4dc513fd21db456de37ba0ce775697b3)), closes [#481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/481)
* **statistiques:** récupération des objets d'accompagnement manquants ([dcc6bec](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dcc6bec36a963ab6b0137a5d6cf4e89bb3c73914)), closes [#481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/481)
* **structure:** gestion de l'affichage du bouton désactiver ([27ceb42](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/27ceb425a7cbd60e95437baba5b5e49100dc246f))

## [1.25.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.24.3...v1.25.0) (2024-06-03)


### Features

* **budget:** ajout des catégories de dettes au budget ([d879d42](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d879d423b83bba09d3094a5a3f5c68f5d2c3d166)), closes [#456](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/456)
* **budget:** ajout du champ banque de France et calcul automatique du reste à vivre ([21eb393](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/21eb393d89f2f168ae5afdc7b3769893050bbc08)), closes [#472](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/472)
* **budget:** catégorisation des crédits ([060e105](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/060e1057d9765ff1f54b23286880f65f8f092235)), closes [#457](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/457)
* **budget:** création d'une section assurances dans les charges ([e098051](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e0980515ab70c554441c5b506b56eac73cfe03dd)), closes [#455](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/455)
* **budget:** maj de la visualisation des informations générales du budget ([d2fe39b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d2fe39b9d59655f1277b4544da20a06962b514b7)), closes [#472](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/472)
* **data-inclusion:** édition d'une structure importée par Data inclusion ([8408607](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8408607f36127fa36c9e1fcf3c162c234b674539)), closes [#435](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/435)
* **data-inclusion:** finalisation de la V1 de l'intégration Data Inclusion ([f20feaa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f20feaa4f53dad5f92717d9cf54446ddf5f935e2)), closes [#435](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/435)
* **dépendance:** mise à jour de sentry ([3d94db7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3d94db79e5a6156ea410d6f6d314889a608b39e9))
* **statistiques:** ajout du nombre de bénéficiaires concernés par les accompagnements ([fa19295](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fa192959571564f49a49d6022d7c741b36876b12)), closes [#468](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/468)


### Bug Fixes

* **budget:** correctifs budget ([1acca1b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1acca1bf577cede392c8af20b3058b305ec36bc9)), closes [#472](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/472)

### [1.24.3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.24.2...v1.24.3) (2024-05-25)

### Bug Fixes

* **ui:** uniformisation barre d'action & amélioration UI objets accompagnement ([e097315](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e0973150da79a411d1dff2f7b1c53a5dbb93547a)), closes [#470](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/470)

* **statistiques:** retrait du bandeau d'informations ([4754d3d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4754d3d5fee22e1d882c65f6b53b264cc2574d2d)), closes [#467](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/467)

### Features

* **data-inclusion:** ajout organisme partenaire depuis Data Inclusion ([b760900](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b760900a8915f1029fe5be84b5fd38c2c6a7c305)), closes [#435](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/435)
* **documents:** mention de confidentialité des informations des bénéficiaires ([3059176](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/305917690f02466ec962fae3f47b346767e48918)), closes [#464](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/464)

## [1.20.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.19.1...v1.20.0) (2024-05-14)


### Features

* **accessibilité:** ajout plan d'action à la déclaration d'accessibilité ([a1d5e1c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a1d5e1cbd3e3a867851a3bf9d32818d8b5ce8304)), closes [#454](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/454)
* **api:** ouverture API structure et mise en place de la table des secrets ([40c60a1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/40c60a102ef1b6bc6324edaa7c3f7a6448bd2777))
* **beneficiaire:** ajout des dates de séparation et de divorce ([97336ff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/97336ff7421be931c97c035b70357262280e0a4b))
* **beneficiaire:** modification du formulaire d'activité ([6aabf27](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6aabf27dc681b59b55c946f1ee9fd9f2ddd2ea7a))
* **cgu:** Désactivation de l'acceptation des CGU en attendant le changement d'hébergeur ([5dfbde8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5dfbde88b79aa644e87a4ac2742021290e9d8ff5)), closes [#461](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/461)
* **cgu:** mise en ligne et activation module validation des CGU ([6f9983e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6f9983e37b71a1dde57d70721b6e9f12bb1aa954)), closes [#100](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/100)
* **confidentialite:** mise en ligne de la politique de confidentialité ([00dd5fa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/00dd5fa6f53ec88db0debbaccd09e784db85b1b1)), closes [#101](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/101)
* **data-inclusion:** ajout de la page admin des intégrations avec Data inclusion ([cefaa2b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cefaa2ba40652607787a743454265056f1086217)), closes [#435](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/435)
* **data-inclusion:** imports des données de structure de Data Inclusion ([7edb820](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7edb82007d21e2097ce01298c766a0d18c1cdef2)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/merge_requests/532/diffs#70fb162dae8ffe4611163cbf5d2a349cfdc28292](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/merge_requests/532/diffs/issues/70fb162dae8ffe4611163cbf5d2a349cfdc28292) [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/merge_requests/532/diffs#70fb162dae8ffe4611163cbf5d2a349cfdc28292_0_37](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/merge_requests/532/diffs/issues/70fb162dae8ffe4611163cbf5d2a349cfdc28292_0_37) [#435](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/435)
* **document:** limite la taille des documents à 20M ([242996c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/242996c3f3b6b05c4afabb9a722773c330a4a426))
* **document:** Stockage et migration des documents en base de données ([1e76a45](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1e76a45a45a9edc5ed1b8df756a693ddeb8b6d56))
* **notification:** ajout d'une notification de mise à jour des objets d'accompagnement ([b19fdd2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b19fdd2632b4e2693709909991dd9f9512bea301))


### Bug Fixes

* **app:** chargement notifications et synthèse sur tableau de bord défectueux pour agent d'accueil ([27e8bf8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/27e8bf832494080443103518ddccd63261349dd4)), closes [#463](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/463)
* **beneficiaire:** affichage de la date de séparation dans un champ en cours de modification ([c923cd7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c923cd775caafe7021bab5965ecf600bf3291292)), closes [#432](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/432)
* **budget:** arrondi des éléments du budget au centime ([7af3df8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7af3df85118fe2c5b10adcd95c3fe237f5cb4c90)), closes [#449](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/449)
* **cgu:** correctifs UI et typo ([4429097](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4429097f78d18ee0b0b60eae01db118ce1ff619d)), closes [#100](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/100)

### [1.19.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.19.0...v1.19.1) (2024-04-16)


### Bug Fixes

* **annuaire:** affichage conditionnel des contacts des organismes prescripteurs ([6076a67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6076a67b8e42d1a8b133dfc96353355e21546f34)), closes [#383](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/383)
* **beneficiaire:** amélioration de l'UI de la section activité du formulaire bénéficiaire ([d1e705d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d1e705dd103ea1b030b2aa82e7f160c58d5c4c3c)), closes [#165](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/165)
* **beneficiaire:** chargement des filtres de la page bénéficiaire apprès suppression ([1a016a9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1a016a945c07a2b15a588cd590fe22816ae01b6a)), closes [#427](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/427)
* **budget:** encart d'information sur le nouveau budget ([c052ac4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c052ac424c5ad58617fc5451093e3c33e62f3607))

## [1.19.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.18.2...v1.19.0) (2024-04-15)


### Features

* **annuaire:** gestion du contact pour les organismes prescripteurs et instructeurs ([afa1abe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/afa1abe8dbddec098cab5cbca0479df325bdb529))
* **budget:** Affichage des revenus ([f72cb2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f72cb2ee8494995e3ae40f93f28119e18c318554)), closes [#415](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/415)
* **budget:** ajout d'un membre au foyer ([40ade29](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/40ade29db6fc06785fde4c0883cccd0146ed4524)), closes [#415](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/415)
* **budget:** ajout de la permission d'accès au budget ([d9fb3c1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d9fb3c1a6d0daac6cd6056ab32e4daefce519202))
* **budget:** Ajout des couches techniques coté serveur ([7b56152](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7b561520cfc4cad6d7159d733d0f69f10bb7fcb0))
* **budget:** ajout des formulaires de revenus ([d951a36](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d951a3667fdf65424c2b1abc36073481af1c6e8d)), closes [#417](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/417)
* **budget:** ajout vue et modification des infos générales ([3508085](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/35080855ae70b946aeedc40436c2eaa23c0421be)), closes [#413](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/413)
* **budget:** correctifs sur les infos générales ([4f78961](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4f789616e0a5465e5ba974c54b7cd79fbe71b9b6)), closes [#440](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/440)
* **budget:** création du modèle de données ([70c4322](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/70c4322c05585fbba40e896f298e82923af6a28c)), closes [#411](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/411)
* **budget:** migration des données ([fe2cc08](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fe2cc082d7391a328b62e27e639e9b308f90f3f7))
* **budget:** modification des charges fixes mensuelles ([9c52f6c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9c52f6c7df19ed833ce5e951abaf9b0270a0e279)), closes [#414](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/414)
* **budget:** suppression d'un membre du foyer ([33214b0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/33214b072cb16a485f305ba5a2f7a03559f9b69e))
* **budget:** vue des charges fixes mensuelles ([2131320](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2131320b301c089846c43d7f97dca59e2825f2c9)), closes [#414](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/414)
* **dependance:** mise à jour de trpc-nuxt et nuxt-security ([f9d9c86](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f9d9c866842cf44c46ca31926cdc1a6fc9b7797e))
* **dependance:** mises à jour des versions mineures des librairies ([aae030a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/aae030a230a54828e9c4c63ecf6f7815f0e31b4b))


### Bug Fixes

* **budget:** clarifier message bandeau d'info anciens revenus ([0f7a094](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f7a0946975ffdf06915162b6a4f3877671d6550)), closes [#446](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/446)
* **budget:** correctifs de finalisation de la fonctionnalité ([995907e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/995907e199cfc472c8eb766e9d041d41e5368a14)), closes [#417](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/417)
* **budget:** création de revenus uniquement pour les nouveaux membres du foyer ([d9be33f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d9be33f7c644a28149b50ebf8288e58bcf5e67cc)), closes [#441](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/441)
* **budget:** le total dans les formulaires de revenus et de charges est réactif ([ab103d3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ab103d3147854965d9c4ed7fbd41eedf84f8e9cc)), closes [#417](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/417)
* **budget:** renommage de la colonne BudgetIncome.beneficiaryRelative en BudgetIncome.householdMember ([8e92cf3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8e92cf3b827be3766d945d48f3ba5b1b705688ec)), closes [#434](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/434)
* **budget:** supression du bouton secondaire Enregistrer dans la barre d'action ([1a7657d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1a7657def05cbe4d67759eefad009e09ee9c877d)), closes [#439](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/439)

### [1.18.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.18.1...v1.18.2) (2024-03-26)

### [1.18.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.18.0...v1.18.1) (2024-03-26)


### Bug Fixes

* **structure:** corrige l'initialisation des objets d'accompagments ([e396d18](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e396d181aca85b4a98edd9226547d4ef2f1e4aed))

## [1.18.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.17.1...v1.18.0) (2024-03-26)


### Features

* **admin:** configuration de la structure dans l'admin ([e025a94](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e025a945320b23559259eee99fe35eea161dbb92))
* **admin:** limit à 10 le nombre de structures dans le tableau ([f469b79](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f469b79a3b82358aa338c67086dc723e7e3df1e2))

### [1.17.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.17.0...v1.17.1) (2024-03-25)

## [1.17.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.16.1...v1.17.0) (2024-03-25)


### Features

* Affiche les pages avec un tableau ou des statistiques sur toute la largeur sur grand écran ([34a9006](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/34a9006b55da5235f023809002ec237fe863f73d))
* Ajuste les espacements dans l'application ([ca8eaa1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ca8eaa16bd920ebfd98673a7443f544dad9556aa))
* **beneficiaire:** Gestion du statut de brouillon lors de la création ([8db36fa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8db36fa00ce5865cb081b068967200bf649e4b73))
* **beneficiary:** Affiche l'âge du bénéficiaire à côté de sa date de naissance ([7c7bf48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7c7bf48a252e50dd6bd046693b2f5273cb0c19d0)), closes [#365](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/365)
* **beneficiary:** masquage des personnes vulnérables pour les associations et ministères ([6697354](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6697354669a6ad534d37dac82d227f223b45974f)), closes [#409](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/409)
* **document:** Coupe le nom du document pour éviter le défilement du tableau ([f8d3bae](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f8d3baef65e21de59738aa9638677e44e8e94621)), closes [#364](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/364)
* **file-instructions:** Affiche le filtre des thématiques de dossier sous forme de bouttons radio avec pictogramme ([778fb16](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/778fb16e738eebfc633960653ba482c2c3a5b723)), closes [#373](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/373)
* **habilitation:** ajoute les habilitations de la page structure ([99ce43b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/99ce43b4cf014475e86718496449418e4937d1cb))
* **history:** Affiche dans l'en-tête des éléments de l'historique du bénéficiaire si c'est son premier rendez-vous ou non ([a693d60](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a693d60a90dee04a7ed121b1a3506e9b65e95c02)), closes [#365](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/365)
* **history:** Améliore le design du tableau des dossiers d'instruction ([6954c8d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6954c8d069071378b649be7560c8920940335055)), closes [#372](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/372)
* **history:** Améliore le design du tableau des synthèses d'entretien ([f23e3ec](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f23e3ec5c259e4b25966e095c30d809c8df64767)), closes [#371](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/371)
* **landing:** Ajoute la page des statistiques publiques ([7bd8fe5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7bd8fe59026cf359244d2a2b8367f71b874a736e)), closes [#351](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/351)
* Permettre de retirer un sujet d'accompagnement d'une structure ([1e8dfe1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1e8dfe133736a1d0ca29dec5625fa4c4f6a7b0c3))
* **social-support-subjects:** ajout d'objets d'accompagnements ([dd03f36](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dd03f360c84233546808bd7ffc7ab0fc9be60ab5))
* **structure:** Accès à la page structure mode vue ([ca8aa61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ca8aa618d093f1016700f34e3bd93a6e0e938ff8))
* **structure:** affichage des accompagnements proposés par la structure ([5f90f25](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f90f256e52ac1be56e02532d8ed5a1814c11f8c)), closes [#380](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/380)
* **structure:** affiche le formulaire d'une structure dans l'admin ([ea4b56b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ea4b56bac18c2805bea1eff56178539fe2e965e3))
* **structure:** Ajout d'un objet - Champ personnalisé ([a727a64](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a727a64f968af68179a02f913d451207a7e576e6))
* **structure:** ajout de la modale de modification des informations de la structure ([e1f9b84](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e1f9b8406be610ecb7241d6acd202f315da1b2ab)), closes [#382](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/382)
* **structure:** ajout de la page structure contenant le résumé de ses informations ([66bdbbe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66bdbbe9a720d514537bd1833cb3af111a19f48d))
* **structure:** ne permet pas de modifier un objet du système ([7f4e4e5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7f4e4e56355f37d2e81587ec05b01ce10ca6b946))
* **structure:** Non modification objets par défaut ([d2b6737](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d2b6737c00a5772de86af0b452f3f8ed7107746f))
* Valide et formatte correctement les numéros de téléphone français ([39208b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/39208b2a3ee6481630aa87c0006ea1aa0c628c4a)), closes [#366](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/366)


### Bug Fixes

* **a11y:** Ajoute du contenu au libellé des cases à cochées, même lorsqu'il est masqué ([d7468a4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7468a4d291d60d972b19c66e81785e994498f11)), closes [#253](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/253)
* **Accompagnement social:** déselection de l'organisme insctructeur ([0a55a2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0a55a2e89ff446c64f52aee8c313d4b7a1a5a9f8))
* **beneficiary:** correction du label désignant les personnes vulnérables ([366a8e7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/366a8e7719da9b951dfe49f0e889f9ba8751e1ba)), closes [#360](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/360)
* **entretiens:** Filtres non fonctionnels ([691a5cb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/691a5cbd37b12694598f493b1b228cbb35d94065))
* **instruction de dossier:** Objet d'accompagnements disparaît ([02f2255](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/02f2255cfbb97821aa703ea791d2bc5344b92079))
* **structure:** création des os par défaut ([67d3df1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/67d3df10248963f80355e236c551c5f00ef79f4a))
* **structure:** issue[#381](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/381) ([21156fc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/21156fc2e5b4632a374c13dd1fb2fae8cc3e949c))
* **Synthèse:** déselection d'un organisme prescripteur ([1d4f103](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d4f10310845b7e1e4d697252095cb71188b1eba))
* **tableau de bord:** Affichage entretien en cours ([5c552e7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5c552e77cd150d56cd0f288883b6ca0bab314f82))

### [1.16.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.16.0...v1.16.1) (2024-03-04)


### Bug Fixes

* **stat:** Correction du ticket [#359](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/359) ([e6317e9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e6317e9570e1cab44b48d7c7a89ae3d886c5cb68))

## [1.16.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.15.0...v1.16.0) (2024-03-04)


### Features

* Ajoute une favicon ([c045eb7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c045eb77b9931046c353e48338fd8a43728aa582)), closes [#258](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/258)
* **Annuaire partenaire:** Amélioration de l'UI ([eca0932](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/eca09324115cf37948b36af0536724411c09da4a))
* **Annuaire partenaire:** Possibilité de supprimer un partenaire ([b0c4ab8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b0c4ab822ceebd4f070f96a5883cb347039c9c99))
* **Annuaire partenaire:** Reprise du style ([df575aa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/df575aaa5f6ac88ea5a1aa5127d359a7713337a7))
* **annuaire:** gestion de la liste de contacts ([ed8902c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ed8902c748a1c97d4942d00cfad4289a9511508c))
* **annuaire:** possibilité de supprimer un contact ([309ad00](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/309ad00d04fe115c5c35bccfcae90831c4eb328d))
* **beneficiary:** Ajoute l'adresse au formulaire de création de bénéficiaire ([fb752b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fb752b479fcd5db08f8aee331e6a3ae4296000e3)), closes [#332](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/332)
* **beneficiary:** Ajoute la détection de doublons à la création d'un bénéficiaire ([4097f6c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4097f6c436d7b51286ae693628ad8a4c00e32f53)), closes [#331](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/331)
* **beneficiary:** Retire le champ Mandat Aidants Connect sur le formulaire simplifié de création ([2d20318](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2d203184b62561da98423afc2daa1fba625ab1b9)), closes [#332](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/332)
* **Catalogue partenaire:** Ajout de la page du catalogue des partenaires ([285b59c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/285b59c73cba11b1db986aa085b3b32eb94adc71))
* **file-instruction:** Rétablis le filtre par date de fin de prise en charge ([f032956](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f0329567085fc1bd28287e522c5ad8fa20c69935)), closes [#353](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/353)
* **followup,file-instructions:** Ajoute l'alerte lorsqu'on quitte le formulaire ([f1e10bc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f1e10bc78e8380febdd55ac307c502408c797f20)), closes [#367](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/367)
* **followup:** Ajoute la durée d'entretien ([c151ce8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c151ce8ee2e71ff6daf0b588be9d2c5993955a84)), closes [#352](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/352)
* **ministere:** Supprime les signalements et les interventions ([6b45bb2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6b45bb211fce82ca1a099afc19af3a3c64569152)), closes [#290](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/290)
* **notifications:** Ne notifie pas l'auteur de l'accompagnement lorsqu'il commente son propre accompagnement et notifie les référents du bénéficiaire ([2d99ea5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2d99ea5e7164493edaa74be1077db334cf5b5700)), closes [#312](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/312)
* **notifications:** Reprend le design du module de notifications ([fc0f837](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fc0f8374560d470feac98e97d23941a7cc56a41b)), closes [#288](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/288)
* **overview:** Affiche les accompagnements en cours ([c174133](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c174133a067f69af5f11606aefa519d320747189)), closes [#336](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/336)
* **structure:** Met à jour les objets d'accompagnement proposés par défaut ([5143d00](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5143d002ba6316965ddec3f9dd8899d613938f1e)), closes [#350](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/350)
* Utilise le svg de l'annuaire sur le tableau de bord ([c0667d8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c0667d87fc1d3508ef57fcfa87240d3203ca5d7e))


### Bug Fixes

* Ajuste la couleur des pictogrammes ([f86a857](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f86a857448eaf87a7ccc28b5d2401544684a77aa))
* **followup:** Corrige un bug qui empêche de supprimer un objet d'accompagnement d'une synthèse d'entretien ([06c8fd4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/06c8fd432bb854577460cf7d61a14b66a73636a9)), closes [#363](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/363)
* **history:** Corrige un bug qui empêchait le défilement jusqu'à l'élément ciblé dans l'historique du bénéficiaire ([3e88674](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3e88674700170322d6ac984f91a29daf45646bd7)), closes [#354](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/354)
* Simplifie le texte d'aide des champs de numéro de téléphone ([625eca8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/625eca86707a361ea6f4d3ffd2dd10788566415d))
* **social-support:** Affiche le bon libellé pour le statut Traité pour les ministères ([f3b3a88](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f3b3a881cf93f2f09201654cd275ddc88b46acf2)), closes [#355](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/355)

## [1.15.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.14.2...v1.15.0) (2024-02-15)


### Features

* Ajout d'un filtre sur le type de dossier d'instruction ([da5fe7e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/da5fe7e6700039792a35ad8369dd29f46910a542))
* ajoute le code pays dans le format de téléphone attendu ([5556cf2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5556cf2b9d1f63c5a281acb1e2976890a3ffa199))
* Améliorations diverses accessibilité ([896aca4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/896aca4dd7affd62d18706eba33dbe029a8ad223)), closes [#340](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/340)
* **beneficiary:** N'affiche pas des filtres incompatibles avec le filtre de type d'accompagnement sélectionné ([736f3f6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/736f3f6e352a88419dd29ba8ed431cf5050bea05)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/249#note_1761862860](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/249/issues/note_1761862860)
* **dépendance:** Mise à jour des librairies nuxt ([a0ebde2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a0ebde25fa737ed6561fbf89d84725b7c01e87bb))
* **dépendance:** Mise à jour des librairies prisma 5.8.1 ([e8015e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e8015e6b195bb0a38e4920e0636633dbb71b1403))
* **deps:** Mise à jour mineur des versions de librairie ([99ad515](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/99ad5156a86f90f6891769ec7c61980b2d2c650e))
* **help-request:** saving issue ([66067d9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66067d985d94b422bac9ca35b5fc1e04e0de2268)), closes [#324](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/324)
* **history:** Affiche les demandes de logement dans l'historique de la fiche bénéficiaire ([dafb09b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dafb09bbd705f73c10792eaced19161f84707d52)), closes [#249](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/249)
* **history:** Classe les accompagnements d'une même journée par ordre chronologique ([8d892ff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8d892ffe68796d2eafaffd4d5f916e8220162891)), closes [#319](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/319)
* **housing:** Ajoute le formulaire d'instruction de dossier d'aide au logement ([e5e1bad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e5e1bade4018c1437efc4183ba665ac33606bcee)), closes [#291](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/291)
* Introduction de la notion d'instruction de dossier ([c50a07c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c50a07c1cd621ed59f60b16e49cdc86e064f7dd8))
* **minister:** Ajoute les délégués du dernier remaniement ([f42455b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f42455b91618d28d5fe89ea30e6ba8d4ed77cfb8)), closes [#344](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/344)
* Mise à jour des patchs des librairies ([490fa8e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/490fa8e9d72eb87588d8595a4d54ab0320d28b37))
* Mise à jour des versions mineures ([f7259f4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f7259f4064c86c65dcd07cbf7750a2121a3d5ccf))
* Ne trie plus les options d'un sélecteur multiple suivant leur état de sélection ([ecaaa47](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ecaaa476aa59d27cf69047200b0931e8d48aaa1b))
* Reprise du tableau des dossiers d'instruction pour intégrer les demandes de logement ([c099d31](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c099d31c442e4615e8848a84e332d64b24bc53f3))
* **stats:** Affiche les statistiques relatives aux demandes de logement ([e2ce68e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e2ce68e4a46c6e5a1d2cc98705966dc9cb11a4fc)), closes [#199](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/199)
* **stats:** Ajoute le filtre par période ([ac315e8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ac315e8b786ddb9871831b0d574da642abfc58e7)), closes [#341](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/341)
* update stylelint-config-standard-scss 12.0.0 to 13.0.0 ([2be05ff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2be05ffe761dca671671a933c160ea35aaff15dd))


### Bug Fixes

* **a11y:** améliore l'accessibilité du titre de la barre latérale ([8962598](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/89625983d81d56f1df9963635baf5f598a09a413))
* **a11y:** Améliore la découvrabilité des options d'un menu mise en focus ([4c9eac2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4c9eac26e2fcafaaa99dd550ae09144509503c63))
* **a11y:** améliore la navigation au clavier dans l'onglet historique de la fiche bénéficiaire ([b4488dc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b4488dcc4df0cafa2565d47159480ef50475bdf2))
* Après la déselection d'un filtre à sélection multiple, celui-ci peut-être considéré comme aucune des valeurs proposées ([f45720d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f45720d445eea7f473c849301eef8df4c675c9f8))
* **beneficiary:** Corrige la réinitialisation des filtres aux valeurs précédemment sélectionnés ([d6aaa03](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d6aaa036d88db0491e1b54be04774b4d4fb248f3))
* **beneficiary:** Corrige un défaut de typo ([52c3517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/52c3517b4d0573f61860478ee584ca5becd7bf08))
* **beneficiary:** N'affiche qu'une seule fois chaque objet d'accompagnement utilisé ([1304616](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1304616255b2e370894f0e9b5b55837c9cd02eec)), closes [#342](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/342)
* **beneficiary:** Ramène sur la fiche du bénéficiaire à l'issu de l'impression de celle-ci ([258d0ff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/258d0ffa47ed82372cbd1c088b480b9af8227b22)), closes [#315](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/315)
* Correction de la modification du statut d'un dossier d'instruction ([d0c1253](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d0c1253726bf569045b0d627bdeaf8e7cfca0e30))
* Corrige des bugs sur les filtres ([cc31abd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cc31abd876266c8020d2c370f7380bb9a7d3393f)), closes [#250](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/250)
* Corrige la chaine vide dans une champs date ([6cc82e0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6cc82e0174ef2dbf37179352d9636b362a37ef65))
* Corrige un espacement entre certains champs ([478d6c9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/478d6c97aa39f37ce3fa3bda1d19d4da06a1da5b))
* **formulaire:** autorise les numéro de téléphones étrangers ([018d1c8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/018d1c8ee92db0169d1428c46263e209bf25327f)), closes [#320](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/320)
* Gestion de l'erreur de validation Zod à l'ajout d'un document ([1887ad4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1887ad42ed01edf4b66646c7b0b4a774123a5f13))
* **help-request,housing:** Corrige un bug qui empêche de modifier la synthèse privée d'une instructiion de dossier ([3a160ec](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3a160ec7c58c87e8a6020a36ed97bbcc2214f60b)), closes [#338](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/338)
* **help-request:** Corrige un bug qui permet de sélectionner plusieurs objets d'accompagnement ([385a5b0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/385a5b0636404b3d99d08164bcf2f4024ece8810)), closes [#342](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/342)
* **housing:** Agrandit la taille du chevron de l'accordéon ([aad71ba](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/aad71ba69940fff390cc8d44975d77c88dcf6af0)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/291#note_1764112491](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/291/issues/note_1764112491)
* **housing:** Corrections mineures sur le formulaire de demande de logement ([8cf5932](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8cf59324bf7402eaa7810d655b93d0e2d323e7eb)), closes [#291](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/291)
* **housing:** Corrige les motifs de refus ([6ca4e26](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6ca4e2646b3682d03224caaab72bf2eff41bc5f0)), closes [#245](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/245)
* **housing:** Corrige un bug qui empêche de ne pas renseigner s'il s'agit d'une première de demande de logement ou d'un renouvellement ([3ae0e2b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3ae0e2bc7570421e77bd50ecdca02b260ae28a30))
* L'affichage du tri ne se met pas à jour dans l'en-tête du tableau ([1785148](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/17851489d2adc4250ba095a6eebd5021f8ddf8bc)), closes [#319](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/319)
* **overview:** Améliore la recherche bénéficiaire en se basant d'abord sur le nom de famille ([9ff4db0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9ff4db0f0adc672e03706145b1a88de4a3141402))
* **stat:** Corrige les filtres par date et par type d'accompagnement ([c8c596b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c8c596bb7217dd4783df9e5788d6da543b27b3b4))
* **statistique:** Calcule des aides financières: ([c690eda](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c690edad7db9e69fb56c26ee32af6c4ecedc103d))
* **stats:** Ajuste la date de mise en production de la fonctionnalité des demandes de logement ([441fba1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/441fba13b0811645449d50929d4d4b88a17cd2f5))
* **stats:** Applique correctement le filtre par période sur les statistiques des bénéficiaires ([baaa92b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/baaa92b14f34e5753ec60008c2d050c19168cfdc)), closes [#341](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/341)
* **stats:** Désactive les pourcentages sur les statistiquers de bénéficiaires ayant interpellé chaque ministre ([bf8d2e2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bf8d2e2de588c0ab42ee36f98f131c4c879e7150)), closes [#199](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/199)
* **stats:** Masque les filtres incompatibles avec la thématique sélectionnée ([2cbb269](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2cbb2691c9bd30d96ad8e1473b20744c70143638))
* **stats:** missing social support type condition ([2224de6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2224de6b1385d503029a8bbef89db1916acb8018)), closes [#325](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/325)
* **stats:** N'affiche pas des filtres qui sont vides ([b8f9ddf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8f9ddf7c8df7caddd4f94ad02de94e555afb951))
* **stats:** Recalcule le nombre total d'instructions en fonction du filtre de thématique d'instruction ([ee35fc8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ee35fc823b6b42a6b9c5a03b8dce376b8551d1f4)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/199#note_1761990326](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/199/issues/note_1761990326)
* **stats:** Restreint l'accès aux routes de statistiques en fonction des permissions ([3f6d6c2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f6d6c2122391c3ab2df53223ab6515c4180084d))
* **stats:** Trie les statistiques d'interpellation de ministre ([64a3732](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/64a373278966920cbadb493f682d6c026653c004))
* **structure:** Corrige un bug qui affiche en double un type d'accompagnement falcultatif créé une fois la structure modifiée ([ed5bf15](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ed5bf15cb038365c07a546a64c6943d2f6f2caa8))

### [1.14.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.14.1...v1.14.2) (2024-02-12)


### Bug Fixes

* **stats:** Corrige un bug qui empêche de filtrer les statistiques entre deux dates ou en fonction d'un objet d'accompagnement ([d98fc0e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d98fc0e509f2fa1219584709a5f7d93f0454d47c))

### [1.14.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.14.0...v1.14.1) (2024-01-23)


### Bug Fixes

* **statistique:** Calcule des aides financières: ([213a9ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/213a9ad0d711a371510de70032787d7e130249fc))

## [1.14.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.12.0...v1.14.0) (2024-01-22)


### Features

* Affiche la version du client dans le pied de page ([8f9da19](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f9da196782389eb334fe31d08d6317da697ec1d))
* Affiche la version du client dans menu latéral ([c6a8223](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c6a8223841826e26f3801aec5d7c360117393fb2))
* Affiche une erreur lorsque les caractères < et > sont utilisés dans un texte libre ([c802e30](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c802e301cfefda118b130b45e6b81428f551a676)), closes [#202](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/202)
* **beneficiary:** Ajoute le champ département ([82fffb2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82fffb2a23bc4a1be83cca4be3a3a6120306524b)), closes [#289](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/289)
* **document:** Affiche une erreur lorsque l'ajout d'un document échoue ([05f7683](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/05f76832b29fc6764fab6fae0d0e66b85c45634e))
* Gestion de la déconnexion après une mise en vielle ([6a46386](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6a463864227efcdb95549c904594a8f75413fb22))
* Gestion du rechargement de l'application après publication d'une mise à jour ([4b03b04](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b03b044c134a51775967783b3befda5ca2d2fb4))
* **history:** Déplie l'élément visé de l'historique du bénéficiaire lorsque accédé depuis la page des Accompagnements ([f0cc30d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f0cc30dee278e0ea6c700babbdf9a370355b6936)), closes [#284](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/284)
* **instruction:** Ajoute une modale pour sélectionner le type d'instructions de dossier que l'on veut créer ([829c8d3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/829c8d3a9a2856e02268836acf6564542f0cfe21))
* **instruction:** Reprend la mise en page du formulaire d'instruction de dossier d'aide financière ([15bb1ac](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/15bb1ac645f1b34a521e099e5a4adc7779778d8b)), closes [#246](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/246) [#245](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/245)
* **logging:** Ajout de la journalisation des adresses IP ([093149b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/093149bd5f3311dcee343bc511d0f7457f7c7265))
* **matomo:** Ajoute un événement de suivi lors de l'accès à la page de création de bénéficiaire depuis le tableau de bord ([4dee2dd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4dee2ddc890f126a63af669321be23c628185079)), closes [#280](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/280)
* **ministry:** Ajoute le nouveau ministre suite au remaniement ([13771b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/13771b2ed3e6e072a9357516f74b0f26133583eb))
* Retire le droit de suppression d'un bénéficiaire aux référents ([b38912d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b38912dcad99a5a3e601e051294ab89609ed51d7))
* **securite:** Augmente à 300 le nombre limite de requêtes avant que le serveur soit saturé ([ab4eb51](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ab4eb51bf843c8003e17375aa09c07793e41ea71))
* **stats:** Ajoute le filtre par statut pour les statistiques de bénéficiaires ([32a4863](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/32a4863e076e49405abf0095dade0c92804f872e)), closes [#283](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/283)
* **stats:** Ajoute les statistiques par département et par ministre ([98b2df6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/98b2df6e8ac9ad9ab4a395c3fccfaafa40afa3e2)), closes [#282](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/282)
* **structure:** Supprime les types d'accompagnement associés à une structure lorsque celle-ci est supprimée ([45ae4ba](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/45ae4ba319e5d74fc768c5a9a416dbb3b5df662a))


### Bug Fixes

* **a11y:** Permet aux lecteurs d'écran d'annoncer les messages suite à la soumission d'un formulaire ([acb236b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/acb236b8ed607233cf560d00cef7076d2bcf2fd9))
* Agent d'accueil ne peut pas modifier une instruction de dossier ([67711ef](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/67711ef3ad2fc43c529be7dae4d79a8ce903db3f))
* **beneficiaire:** Corrige un bug du comportement déplié/replié des éléments de l'historique d'une fiche bénéficiaire lorsque l'onglet actif était changé ([bed69bf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bed69bfa959a94ec9eef30ee03abce577989383d)), closes [#273](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/273)
* **beneficiaire:** Corrige un bug qui empêche l'affichage des flèches de navigation en bas du formulaire d'édition d'une fiche bénéficiaire ([dfc5ded](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dfc5ded6433f120f1b9e5c5d99d36424e99665f5))
* **beneficiary:** Corrige un bug qui empêche de filtrer les bénéficiaires par tranche d'âge ([10410ef](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/10410efc1e77b7690b7db61885469e8f2a6f90ca))
* Correction du calcul des statistiques des demandes d'aide financières ([3d385aa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3d385aa53adabcc80a3d89a5cb589157379946c2))
* Corrige un bug qui empêche la création d'un nouvel organisme prescripteur ([82b6e35](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82b6e35fd62c31131878ce8a456ffdc3e236b09f))
* **document:** L'état de chargement apparaît suite à la modification ou la suppression d'un document ([a4a8817](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a4a8817b544703e67276a72a68892460aa96eef9)), closes [#305](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/305)
* **foyer-fiscal:** Corrige la modification des membres du foyer fiscal ([9b9fdfe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9b9fdfeb43f7252faa3aa86470c719af743ea177))
* **history:** Un référent peut exporter les synthèses d'entretien et les instructions de dossier ([302882f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/302882fd1dfb026c59a80daa46341cfc9e9df1e6))
* **instructions:** Impossible de modifier le montant alloué depuis le tableau des instructions ([9a9904e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9a9904eedd0c93d3cb8b219a6b450368aff80ad7))
* Corrige un bug qui empêche l'initialisation de certains filtres avec leur valeur précédemment sélectionnée ([2b923ed](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b923ede228f86eec67204073d0441da7a9e1ab2)), closes [#254](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/254)
* **notification:** Corrige un bug sur les notifications de commentaire ([2b75ae6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b75ae64ec5eaa2f27e8b7d29e08c75d9bc0c546))
* Présentation par ordre décroissant des stats de types de suivi ([f781a1a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f781a1a7e2aaa1e5ecc95ec80946295d5e4e1c2b))
* **security:** Consolide la configuration des Content Security Policy ([80128b1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/80128b18f40135d8a32e9fa4e028bb6e2f22d755))
* **social-support:** affiche le séparateur avec les commentaires dès lors qu'on peut ajouter un commentaire ([5a4a31b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5a4a31b40a1b82f505cee48dff297f5c902bb9b8))

## [1.13.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.12.0...v1.13.0) (2023-12-19)


### Features

* Affiche la version du client dans le footer ([2a10399](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2a10399de213bfd92c6f6e2636eb792c6af30b45))
* Affiche la version du client dans menu droite ([8f88640](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f886400270480ba3cca2812853e0239814a7e16))
* **logging:** Ajout d'un middleware pour logger les adresses IP ([0ed7df1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0ed7df18110c6157908472ef01c46fe3487314b4))
* **securite:** Augmente à 300 le nombre limite de requetes avant 429 ([a55664e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a55664e508d09c1caec39aa4d4bd02cdd607afab))

## [1.12.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.11.1...v1.12.0) (2023-12-13)


### Features

* **beneficiaire:** Affiche des champs relatifs aux ressources des autres membres du foyer en fonction de l'ajout de membres au foyer fiscal de la fiche ([302745f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/302745ffd5f67f5b2c35abc2b0d13fa24e1983da)), closes [#241](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/241)
* Être notifié quand un.e collègue ajoute un suivi à un bénéficiaire dont je suis référent ([faafb10](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/faafb1045ed803c00e51d0a71de3246ca2fe882a)), closes [#239](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/239)
* Gestion de message d'alerte avant déconnection ([66a729d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66a729d7056156c821dcdcc6ab0209b7f4c830fb))
* Gestion du rafraichissement du jeton de sécurité ([40257e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/40257e49c5f238edc8aea07e4781185c1a5da372))
* **instruction:** Ajoute une modale pour sélectionner le type d'instructions de dossier que l'on veut créer ([52c5d90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/52c5d901b07c511f580ac43327ee2910765ece4a)), closes [#243](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/243) [#247](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/247)
* **instruction:** Renomme instruction de demande d'aide en instruction de dossier partout dans l'application ([0e41e16](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0e41e168f6976fdae711f08a3fb7da7f65f5148b)), closes [#237](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/237)
* Marque le cookie en http-only ([1b70e0e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b70e0e994f509144fa1de583fcf0f8890edf980))
* Mise à jour de nuxt-security ([11a138c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/11a138ccc519c10ac6436643f77635ad89327842))
* statistics contrast ([da65e55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/da65e550ef63d8b209a1d492a29415f2dff47a5e)), closes [#251](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/251)
* **stats:** Afficher la répartition des bénéficiaires par villes ([7976819](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7976819bb806571e19d7c9f59733c4318fc953ad)), closes [#240](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/240)


### Bug Fixes

* **auth:** Renvoie correctement une erreur lorsque le retour de l'authentication d'InclusionConnect ne présente pas les paramètres attendus ([4a2d97b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4a2d97b7a6921b7241f3c8033afc17adef981d91)), closes [#262](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/262) [#265](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/265)
* **auth:** Reprise de la validatation du token pour éviter un freeze de la page ([624969e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/624969e3fd2d6a95182859143c47f2c5ac7fee9c))
* **beneficiaire:** Corrige un bug qui rendait cliquable le lien vers un document confidentiel auquel l'utilisateur n'a pas accès depuis un élément de l'historique du bénéficiaire. ([504ad5f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/504ad5fdd2bdbda55fcfdc7673e0e241d48cd36e)), closes [#272](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/272)
* Correctifs mineurs ([c21b5ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c21b5ca9ffb6af291b8e91a8a8ce9b4eb2f6913e))
* Correction et amélioration des vérifications de validité du jeton ([06d2a98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/06d2a987b44b8330ff6582286c4a19b63c90c132))
* Corrige un bug qui empêche de retirer une date déjà saisie ([2b9d6bb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b9d6bb99a7f54737392518325c65002c1a0aef4)), closes [#270](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/270)
* Corrige un bug qui empêche de sélectionner une adresse dans un DROM dans les formuaires ([b6a44e8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b6a44e8c27e4311e71b2fd3eca9a8eaff0e896bf)), closes [#275](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/275)
* Désactive la modale de choix de type d'instruction de dossier ([3aee473](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3aee4731f941fd2afef1f663082de06343e4d738))
* **document:** ajout d'un documnt closes [#263](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/263) ([baba8d4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/baba8d404449a6a323225ee554f58ff8ef3b6807))
* **matomo:** enable matomo on dev environment ([0f48434](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f484340cc008a016e2c3ec916566f4d989bc294))
* Modification de la durée de validation du jeton de sécurité ([dc9a04c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dc9a04c0ff8331e63d0298cfb784bd57388af183))

### [1.11.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.11.0...v1.11.1) (2023-12-11)


### Bug Fixes

* **admin:** Impossible de créer un type d'accompagnement facultatif sur une structure ([58c68c9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/58c68c9de1e42273e4953d18c5de4834e19ca2bd)), closes [#266](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/266)
* **auth:** Renvoie correctement une erreur lorsque le retour de l'authentication d'InclusionConnect ne présente pas les paramètres attendus ([79d42b7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/79d42b723a173b973a7eb33b777e5ef98d6fe982)), closes [#262](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/262) [#265](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/265)
* **auth:** Reprise de la validatation du token pour éviter un freeze de la page ([65799f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/65799f7a26ab12091357e1355054de13834af8e9))
* **documents:** Améliore la mise en page des filtres ([94c11cb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94c11cb05c6c53137f077adeb270bbe6ef888948)), closes [#261](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/261)

## [1.11.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.10.2...v1.11.0) (2023-12-05)


### Features

* Cache les champs signalisations et interventions des suivis ([6a06fb4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6a06fb442b708be1f03c81783b78523254a72c1b)), closes [#234](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/234)
* Mise à jour de nuxt-security ([eca6ca9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/eca6ca9b9f141a0af0c61da0d347a9163257be7c))
* Mise à jour des versions mineurs des paquets ([534edf4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/534edf40c8b047aa301eec752df0ddfaf23e4564))


### Bug Fixes

* **admin:** Affiche un état de chargement lors de l'authentification à l'espace d'administration ([b8da168](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8da16830a7d1a7b4f5a517a6adee21f0ee30578))
* **admin:** Corrige l'envoi de l'invitation à l'atelier premiers pas aux utilisateurs créés 7 jours plus tôt ([6937b4e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6937b4e0725abf37de2dde959ec019e15c7b400f))
* **admin:** Impossible de mettre à jour les types d'accompagnement des structures ([55dbe61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/55dbe61ab8c6ebee78ca6f556bf471935e9519cf)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **admin:** La récupération de l'admin authentifié depuis les cookies génère une erreur ([4db97c1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4db97c108d6e834410db06840428458cc82306aa)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **auth:** L'application ne gère pas le cas où le jeton d'authentification n'a pas pu être récupéré auprès d'InclusionConnect ([46227d9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/46227d914727c886610a48d69c8b4127e102b0d7)), closes [#230](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/230)
* **beneficiaire:** Affiche toujours 3 filtres par défaut ([ec4297e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ec4297eeaf9f4c4d905c4215e1b18bfbd67a51f4)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* **beneficiaire:** Lors de la modification du numéro de téléphone du bénéficiaire, l'information était effacée ([6276e27](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6276e2762520b13f0a87f7501f9ebc1945b0c48a)), closes [#226](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/226)
* **beneficiaire:** Quand le tri par date de naissance est appliqué, les fiches sans dates de naissance sont toujours triées en dernier ([1b29222](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b29222b928c1ad1cb69d847e966ac52a436fd30)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([23dd319](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/23dd3190354312f60767a648f259fdd2c3e45e42))
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([94556ae](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94556ae512a32acca2d10c940ad84d83d0360f2e))
* closes [#228](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/228) ([039ec23](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/039ec238a5ffecd87d87687469e705afe5082dff))
* Déplace la génération du contenu des emails coté serveur ([d504e45](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d504e45c3d71399f411eb11c5652632a2be84cea))
* Do not throw an error during trpc context creation if the user is not found ([d62d705](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d62d705efed40aa86e96284816159c71f040aa4a))
* **export:** closes [#233](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/233) ([711d84d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/711d84d5c36da6ce626a065bbf5c2b1a03617709))
* Gestion d'un utilisateur connu de IC et pas de MSS ([8bfd380](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8bfd380e8517db633c26fd00235e8506cf6cbc4e))
* Gestion de la contrainte unique lors de la création d'un type de suivi ([62aba47](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/62aba474da20c502781f06c80b1bd4bf97170256))
* issue[#252](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/252) ([17cc65f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/17cc65fa6d39e74c43babe9a0e39dac1fbf02f12))
* **matomo:** set cross-origin-embedder-policy to unsafe ([e9af7b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e9af7b2423846ff12ea03c258b1f13b1b85a1845))
* **nuxt:** ptimizing dependencies configuration ([a2fe16a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a2fe16a75387aec7eb0675c6697486b24f0f0d93))
* Sauvegarde de Réorienté après l'entretien ([204c08a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/204c08a9c56e7ce38baee120313245cab114344e))
* **structure:** Corrige l'affichage d'erreur lorsqu'aucun type d'accompagnement n'est sélectionné lors de la modification de la structure ([0feb692](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0feb692cd7899dddf921244b231fdbe5bc74b20d)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* Supprime les espaces et mets en mininuscule l'email de l'utilisateur ([f8e7a30](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f8e7a305c439003102ef396a4fd36c98779af5fc))

### [1.10.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.10.1...v1.10.2) (2023-11-28)


### Bug Fixes

* **admin:** Corrige l'envoi de l'invitation à l'atelier premiers pas aux utilisateurs créés 7 jours plus tôt ([ecdca56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ecdca5612071cbf33a4386fde0c702307709ab52))
* **admin:** Impossible de mettre à jour les types d'accompagnement des structures ([afdaef6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/afdaef6e93907a946a6d319ad7a60aeead18b214)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **admin:** La récupération de l'admin authentifié depuis les cookies génère une erreur ([1dff780](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1dff7806c6a4c532a7f6b3a7289b923169cae6e1)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)
* **structure:** Corrige l'affichage d'erreur lorsqu'aucun type d'accompagnement n'est sélectionné lors de la modification de la structure ([8605bf3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8605bf357aa4f7f3edc1c1dd34e4ca738a294c1b)), closes [#229](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/229)

### [1.10.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.10.0...v1.10.1) (2023-11-23)


### Bug Fixes

* **beneficiaire:** Affiche toujours 3 filtres par défaut ([be17a99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/be17a99c6e6e0f6b77a35100328e8a67290c1b46)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* **beneficiaire:** Lors de la modification du numéro de téléphone du bénéficiaire, l'information était effacée ([43a542e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/43a542e0fddd3fb4f4417e64617502fb88875113)), closes [#226](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/226)
* **beneficiaire:** Quand le tri par date de naissance est appliqué, les fiches sans dates de naissance sont toujours triées en dernier ([2c467fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c467fe0ee0ddfce6bc9f5b7b5d825bd966c6c19)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([4b1c034](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b1c034d9d059718d92d1c41faee9c6cbccf10a3))
* closes [#227](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/227) ([3c7d7bb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3c7d7bbc162f9228062441bd7d2782db947f5e14))
* closes [#228](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/228) ([af18045](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/af180459ddf2d9f2062e2a3dc68917df393bce33))
* Déplace la génération du contenu des emails coté serveur ([fec1b3b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fec1b3b5bc04733107aea56fc9d0562f09ba8a93))
* Gestion d'un utilisateur connu de IC et pas de MSS ([e23fe3a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e23fe3a773ce73bec0f052dce382efc3461261e8))
* Supprime les espaces et mets en mininuscule l'email de l'utilisateur ([b424503](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b424503ed15e684f6f209479edddd61d7ef1af02))

## [1.10.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.9.0...v1.10.0) (2023-11-21)


### Features

* **beneficiaire:** Permet aux utilisateurs avec le rôle Travailleur social ou Référent de supprimer une fiche bénéficaire à laquelle ils ont accès ([dee2b51](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dee2b513ae2184153bc8d1c4bea9368e152398a1)), closes [#211](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/211)
* **beneficiaires:** Reprend le design de la page qui liste les bénéficiaires de la structure ([ee856c5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ee856c512b0826cb00200465fab29ccfe326f7b1)), closes [#92](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/92)
* **statistiques:** Ajoute un filtre par référents dans les statistiques ([4d7c5d1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4d7c5d18e4c42c8843cef1aed282cc8905ba8290)), closes [#213](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/213)
* **utilisateur:** Envoie aux utilisateurs créés il y a une semaine un mail d'invitation à l'atelier "Premiers pas" ([45a02bf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/45a02bf548494a8db1a08551c45d29f4f9948421)), closes [#212](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/212)


### Bug Fixes

* **beneficiarie:** L'identité du bénéficiaire n'est pas affichée dans le fil d'Ariane ([e3628b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e3628b26e943d027862675d02e9c695755fbf0c8)), closes [#214](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/214)
* Création d'un bénéficiaire avec un membre du foyer lié ([0ea892d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0ea892d51adaf52bcf110d455f5ef98d7eeb9c7b))

## [1.9.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.0...v1.9.0) (2023-11-15)


### Features

* Ajout d'un bandeau dans le formulaire de santé ([47038b6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/47038b6d73767add43a10adf9a97346c44cde66c))
* Ajout d'une API health check ([99b4b45](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/99b4b452d46af1fe92483bab49c9278063e59813))
* Ajout des constraintes d'accès aux données ([3ed6c02](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3ed6c0222f1f711264184415cf39855b8a4a6472))
* Ajout des permissions pour les documents ([1f95281](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1f95281a6f22c017bef4758236de3b0cd468f996))
* Ajout du type de structure EFS ([f7ded12](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f7ded120a43f73bdcb8141cbea3c7fc049fcf9aa))
* **beneficiaire:** Affiche une alerte lorsque l'utilisateur quitte le formulaire de la fiche bénéficiaire afin de préciser que les informations ne seront pas sauvegardées ([c0d0a97](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c0d0a974b0e060190c0367010c4d5ad327ed7b0c)), closes [#148](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/148)
* **beneficiaire:** Mets à jour le design du bloc invitant à compléter les informations du bénéficiaire sur le formulaire simplifié ([6fe8564](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6fe8564bc547493dc25d20f94997a2a8cb554c32)), closes [#191](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/191)
* design corrections for beneficiary page ([47ef8a0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/47ef8a0b5e85e3bd12b89f2ef63ff8a7d29b7996)), closes [#194](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/194)
* **formulaire:** affiche un bloc d'erreur qui résume toutes les erreurs présentent dans le formulaire ([2b8d1e9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b8d1e90d91542f2f24c135ce979c9301ec1f5a1)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)
* Gestion des règles de la page bénéficiaire via les permissions ([28c0769](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/28c0769d69f26c1e525ab70a7bbbf45e7429d4c9))
* **help-request:** closed by agent status ([44dca2a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/44dca2a7dcf670fec1a3a99ecaf3b5bb8dc18086)), closes [#171](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/171)
* Historique du changement des données ([1a4c6e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1a4c6e6c383d01c6a7e4df582ec29bf3719f249b)), closes [#57](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/57)
* Introduction des contraintes sur les dépots des données ([d3d6d1b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d3d6d1b83e29bd9b118e384f908b0d2daab5a24e))
* Mise à jour de la règle de visibilité des informations de santé ([94a621c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/94a621c2db16736e972569ccfb5f061954741107)), closes [#163](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/163)
* **prisma:** Usage des écritures imbriquées au lieu des transactions ([3b98f2c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3b98f2cda225159270b66a409de974f09a398825))
* **security:** Utilisation des permissions pour la création d'un bénéficiaire ([72295d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/72295d6cd4e4c1daca20c98428b77c2c4ede019f))
* **security:** Vérification de la permission de changer un champ ([4e7dc61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4e7dc61d9ad9e851037f9213948680e953955b08))
* **structure:** ajoute un mail de bienvenue à la création d'un espace pour une nouvelle structure ([d9d7209](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d9d720964548e189db52ee9d8f02c78bf1b4d1f6)), closes [#203](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/203)
* Suppression des guards et ajout des permissions par type de structure ([f885723](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f885723764713fa62ee3e8f5a1ccc062bd8ca75b))
* Une commune n'a pas accès au NIR ([a4445ad](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a4445ad0dc977b87d1ce521dbf3c21af89fe762e))
* Utilisation d'une vue sql pour les types de suivi ([bb6ebf8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bb6ebf8f296e2860172426ec46dcb3ba2fa49208))
* Utilisation des permissions pour la création du bénéficiaire et gestion des types de structures ([6d79df5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6d79df5b46bc94dca72e67ecaec021dcf6632271))


### Bug Fixes

* Affichage d'un bandeau informant du problème d'envoi d'email ([faa7ad4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/faa7ad417ff981b54050620d071a1f99b50b59dd))
* Affichage des commentaires dans l'historique d'un bénéficiaire ([4990d47](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4990d47a3d3c7192e5a6a2064312188a2aef68ca))
* Améliore les erreurs affichées sur plusieurs formulaires ([a93d774](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a93d77479d4f4c530efee0eb5b3e7da4e9b91eeb)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)
* **beneficiaire:** Affiche l'alerte de fermeture lorsque l'utilisateur quitte le formulaire via le bouton Annuler ([15c5afe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/15c5afe7598c4ffffb4267c5f42aec5626d182a0))
* **beneficiaire:** l'impression de la fiche bénéficiaire génère une page blanche ([10fdce5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/10fdce51b2c43d042f4e67bedd3a0930d79bb0d6)), closes [#181](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/181)
* **beneficiaire:** la navigation entre les sous-sections de la section informations générales recharge la page si le formulaire d'édition a été accédé depuis un onglet de la fiche bénéficiaire ([d7d028b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7d028b9fda9dd7fd386324802f785af1dfdcb44))
* **beneficiaire:** la navigation vers une sous-section du formulaire ne fonctionne pas correctement sur chrome ([3db8d08](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3db8d0812bc34243e448759a3e2f5a4038cad936)), closes [#180](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/180)
* **beneficiaire:** Masque une section de la fiche lorsqu'elle est vide ([1896b6e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1896b6e3c7123bcb10cfc3d6d7fc4b5b4a2ab649)), closes [#209](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/209)
* **beneficiaire:** N'affiche pas l'alerte de fermeture si aucune modification n'est faite sur le ([06b978a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/06b978a77cd2ae40019fdbceb080a87b74a1e3db))
* Change la règle d'apparition du block santé ([35b9eca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/35b9eca8cc0707d05b3457c7e1033f843c88e409))
* closes [#177](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/177) ([98c226d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/98c226dbae4f7a1d2a449345fedb6eb51ba41bc3))
* **connexion:** affiche un message d'erreur si la validation du jeton échoue ([18c296d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/18c296da062a10183256ce6fcdab86eb8f37ce25))
* Correction de la création d'un utilisateur ([9c73710](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9c737100221181116fcb299947d80a0a5391359b))
* Diverses corrections mineures de bugs qui permettent de sélectionner une option désactivée dans un sélecteur multiple ([5a4b882](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5a4b882fd85ac3158b0a2f2ad4df23f745d9964f))
* **email:** Les retours à la ligne ne fonctionnent pas sur Gmail ([f85a7c8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f85a7c8ebd580c2e8be247282f4f25380d6ff5c1)), closes [#203](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/203)
* **filters:** pagination bug ([2139ad0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2139ad009166a6553367e0e2df9c5e497cab4f7a)), closes [#190](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/190)
* Filtre les données avant le traitement des requêtes ([a63ab49](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a63ab49c2213333795f8e9afd0c8d2230ae0a030))
* **formulaire:** affiche un message compréhensible lorsque la date saisie est incorrecte ([f51f0e2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f51f0e24f7c3f2b8cb866b4f6a927fcced3e3dd1)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)
* **help-request,followup:** Corrige l'ajout de commentaires ([56978f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/56978f7495b477898070199bf6d5e21f7c5d7014)), closes [#210](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/210)
* le bouton de fermeture d'une alerte temporaire ne s'affiche pas correctement ([b002a84](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b002a84fb32ebb2534ba77bc4645c4c002d2f943))
* lock file ([5f2885a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f2885a01e748ab62fc1f0eb1e79f54d6b196960))
* Masque l'icône calendrier du DSFR sur les champs de date sur Firefox au profit de l'icône rajoutée par le navigateur ([250d75f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/250d75f7a58fc6b425034c18a8829b408db8c59c)), closes [#192](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/192)
* Règle d'édition des informations de santé ([fe51b01](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fe51b019dcdefcd602cee755f5d9ed29ae37e63f))
* un champ de formulaire faisant partie d'un groupe de champs n'est pas focus s'il est le seul a avoir une erreur ([87fb347](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/87fb3478b5987c37c9e0ac831f6ee024d4dde794)), closes [#206](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/206)
* VeeInput automatic formatting ([01873d1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/01873d18004d27d8de7a0524fb0e80ae5e817d18))

### [1.8.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.8.0...v1.8.1) (2023-10-31)

## [1.8.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.2...v1.8.0) (2023-10-30)


### Features

* Ajout du type de structure EFS ([c1eca2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c1eca2e801a93fb2d740d022ab4cdc1e606a9cf3))
* Une commune n'a pas accès au NIR ([2d01169](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2d011693339819f970f06bede0f8c8cbe6b8c9d9))

### [1.7.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.1...v1.7.2) (2023-10-23)


### Bug Fixes

* Affichage d'un bandeau informant du problème d'envoi d'email ([22a088c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/22a088ca0c9f892fca0f914b687e16d134e58ce6))

### [1.7.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.7.0...v1.7.1) (2023-10-23)


### Bug Fixes

* Correction de la création d'un utilisateur ([fa11bfd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fa11bfdcab64d417ffe194f52da3faaed91bf52a))

## [1.7.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.6.1...v1.7.0) (2023-10-16)


### Features

* **accueil:** ajoute un bouton et un bandeau de contact sur la page d'accueil ([8c4cb05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8c4cb05ac9b33f6723ffc26fb058ad32999c62b9)), closes [#143](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/143)
* Ajout de contraintes sur les entrées des formulaires ([4957334](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4957334f77b3b8a32ab6aef7c17f2f0fa8220e91))
* Ajout des vérifications des règles de sécurité pour les routes ([a5e6e00](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a5e6e00de0aa8dee3a3c1eacda45e68a9a10ce91))
* Ajoute la section Activité/Revenu du formulaire de la fiche bénéficiaire ([26ea759](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/26ea7597b2a2e76fa264cef8528a0fbc3e4146f0)), closes [#149](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/149)
* Ajoute la section Entourage de la fiche bénéficiaire ([00bdf22](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/00bdf22c97f88cb1e5097f297fdb77886ca095ae)), closes [#124](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/124)
* Ajoute la section Santé du formulaire de la fiche bénéficiaire ([330c4f5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/330c4f58c1c09308534b88b2e1ba151213dcb6be)), closes [#125](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/125)
* Ajoute la section Structures extérieures du formulaire de la fiche bénéficiaire ([36a8481](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/36a84814fcecd19d9007a468980e00eb5b9fe7fb)), closes [#126](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/126)
* Ajoute les membres du foyer fiscal au nouveau formulaire de la section Foyer fiscal de la fiche bénéficiaire ([6ada3df](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6ada3df0338f65a14367ab9cb8e887d004b723d4)), closes [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/123)
* Ajoute une étape Foyer fiscal au formulaire ([3f82008](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f820080a8141eecd1191799d91389a18760f2ec)), closes [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/123)
* Améliore la mise en page de la section Foyer fiscal du formulaire de la fiche bénéficiaire et rajoute le champ "Date de naissance" à cette section ([2e5848b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2e5848bf88dc42b915d40eebb3c3f6af66c85132)), closes [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/123)
* **beneficiaire:** ajoute le menu latéral du nouveau formulaire de la fiche bénéficiaire pour naviguer entre les sections ([dac0251](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dac0251f94fd47069868961838b710d1313b52e3)), closes [#127](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/127)
* **beneficiaire:** met à jour la sous-section active dans la section Informations générales au fur et à mesure que l'utilisateur défile ([e124307](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e1243077cd90e580911be20daa7aafabfd4d8bb4)), closes [#127](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/127)
* **beneficiaire:** retire le champ mandat aidant connect du formulaire simplifié et reprend la mise en page de ce formulaire ([545e56a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/545e56aab18d3183e16f8c35a8e290e2645356ba)), closes [#147](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/147)
* **beneficiary:** add quick action "add button" in sidenav and add required fields indicator ([bdf55f1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bdf55f124e5a1ed100c0bb4d17537fa4031bc379)), closes [#135](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/135)
* Change l'apparence du système d'onglet de la fiche bénéficiaire ([c1fdabc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c1fdabc8693d976e9d283beee84dc06a6851c456)), closes [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/132)
* Change la présentation de l'en-tête de la fiche bénéficiaire et des boutons d'action associés ([9080776](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/908077664ae4c2d47ed4038551ca455157cf8aa0)), closes [#130](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/130)
* Configuration des En-têtes de sécurité ([d7383d0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d7383d00d4bc064469a579432249f71bfcc25142))
* **document:** affiche les étiquettes supplémentaires du document au survol ([43aac23](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/43aac239d3d64a2cc123b65fce8cdf748df12417))
* **document:** améliore l'accessibilité des boutons d'action ([e652f65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e652f6513f41cda76faaaed0806824d2ccb45630))
* Enregistre l'onglet de la fiche bénéficiaire ouvert au moment d'accéder au formulation d'édition ([3c4fb4f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3c4fb4f723b8de76facf270c8004bab921705594)), closes [#128](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/128)
* factorisation de la vérification des règles de sécurité ([4acdedc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4acdedc0829cb7723f9ea80d8423b43900432788))
* **foyer fiscal,entourage:** ajoute des statuts aux membres du foyer fiscal et ajoute les nouveaux status Entourage professionnel et Ami·e aux membres de l'entourage ([046b785](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/046b785cbc6995a8490b09eedc38833a57f24be0)), closes [#139](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/139)
* **Liste blanche:** ajout des permissions pour les statistiques et l'administrateur ([a9f9ec9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a9f9ec9044eca788c69250d43562dc508b40b690))
* Mise en place d'une couche d'accès à la base de données pour chaque table ([b908ed5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b908ed503931fbad29c5e4fdfa6847f5dcb81135))
* Mise en place d'une liste blanche pour les accès API ([7ec0f6c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7ec0f6cdff69319cb1785cc3972de5cabca19c50))
* Nouvelle présentation de l'onglet information d'un bénéficiaire ([82dddba](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/82dddba6c775138d9e845f7e0d9524b94305cc52))
* Préparation à la mise en place de la liste blanche ([f548e72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f548e72f3644c399c735d4700754cc764f4cd678))
* Reprend l'expérience utilisateur de la section Informations Générales du formulaire de la fiche bénéficiaire ([b43866c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b43866c16d6958619981d9fbbceaecfe83740147)), closes [#122](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/122)
* Reprend l'interface des documents dans la fiche bénéficaire ([fd4e961](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fd4e96143c93116abb41b03a19014745817e51b3)), closes [#60](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/60)
* Reprend le design des systèmes d'onglets ([18d6d9c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/18d6d9c48c8c662834b73b4d4196bb48a957f427)), closes [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/132)
* Retire le nouveau champ Lieu-dit, commune déléguée ou boîte postale ([d844eff](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d844efffaccf44a7c7fd3bf18d20995e09dba7a5)), closes [#122](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/122)
* **seed:** ajoute un compte administrateur par défaut pour travailler en local ([34c55b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/34c55b4d761877b8327e4f2c0621e3da1fc20a23))
* valide la saisie d'un champ uniquement après l'avoir quitté ([1b2740d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b2740d3ff53d171c4d81aa0315150aae5d6853a)), closes [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/67)


### Bug Fixes

* **a11y:** améliore l'accessibilité des liens externes ([6bda729](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6bda72926cf8f73f54acb19b5614ff55f93fbe0b))
* Ajustements UI sur la nouvelle fiche bénéficiaire ([fbecc43](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fbecc43f9312ae147dcb366e2d018fbd347cb5af)), closes [#131](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/131)
* **beneficiaire:** améliore la mise en page de la section entourage du formulaire de la fiche bénéficiaire ([4b6ef34](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b6ef34d1f93b2b9cbbef04bc722f0d1a5f25653)), closes [#124](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/124)
* **beneficiaire:** corrige des libellés d'informations de la fiche bénéficiaire ([62e7b34](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/62e7b344a99f57e11aee7f2e5e19b5ad2c418ab4)), closes [#125](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/125)
* **beneficiaire:** impossible pour un agent d'accueil de charger les filtres relatifs aux demandes d'aide ([baf42e1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/baf42e16dab9eefc83a0655ccdc43955e6d1d74b))
* **beneficiaire:** le second numéro de téléphone n'est pas affiché ([a96a4fb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a96a4fb11636c12d2e5f1f81791e7e6a70bba950))
* **beneficiaire:** lors de l'accès à la sous-section Mobilité depuis la section Informations bénéficiaire, celle-ci ne reste pas active dans le menu de navigation ([f3fa858](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f3fa85827610ccff29eba26e05dec6da2dfe0095))
* **beneficiaire:** retire l'ombre portée des blocs de l'onglet historique ([a19c5e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a19c5e69f686f0bfad6c53a91591e0ddf66c95b9))
* **beneficiary:** améliore la mise en page de la section activité/revenu du formulaire de la fiche bénéficiaire ([ef24baa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ef24baab8653a7ca79d5378c7f29d4348ee64d54)), closes [#149](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/149)
* **beneficiary:** file number in details metas ([5f5d36b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5f5d36bf01f0c697c2893d8a875db12f7f52ae29)), closes [#130](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/130)
* **beneficiary:** referent role can save beneficiary with complete info ([bac2104](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bac2104fb7b54fee5884e5cec6b786d3a1c28e96)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/135#note_1545546050](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/135/issues/note_1545546050)
* Corrige l'affichage des onglets de la page Accompagnements ([196eba1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/196eba1c1df74dfa8b0fa78cf82c2ec90a86f19c)), closes [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/132)
* Corrige le message d'erreur lorsque la modification d'une fiche bénéficiaire échoue ([dbe7760](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dbe7760c56ea32e6584a7f9da20d13bfca579e43))
* **demande d'aide:** impossible de modifier une demande d'aide ([df127b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/df127b2083cfc8c51a54230dd89d6d71fc5f338e))
* dépencance cyclique ([c06f90e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c06f90e61d01a9ee9a37f8abc63fa6a2d7de95f8))
* does not return handlers from route creation ([1d0670f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d0670fa576a8589a116915e66c939d090d81d1a))
* **followup:** the success message after uploading a document to the followup is wrong ([63be16a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/63be16a815f8751c21331668e2aa0f5bb404d1ea)), closes [#136](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/136)
* **formulaire:** problème d'affichage sur les champs de type date lorsqu'ils sont désactivés ([d87bde6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d87bde65cda6441b1909ac39521613a31dcacec8)), closes [#164](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/164)
* Impossible d'accéder aux statistiques ([e4be399](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4be3993e47422c570eb21be8325f0aebe37cfe7))
* Impossible de vider le champ Numéro Pegase ([132cd0d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/132cd0de45e4e4debdc035882dab4139d18d05db))
* impossible de vider un champ optionnel avec un schéma nécessitant une longeur minimale ([fc6a76f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fc6a76f158141dd067a864b160ea23b7649ffccf)), closes [#107](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/107)
* **Inclusion Connect:** au retour d'IC, l'email est mis en minuscule pour faire la recherche coté MSS ([71eca89](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/71eca89288ffd5d12cbebc2a24d5408bbb7b8a9d))
* Lors de l'annulation du formulaire d'édition d'une fiche bénéficiaire, la redirection se fait sur la fiche bénéficiaire ([acc5c6a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/acc5c6a730ec1ea09cbbc2fba6e3eb05b736e1f1)), closes [#128](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/128)
* **seed:** beneficiaries does not have any referent ([1b05e18](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1b05e189f399b8d8123dc1028d5c57ebb19932ed))

### [1.6.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.6.0...v1.6.1) (2023-09-26)


### Bug Fixes

* Impossible de saisir un montant de ressource avec des centimes et de saisir 0 enfant mineur/majeur ([9b68ce5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9b68ce5b91c5288600f79e30a5ff4bd3cb05c95a)), closes [#145](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/145)

## [1.6.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.5.2...v1.6.0) (2023-09-04)


### Features

*  beneficiaries export ([28cf1d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/28cf1d7d17e8f80f1ac9219a849930c73519121d)), closes [#55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/55)
* **a11y:** adapt title for each page ([1f0ba7e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1f0ba7e09a857da3253b9c8cfa011c2f872c1f38)), closes [#87](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/87)
* **admin:** améliorations espace admin ([9d200fa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9d200fafafeb939b8b3e340911cad8af37516d79)), closes [#103](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/103)
* **admin:** Problèmes de création d'un utilisateur depuis la partie admin ([b497867](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b4978671be9c1abd333919850f1b63f028e55604)), closes [#102](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/102)
* Ajout d'un ruban indiquant l'environnement ([f572be8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f572be89c90284de3ffe8833c3bc19e95f7e6ef2))
* **beneficiaire:** Gestion du formulaire complet de création d'un bénéficiaire ([64e3f69](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/64e3f694bac38b725b86364790a4e07e52f69ce4))
* **export:** add followup count, followup type total for followups and help request count ([d40f115](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d40f115841a428be68dc5ef8d357fa7fba1885b3)), closes [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/56)
* **export:** Ajout de l'export des statistiques ([a7ea908](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a7ea908bcb8b407ce5593ccdc9fb19ea0a6763c3)), closes [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/56)
* **export:** external organisation column name ([d81e1af](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d81e1af8a0272555cf3e82035637e3c9cc3e0214))
* **export:** Résolution des retours sur les exports d'historique ([76ff8ca](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/76ff8cab7b5599b22d93992aea656322ac73aee0)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)
* **followup,help-request:** update ministers after the last cabinet reshuffle ([b6ba784](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b6ba784da23941dcfed990700ee50ab72e9ca90c)), closes [#99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/99)
* **history:** add export to Excel sheet ([dd58721](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dd58721557c7f01356ca2b56313468f8eb05d6a4)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)
* **matomo:** add matomo events ([776c393](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/776c3937a25b0d6c0b64287445f6bd3ad7eb8bbb)), closes [#65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/65)
* **matomo:** add tracking on the document edit button ([580c234](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/580c2342d9c4620f6cb8e920bcb17618bb33d653)), closes [#113](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/113)
* update roles table for history export ([0548ed7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0548ed78efa8c3e5193c10d37c3480c34107ffe1)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)


### Bug Fixes

* **beneficiaries:** age group filter is not working ([8943335](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8943335ed3bd9549befdc06716442d9d22f23bd8)), closes [#117](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/117)
* **beneficiary:** the accommodation mode additional information input is shown even if the beneficiary is not hosted ([1e2ec05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1e2ec05287c5180e745f7eebe9c6675eac21f2a2))
* can not edit additional information of beneficiaries ([64ab031](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/64ab0317d443f74eda45ed665847eef512f8daea)), closes [#105](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/105)
* death date error ([968718e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/968718e800d4e7b2df571f6896fb96a1d53d11bf))
* **export:** correct stats export permissions ([0a6a340](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0a6a3401663a97c6cf149cf8806ebb13e2c919ba)), closes [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/56)
* **export:** minor fixes in the beneficiary export ([cb45cf8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cb45cf8ec1f68dc00d71f50e9c4fdb21fc518009)), closes [#55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/55)
* **export:** minor fixes on history exports ([9e67dcc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9e67dcc45b5471019ec6e6b6a7701656b8b9b027)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/54#note_1533726687](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/54/issues/note_1533726687)
* **export:** resolve beneficiary export issues ([d48c2e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d48c2e4fb663268594238886714dffe553edc0f0)), closes [/gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/55#note_1533255992](https://gitlab.com/incubateur-territoires/startups/monsuivisocial//gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/-/issues/55/issues/note_1533255992)
* **export:** use the dsfr button instead of a link ([c771f98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c771f98b6e8bea64aac330d6421c427eb346c461))
* **export:** wrong column label for the followup referent ([12539b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/12539b47f48d593e6c05d3624bc1a4d05af09814)), closes [#54](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/54)
* **filters:** examination dates and cities filters only includes accessible data ([880f9e0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/880f9e02c3ff20faaaedad69ab4961a9e0cbc602))
* **filters:** hide referents filter to referents ([289f704](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/289f704fcc3eefcfed97f60419ee179647927662))
* **filters:** referents can not filter based on referents ([7448195](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7448195ee3d465afbefb8f9bc107a03fcd54b3c4))
* **permissions:** stats export is wrong for referents ([f5c0f1b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f5c0f1baddd7487d93139f62e58bbf01a28d0d23)), closes [#110](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/110)
* remove dead link ([d2d06d4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d2d06d40cde40aa63d238489c342ebb65334459e))
* Sauvegarde du champs Mode d'orientation dans l'édition d'un bénéficiaire ([11708d6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/11708d6e48fadd10d7e5bf803f252a2000aa6b71))
* schema imports errors ([ce65ad3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ce65ad3ad5f9b5944c239d1ca5a8d7dbbb5204b9))
* **stat:** first follow up stat does not take into account the referent status ([6ce621a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6ce621acf3a3a211e99b277aa93cfb23c9f72a8c))
* stats permissions ([b91b9a7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b91b9a764ddb96bb151e2b14248b95fc61111f94)), closes [#110](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/110)

### [1.5.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.5.1...v1.5.2) (2023-07-27)


### Bug Fixes

* **matomo:** add missing events to track beneficiary creation ([c4e3c94](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c4e3c949839398d2a57e7243581013c332a53bec))

### [1.5.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.5.0...v1.5.1) (2023-07-25)


### Bug Fixes

* **followup,help-request:** missing current ministers ([dbd7263](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dbd7263c0a7430b021c95997e90563958646840c))

## [1.5.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.4.1...v1.5.0) (2023-07-25)


### Features

* **followup,help-request:** update ministers after the last cabinet reshuffle ([5c0f6fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5c0f6feff72b3755dad6a8767faa1ad1f99a7bfd)), closes [#99](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/99)
* **matomo:** add matomo events ([6de414a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6de414a328f8b5cc1302333cbd9ab01024536f72)), closes [#65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/65)

### [1.4.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.4.0...v1.4.1) (2023-07-24)


### Bug Fixes

* death date error ([c43c363](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c43c363d0adaeebc8a45a48a6102b700ace75bbf))

## [1.4.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.6...v1.4.0) (2023-07-24)


### Features

* **beneficiaries:** sort beneficiaries by the usual name by default ([6c45830](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6c458303a1cb9a9c2831369c870c664344269f3c)), closes [#80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/80)
* **followups:** add date column and remove medium column ([850d0e4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/850d0e4e6d11dd627c2ee02c0437fa835e28b2cf)), closes [#78](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/78)
* **help-request:** update the label of the accepted status in help requests for minister structure ([56b869c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/56b869c9b3ee3964ccbed17705e9b060f9ff454d)), closes [#83](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/83)
* **history:** add minister filters for minitry structures ([0cfcf80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0cfcf8052f37e74f4ff01cf7155c7e924c3c491e)), closes [#83](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/83)
* **seed:** add minimal seed ([bef53e3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bef53e3b222e39f0646e68268aa49a3dc573025e)), closes [#89](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/89)
* **stats:** sort stats related to followup types ([d57cdf2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d57cdf298d698d81e8f609dfe5a9a171c8708849)), closes [#79](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/79)


### Bug Fixes

* **admin:** impossible to create an admin user not linked to a structure ([1e141bd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1e141bd55a93fcbb43f6107f5720e9f229c9c787)), closes [#88](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/88)
* **beneficiaries,help-requests:** filtering by age group fails and cities and examination dates contains other structure data ([7ed1934](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7ed19342b87173271b20a1301bc3cfaa94793650)), closes [#98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/98)
* **followups:** the follow-up types row is too wide ([184ee1c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/184ee1c6c64a10b95df4f0c66b9cbca8b24430d1))
* **help-request:** status for ministry are mismatched ([831e059](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/831e0598464633ca6f62ba5afdfbe75fc248ee56)), closes [#83](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/83)
* prisma updated date not updated ([934b6b1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/934b6b16d8d0305b2217c5d47bd598ffde7d5ae0))
* **table:** change the label of the sort button for numeric sort ([f2a1752](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f2a17527c5f26ee80b993c4364c7ffad02ce0e44))

### [1.3.6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.5...v1.3.6) (2023-07-19)


### Bug Fixes

* **email:** remove the incident banner on top of the user form ([7d8fafa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7d8fafa8806b44abeebfc0899bae5e27521fa0d2))

### [1.3.5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.4...v1.3.5) (2023-07-18)


### Bug Fixes

* **email:** update the incident banner message ([381f2f2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/381f2f2b92180bc46f824ac63c5387bac1cffb97))

### [1.3.4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.3...v1.3.4) (2023-07-18)


### Bug Fixes

* **email:** add a banner on top of the user form to inform about the brevo incident ([735debe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/735debe8ff4453f577710744a7ff2de7b7df30db)), closes [#91](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/91)

### [1.3.3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.2...v1.3.3) (2023-07-12)


### Bug Fixes

* **admin:** tous les utilisateurs peuvent accéder à l'interface d'administration ([05e3958](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/05e3958c1c4b018470105400e9ee030d55aaa03e))

### [1.3.2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.1...v1.3.2) (2023-07-10)


### Bug Fixes

* **logout:** remove user from auth store ([9baa27d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9baa27d04336deeaaf87973fe64019f99637f2e1))

### [1.3.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.3.0...v1.3.1) (2023-07-10)

## [1.3.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.2.0...v1.3.0) (2023-07-10)


### Features

* **document:** set added document into followup and help request ([69ee23e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/69ee23ebd64c8485093d48383daea688a88c2511))
* **tax-household:** remove deletion action ([dc45960](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dc4596013866aeac17e3d23293f04f30ea0fa968))


### Bug Fixes

* environment variable is not set ([bb9bff0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bb9bff0c2503ee7d91ef54957090d6decd309a5b))

## [1.2.0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.1.1...v1.2.0) (2023-07-10)


### Features

* ajoute matomo ([c92afa8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c92afa83c7a271ca30fc50c048eb76392e10bff3))


### Bug Fixes

* **beneficiaire:** ajoute le numéro pégase au formulaire de création de bénéficiaire pour les structures de type Ministère ([0d3d7fc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0d3d7fc4687fb2b7f0f7beac259efb8cac47549c))
* **beneficiaire:** corrige un bug qui empêche de vider le champ code postal ([132c281](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/132c2818d29f1f0c21de833977d7c1d142e6faa7))
* **formulaire:** Affiche des erreurs intelligibles sur les sélecteurs ([4509659](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4509659656b13ef8f308dd4201ca4b33fef7b8de))
* les tags de document ne sont pas correctement affichés ([bce44a8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bce44a8c5f3457b0cf255ba081c03e9ec0cb74a6))
* **logout:** call inclusion connect from client ([4751305](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4751305a492e0c7115c966b531c95ebeda703f01))
* use custom validation for zip code ([d0fe83f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d0fe83f2e0c7243f28352cb41c629d30073d195d))

### [1.1.1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/compare/v1.1.0...v1.1.1) (2023-07-06)

## 1.1.0 (2023-07-06)


### Features

* add accommodation zone stat ([86a3389](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/86a3389a60bfc6231dd2d68954245a8974fe8f41))
* add account page ([5875a05](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5875a05bd0ef4bd60e7643a3673ecd666cb62153))
* add beneficairy button action with access to add a followup ([2a63c1a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2a63c1a2d5673f638cc606794c3ead3bc00b46be))
* add beneficiary and history item brute delete ([74394f1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/74394f1750bbb8b76b658cb53f3aa36f479e2438))
* add beneficiary history ([a186517](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a186517eed911feddaf986cefc0b8d0ce6096a6a))
* add collection mutation logs on beneficiary page ([3844a28](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3844a28a14c567f000c0f02980812952db2c2b37))
* add createby and updatedby on mutable models ([8b2a4fa](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b2a4faf0538f811102f3a026ab6f60e128b9b17))
* add document ([3931e6e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3931e6e60741bcdb1ee99834a8bcb27c61457cc5))
* add due date and end of support notification generation ([c6fc73d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c6fc73d2a9cc3a65991f0bd69efdbfe4f87b4c78))
* add edit permissions to the help request table ([5e03655](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5e036557c79c1b8c8b2b48a0ab6ab2eda3baf70c))
* add followup tab in history page ([c9fd6a8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c9fd6a80e938bbd93ca63008e3c7ef3e2c55d46b))
* add help request tab in history page ([be2d293](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/be2d2935641dcf9e461791ec9093995681b64bf0))
* add instructor organizations to the help request table with sort ([d5aa4b2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d5aa4b2e33f6d85cfc478f3acfb5f68bb2bd0b2f))
* add license ([126a42f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/126a42f4bf658bcb49fad9150a3976e2f7287d23))
* add middleware to skip auth for authenticated users ([96074ed](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/96074ed8ff900576dfd3917638b528ec939b8da2))
* add ministere departement structure ac stat ([502fea8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/502fea817bc82a940312668be68c02e76ac508b6))
* add ministere structure stat ([24226c2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/24226c2a37f94a03fd10e915a59c73bdae00e05e))
* add missing instructor organization stats ([c12dc2c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c12dc2ca9b101d859eec5f026826724540b0a3ea))
* add missing rule ([63205f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/63205f7ed5e6e3d7769fec13e077a960483d9d4a))
* add notification generation one month before a due date ([20300c0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/20300c0527427bf66975ce591ab7bd20e2e68009))
* add page for disabled accounts ([0c3ac62](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0c3ac62f662c1742d008379e3ef0631c12d32c1d)), closes [#13](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/13)
* add pages to create or edit a help request ([e4c003c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4c003cc92d0b1a1e3dd89eed2c50f6158cc63cf))
* add print page and other information section in beneficairy details ([4171e96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4171e96650c26c9568584e6264571e06dc83f059))
* add structure page ([046d0f4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/046d0f4e63e447f730a3698b28844b9978c5e5b8))
* add users page ([12a76d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/12a76d74ffe8c0b30f2c2c3611abde56c48ffff3))
* admin pages for structures ([d015e57](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d015e5716517a9ba17e07636e871ffcd865ec58d))
* **admin:** add overview ([93455ee](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/93455ee2ee5fb0e5af73ecab6ab27a584a0e2663))
* **admin:** create admin space ([5c85ce2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5c85ce2f16d3336574c2a8222441a9be1d9c9b39))
* **auth:** implement properly auth store ([a85a3af](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a85a3af58430af980043ca82fef68c94aca0bd1a))
* beneficiary detail page ([9df5d2a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9df5d2acb4312b3b3e8022e176328e4ac0c2b41c))
* beneficiary edition ([66f2b52](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/66f2b5229fe4d26b5cac21892254be00129b6329))
* beneficiary form edition ([dd4e985](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dd4e9856b1c50b064a7bc1262e9ec0ec275e60ff))
* **beneficiary-details:** add followup types ([31b82b8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/31b82b81b2285de4873f32359617ffa176658816))
* **beneficiary-history:** add beneficiary history tab ([b1d89dc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b1d89dc3dda540bf93b22b4ad5dfe7cebea5a8a4))
* **beneficiary:** handle creation ([1abcfbd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1abcfbdde5c2de8fcc364030d5da270c33e3e8a5))
* **beneficiary:** start implementing beneficiary page ([8b8a2b4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8b8a2b49123ce02c77599f8587baa8f95e14241b))
* **cgu:** reccueil de consentement des CGU ([71fdf43](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/71fdf435233956a135a19f3110b16faf64e1a419))
* clean roles and permissions mess for beneficiary ([c0cc992](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c0cc992f5729d817178f2edfea9bbd8e77238f57))
* clean roles and permissions mess for stats ([3e9b4f7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3e9b4f7f0c7391023f19a910ee30de0a9d822f44))
* clean the whole roles and permissions mess for users, structure and beneficiary ([80a9989](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/80a99893b10c4c2852c26bacd11fa31882326d77))
* complete btn in beneficiary creation form ([5194048](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5194048583d54c858b3875e5c0b74174df03d4a5))
* **document:** add confirmation before deletion ([4b7a46c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4b7a46cc63575954c0149f5fca36d93da86525bf))
* **document:** display document list ([cf7bd3e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cf7bd3eda092d506cae6c69b005f48bb4cea6675))
* **document:** handle filename disk to maintain compatibility ([dba7a20](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/dba7a2042b7dc7c5a02f6646a5389e2c81f3b992))
* **document:** upload and delete document ([1c19951](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1c19951be33ccc2e2d25472bc7e13229e09b78b6))
* dsfr js scripts ([1860f55](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1860f55aac686dca75336c969e9be8066466ff89))
* edit document ([17897ab](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/17897ab9c6f4be17121e61b46ab225ba74681998))
* **edit-beneficiary:** use vee-validate ([4d8210a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4d8210a66e455d47a19426263948d906945d9d07))
* **followup:** implement followup creation ([b001ca7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b001ca75bee00cb90ba00396bd0fe7f82c004919))
* format beneficiary names to a human-friendly format ([2c5b800](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c5b80065ebb83646e98aa865325b06dca17abe4))
* handle relatives in beneficiary edition and update schema ([3420129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/34201295a285820a32748c3855b4a6d70d440af9))
* **help-requests:** update help request status wording ([07895e6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07895e6670e0286a73621f02d8be273715de5805))
* implement beneficiaries page ([08047eb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/08047ebbe8079ff30b530b80b1a8fe0b41f9d299))
* implement page showing variables ([0f1e1db](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f1e1db3ff16e4c929d0581f7ff605380eb30181))
* introduce page container component ([fcdb771](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/fcdb77104507801d680b0354ec64ddf4d181dff2))
* **jwt:** handle refresh token ([ade6e01](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ade6e01350a5bd24c746b3d2cbcdf884fe4399b6))
* lock access to different section in the beneficiary based on the permission rules ([4633d15](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4633d15bc8d539ea58adc57c2628914bc6987a0e))
* **logout:** logout after inactivity too long ([6be4d7f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6be4d7f001798a2c237ac36a4a45af1cae3864ad))
* make trpc protected procedure context safer ([8de8f19](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8de8f19825b2666b3648c4cd4b3ff16f2a84c130))
* merge two enums ([c708307](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c708307ab79d3c35a09b80a47c26be7b5e59479e))
* **notification:** display notification in dashboard ([2b73830](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2b73830d737fcb7bc93db04713da51abee54f5f4))
* **notification:** display notification in dashboard ([3c58976](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3c589767b568c53ef825e1448659af78ba5a71f1))
* **overview:** beneficiary search bar ([07eddcd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07eddcd269b418040049fe323516cdd3c7fce44b))
* redirect to the url the unauthenticated user is trying to access ([edc155b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/edc155b16a826bba847a483f5c6e91739865915d))
* redirect unknown user to an error page ([b107ce0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b107ce061531352b7e7dbed7e39e4641d992d376))
* **s3:** s3 lib and document handlers ([6729bce](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6729bcec73aa86e675e5fc9e840900a3d2968529))
* scroll to target element when navigating accross pages or tabs ([a47f3f6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a47f3f6af713ea61739198b73c9b1cd6b3334e34))
* **search-beneficiary:** search beneficiary input ([510dcb1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/510dcb1e6784b590b7844ef86b2a937980b0b97b))
* show linked documents in history ([07ffd87](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/07ffd873c9bf39f6b0eb56ae3a1a2fd266d0a52b))
* stats filters ([6e85147](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6e85147959d9b3f0a285a3c585bb58c343452bfe))
* stats history ([37e0b2d](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/37e0b2d4c6400fd13f5a9d6fcba732a290d5dc05))
* **tab:** use vue3-tabs-component ([597aee0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/597aee05ac080dbbed7cc4f199dea97c9be6b946))
* **trpc:** add transformer ([4f99885](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4f9988532561aed4c4810c16b5a34db27aff4932))
* update breadcrumb ux ([19e4510](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/19e45101cef1809d84c6774ab6cc59df01fa45a8))
* update token parsing and migrate to the demo realm ([4f48c8f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4f48c8f869a81a10a744d9942290fb77fde91517))
* update vue-dsfr ([5685102](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/56851021dd80e6c9608d0b0ffdb0a56dc26670ff))
* use security rules ([4a9ae48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4a9ae488bf7bfe3e064911a7292d543650d09f09))
* use vue-tabs-component in history page and rename tabpanels for better genericity ([8ac129a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8ac129a0805282d76c2f59f45df20ad402370711))
* useMinistryAgent ([7fe3d8e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7fe3d8e7037e33d3e7ab093380c8da411069765b))
* **user-activity:** introduce user activity ([2ebeaa0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2ebeaa02e6f2e0851100ad13d99eabcc1da31e1a))
* **users-activity:** replace mutation logs by user activity and add pagination ([5324b4a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/5324b4af8b5da4d480acd0966d68c628b02f4d7f))
* **user:** send email at creation ([e4180b6](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4180b61f1b4835912f0f87d27cc0b325a616cba))
* **user:** update email content ([ecc21bc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ecc21bc9fd6375165f5a4a780e7b1bddcfd5eeb6))


### Bug Fixes

* add social security number input ([8427cbb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8427cbb0f845d24c3c5c7de19fb8053a9eb0010c))
* adding structure follow up types ([7ad63c0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7ad63c002a68b467d0ce123cd6d94caa8f155c71))
* **auth:** fix conflicts resolution ([cc82c16](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cc82c164cf64793c7d483c8047ec4398cec2d89c))
* beneficiary deletion - closes [#63](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/63) ([781e1b7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/781e1b75f9f8255e0f580c25048fd570b04f1216))
* **beneficiary:** extra spacing around the info alert in the creation form ([13e74fd](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/13e74fd470750a008ed8dee3d2fd1c1190222dda))
* cannot add a comment to help request ([a624214](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a624214bc78eeefcbe78d62edb06e9bb61933754))
* closes [#64](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/issues/64) ([cbb1ddb](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cbb1ddbb59b663599e6178b42ed55e2c68525346))
* confidential document rules ([8ad62c9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8ad62c9efd5a9d1ed0dc2c2a7847b649ae499034))
* disabled agents in beneficiary add form ([342c8c0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/342c8c00ae4b016117f2e2a6e40b3b6d781fdeb9))
* **document:** use correct event ([f16e00b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f16e00b5a8bbc488b5363a1795153cccbaf52b67))
* examination date bug in filters ([9094fb5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/9094fb50084554deb7a349f90fbde6609dfb188b))
* filters for supports stats ([52c7f2e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/52c7f2e29d5852d4aff4c2210b5143e8db2a40c0))
* force using stable pnpm version ([13e351f](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/13e351f8b3ff9a1be4d5443e0addc2973f8ad236))
* import ([f545d60](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f545d60049c23558be3c4d06eec9cbae1274a01c))
* impossible to access a beneficiary from the searchbar ([f886d70](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/f886d705dec5e4ba88b77201b6516e3c1d6b51d1))
* impossible to edit a help request ([45fb6b7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/45fb6b7a6fb442502a6796f6d00554daa13fb09f))
* **inactivity:** logout only if process is on client side ([4ef2499](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4ef24999570ec8a13e38701067536914521f6b0d))
* instability due to the client directly accessing to prisma ([ea38ab1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ea38ab1f263411264182fba146179b594219919c))
* invalid page file ([1d60bf9](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d60bf933bceaef35eda9088ccafa007549befe7))
* javascript float number magic is applying to allocated amount sum ([cfa43ab](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/cfa43abf977de70449fb7f4de7a95d969dd4dbf9))
* logout success text ([192901a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/192901acfb4c7c6bf7e8d4ad20a3afa64b5e45e3))
* make linter happy ([1d028f2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/1d028f24195af2286bbe62dcb3c0644f9a3ddb7a))
* make skip auth working on a direct navigation ([c99b448](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/c99b448b2d94f1b9fccd00458829a96a05699e0b))
* middleware for editting a followup is based on wrong param ([8c5ddfc](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8c5ddfc84d13b4672b8b4202f3f938a5e185f63f))
* nuxt config ([e4d05cf](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e4d05cf3dbd91bd38df0c775b1f39ff498d0e293))
* prevent crash when user is unknown ([d470b78](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d470b7845db6a26ec6a932a4c637d36c34cf1752))
* **prisma:** prisma client workaround ([b007d96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b007d96fa120d00fa9020d937c810e6bdf2f2204))
* reactivity issues and pagetopbar accessing user before authenticated ([4965040](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/4965040e6ad4fb35d98a957c6e86dbe6b657d629))
* redirect to the correct url after the token validation ([bf499ab](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bf499abc61b29ff1d5ca2394d81c4f46d83b5e46))
* **refresh-token:** avoid infinite loop ([8493bfe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8493bfeaa94727e78f183fc9df2023fcc4e0cc2f))
* relatives labels ([d922bc1](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/d922bc19cd326257b677721197b6113ec5c30271))
* required email alert on beneficiary creation ([2d06df4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2d06df4e46e68ce7ff979d60431604dd62a39c62))
* return button from beneficiary searchbar is wrong ([39464f8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/39464f8829eb8ea2107bfc5ea6fc93b1fef53dc7))
* **route:** missing route rename ([b3bcd67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b3bcd67ace2dadda1e2a68bc5b03ad022156bf76))
* sidenav ssr error ([6812fe0](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6812fe0b25bceeaccb3050df6d9617a15b763574))
* sort custom followup types in the get-structure  route ([e7b9278](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e7b92784e0074dcdcf405fa9ab6503197c80f264))
* sort followup types by name in followup pages ([e677075](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e67707521a7df06f25934df0184d6c892aaa03ec))
* sort legally not required followup types in the structure edition form ([2c19b3a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/2c19b3a1d6ed01227b1da8db052d87171dc9e162))
* sort mutation logs by date ([ac54f6a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/ac54f6a4f17826253830e35592e31a6b5c2c4166))
* stat access and requests ([0c2182a](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0c2182ad8520ac6ff09c2d600d172217565fd540))
* stats issues ([bf749b5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/bf749b59741afc472230dc85ffbfe070109aa8ee))
* structure type is missing in the user in the session ([3f07304](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/3f073047accaa3caeb83adac94c591834a71249c))
* **tax-household:** select a linked beneficiary is not updating the value ([8918aa5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8918aa533d0182ce84f72a5a1533f982b9b3c7c0))
* temporarly remove cgu middleware ([0b5718b](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0b5718bd6adfeb6f7702e7449328fb19d6c6fa9c))
* the user in the store has no structure id ([951b581](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/951b5814c42a511f4bf6f405fc698e21ba95b6e4))
* ts issue ([72741d7](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/72741d7796ab718e51c612377f5ad2757fc0855d))
* ts issues ([abb4211](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/abb421194772279970ecc1681a487667bd786aee))
* ts issues ([04e8412](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/04e8412cdf2b16d53917684bc6d5ee0b4f1dee06))
* type ([e393759](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/e39375962996c0214d377e1478f6552ce65cf515))
* type import issue ([7475be2](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7475be22817af4161d6082f1f7e483e02400108e))
* unwanted horizontal scrollbar on all pages ([7922d5e](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/7922d5e64be4ab331aa5e2c449ad529c2e55953b))
* unwanted notification on history items and documents ([8f72255](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8f722553e44ddd89853f2a9b59a1003feab064ae))
* useless first level in the breadcrumb ([a74bee5](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/a74bee589720cc93004b6286b8359c7a74a8e23d))
* user is still considered as login after inactive logout ([b8b6ea8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/b8b6ea800160b96d12d29de71d04349a87c39537))
* users activicty not initialising properly when directly accessing the page ([6b29e96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/6b29e969edf274dc2f5f0188c8899a419b1d85fa))
* users that can not access an added document are notified ([0f6447c](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/0f6447cfdf6c3671e6342be2b8ee1f88ed7888db))
* **validate-token:** validate token only client side ([064e0fe](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/064e0fe57df2cbc9f3dc0396f75ada7aaf081db5))
* wrong activity type ([8d83376](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2/commit/8d833764465c433a5d883048667c4628bebe90bd))

import { test as setup, Page } from '@playwright/test'

async function authenticate(
  page: Page,
  username: string,
  password: string,
  newUser = false
) {
  await page.goto('/auth/login')
  await page
    .getByRole('link', { name: /s'identifier avec proconnect/i })
    .click()

  // Auth system redirection : ProConnect
  await page.waitForURL(/dev-agentconnect.fr/i, { waitUntil: 'networkidle' })
  await page.getByLabel('Email professionnel').fill(username)
  await page.getByRole('button', { name: /^continuer$/i }).click()

  // Identity provider redirection : Proconnect Identité
  await page.waitForURL(/identite-sandbox.proconnect.gouv.fr/i)
  await page.getByLabel('Renseignez votre mot de passe').fill(password)
  await page.getByRole('button', { name: /identifier/i }).click()

  // Sometimes login flow sets cookies in the process of several redirects.
  // Wait for the final URL to ensure that the cookies are actually set.
  await page.waitForURL(newUser ? /\/cgu/ : /\/app\/overview/)
}

setup.slow()

setup('authenticate as Structure Manager Jeanne', async ({ page }) => {
  const managerFile = 'playwright/.auth/structure-manager.json'
  const username = process.env.STRUCTURE_MANAGER_EMAIL || ''
  const pwd = process.env.STRUCTURE_MANAGER_PASSWORD || ''

  await authenticate(page, username, pwd)
  await page.context().storageState({ path: managerFile })
})

setup('authenticate as Social Worker Jean', async ({ page }) => {
  const workerFile = 'playwright/.auth/social-worker.json'
  const username = process.env.SOCIAL_WORKER_EMAIL || ''
  const pwd = process.env.SOCIAL_WORKER_PASSWORD || ''

  await authenticate(page, username, pwd)
  await page.context().storageState({ path: workerFile })
})

setup('authenticate as Reception Agent Jean', async ({ page }) => {
  const agentFile = 'playwright/.auth/reception-agent.json'
  const username = process.env.RECEPTION_AGENT_EMAIL || ''
  const pwd = process.env.RECEPTION_AGENT_PASSWORD || ''

  await authenticate(page, username, pwd)
  await page.context().storageState({ path: agentFile })
})

setup('authenticate as Structure Manager Anita', async ({ page }) => {
  const managerFile = 'playwright/.auth/structure-manager-new-user.json'
  const username = process.env.STRUCTURE_MANAGER_NEW_USER_EMAIL || ''
  const pwd = process.env.STRUCTURE_MANAGER_NEW_USER_PASSWORD || ''
  const newUser = true

  await authenticate(page, username, pwd, newUser)
  await page.context().storageState({ path: managerFile })
})

setup('authenticate as Social Worker Mehdi', async ({ page }) => {
  const workerFile = 'playwright/.auth/social-worker-new-user.json'
  const username = process.env.SOCIAL_WORKER_NEW_USER_EMAIL || ''
  const pwd = process.env.SOCIAL_WORKER_NEW_USER_PASSWORD || ''
  const newUser = true

  await authenticate(page, username, pwd, newUser)
  await page.context().storageState({ path: workerFile })
})

setup('authenticate as Structure Manager Ministère Laura', async ({ page }) => {
  const managerFile = 'playwright/.auth/structure-manager-ministere.json'
  const username = process.env.STRUCTURE_MANAGER_MINISTERE_EMAIL || ''
  const pwd = process.env.STRUCTURE_MANAGER_MINISTERE_PASSWORD || ''

  await authenticate(page, username, pwd)
  await page.context().storageState({ path: managerFile })
})

import type {
  SecurityRuleGrantee,
  SecurityTargetWithStructure,
  SecurityTargetWithReferents,
  SecurityRule,
  SecurityRuleContext,
  SecurityTargetWithCreator
} from './types'
import { isReferent, isCreator } from './helpers'
export { isReferent, isCreator }

export type {
  SecurityRuleGrantee,
  SecurityTargetWithStructure,
  SecurityRuleContext,
  SecurityTargetWithReferents,
  SecurityRule,
  SecurityTargetWithCreator
}

import { createQuery } from '~/server/route'
import { ExportFollowupsInput, exportFollowupsSchema } from '~/server/schema'
import {
  SecurityRuleContext,
  getAppContextPermissions
} from '~/server/security'
import { ProtectedAppContext } from '~/server/trpc'
import { createExport } from '~/utils/export/createExport'
import { FollowupService } from '~/server/services'

const handler = async ({
  input,
  ctx
}: {
  input: ExportFollowupsInput
  ctx: ProtectedAppContext
}) => {
  const { rows, columns } = await FollowupService.exportFollowups({
    ctx,
    input
  })
  // Forced to return base64 encoded version of the file in tRPC
  // Cf. https://github.com/trpc/trpc/discussions/658
  // & https://stackoverflow.com/questions/73715285/exceljs-download-xlsx-file-with-trpc-router
  const stream = await createExport(ctx.user.id, [
    { name: 'Entretiens', columns, rows }
  ])
  return { data: stream.toString('base64') }
}

const securityCheck = (ctx: SecurityRuleContext) =>
  getAppContextPermissions(ctx).export.history

export const getExport = createQuery({
  inputValidation: exportFollowupsSchema,
  securityCheck,
  handler
})

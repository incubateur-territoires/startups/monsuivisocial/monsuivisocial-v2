import { planAppointment } from './plan-appointment.mutation'
import { getAppointments } from './get-appointments.query'

export const rdvspRoutes = {
  planAppointment,
  getAppointments
}

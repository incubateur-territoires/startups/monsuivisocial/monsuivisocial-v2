import { getBeneficiaryCreationPermissionsQuery } from './get-beneficiary-creation-permissions.query'

export const permissionsRoutes = {
  getBeneficiaryCreationPermissionsQuery
}

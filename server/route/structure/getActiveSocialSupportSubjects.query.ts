import { createQuery } from '~/server/route'
import { ProtectedAppContext } from '~/server/trpc'
import { AllowSecurityCheck } from '~/server/security/rules/types'
import { emptySchema } from '~/server/schema'
import { StructureService } from '~/server/services'

const handler = async ({ ctx }: { ctx: ProtectedAppContext }) => {
  return await StructureService.getActiveSocialSupportSubjects({ ctx })
}

export const getActiveSocialSupportSubjects = createQuery({
  handler,
  securityCheck: AllowSecurityCheck,
  inputValidation: emptySchema
})

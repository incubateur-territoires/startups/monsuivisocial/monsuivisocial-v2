import { createMutation } from '~/server/route'
import { AdminContext } from '~/server/trpc'
import {
  updateAdminStructureSubjectsSchema,
  UpdateAdminStructureSubjectsInput
} from '~/server/schema'
import { StructureRepo } from '~/server/database'
import { StructureService } from '~/server/services'
import { structureContextQuery } from '~/server/trpc/createContext'

const adminHandler = async ({
  input,
  ctx
}: {
  input: UpdateAdminStructureSubjectsInput
  ctx: AdminContext
}) => {
  const structure = await StructureRepo.findUniqueOrThrow(ctx.user, {
    where: {
      id: input.structureId
    },
    select: structureContextQuery().select
  })
  return StructureService.updateSocialSupportSubjects({
    input,
    ctx: {
      user: ctx.user,
      structure
    }
  })
}

export const updateSocialSupportSubjects = createMutation({
  adminHandler,
  inputValidation: updateAdminStructureSubjectsSchema
})

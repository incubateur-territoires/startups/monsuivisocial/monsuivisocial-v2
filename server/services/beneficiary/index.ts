import { create } from './create.service'
import { get } from './get.service'
import { getPage } from './getPage.service'

import { updateTaxHousehold } from './updateTaxHousehold.service'
import { exportBeneficiaries } from './export.service'
import { exportBeneficiary } from './exportBeneficiary.service'
import { search } from './search.service'
import { getSimilar } from './getSimilar.service'
import {
  getBeneficiarySecurityTarget,
  getEditionPermissions,
  BeneficiarySecurityTarget,
  getArchivePermissions
} from './permission.service'
import { getIdFromFamilyFileId } from './get-id-from-family-file-id.service'
import { createFamilyFileMember } from './create-family-file-member.service'
import { updateEntourage } from './update-entourage.service'
import { getLastActivity } from './get-last-activity.service'

// TODO(foyer): SHOULD BE DELETED
export const BeneficiaryService = {
  create,
  get,
  getPage,
  updateTaxHousehold,
  exportBeneficiaries,
  exportBeneficiary,
  search,
  getSimilar,
  getEditionPermissions,
  getArchivePermissions,
  getBeneficiarySecurityTarget,
  getIdFromFamilyFileId,
  createFamilyFileMember,
  updateEntourage,
  getLastActivity
}

export type { BeneficiarySecurityTarget }

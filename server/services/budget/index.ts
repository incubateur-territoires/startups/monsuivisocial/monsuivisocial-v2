import { updateInfo } from './updateInfo.service'
import { updateExpenses } from './updateExpenses.service'
import { updateIncome } from './updateIncome.service'
import { deleteIncome } from './deleteIncome.service'
import { getIncome } from './getIncome.service'
import { getExpenses } from './getExpenses.service'
import { getInfo } from './getInfo.service'
import { getBudget } from './getBudget.service'
import { createBudgetIncome } from './createBudgetIncome.service'

export const BudgetService = {
  updateInfo,
  updateExpenses,
  updateIncome,
  deleteIncome,
  getIncome,
  getExpenses,
  getInfo,
  getBudget,
  createBudgetIncome
}

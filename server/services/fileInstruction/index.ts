import { getPage } from './getPage.service'
import { exportFileInstructions } from './export.service'
import { getOngoing } from './getOngoing.service'

export const FileInstructionService = {
  getPage,
  exportFileInstructions,
  getOngoing
}

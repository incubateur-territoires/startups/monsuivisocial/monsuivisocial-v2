import {
  FollowupMedium,
  NotificationType,
  SocialSupportType
} from '@prisma/client'
import { SocialSupportSubjectService } from '..'
import { prepareSocialSupportSubjectsMutations } from './helper'
import { FamilyFileRepo, SocialSupportRepo } from '~/server/database'
import { AddFollowupInput } from '~/server/schema'
import { SecurityRuleContext } from '~/server/security'
import { connect, prepareDocumentsMutations } from '~/utils/prisma'

type CreateParams = {
  ctx: SecurityRuleContext
  input: AddFollowupInput
}

export const create = async ({ ctx, input }: CreateParams) => {
  const { structure, user } = ctx
  const {
    familyFileId,
    beneficiaryId,
    subjects,
    documents,
    medium,
    place,
    redirected,
    structureName,
    thirdPersonName,
    prescribingOrganizationId,
    prescribingOrganizationContactId,
    ministre,
    date,
    synthesis,
    synthesisRichText,
    privateSynthesis,
    privateSynthesisRichText,
    status,
    helpRequested,
    dueDate,
    firstFollowup,
    numeroPegase,
    classified,
    forwardedToJustice,
    duration
  } = input

  let prescribingOrganization
  if (prescribingOrganizationId) {
    prescribingOrganization = connect(prescribingOrganizationId)
  }
  let prescribingOrganizationContact
  if (prescribingOrganizationContactId) {
    prescribingOrganizationContact = connect(prescribingOrganizationContactId)
  }

  const familyFile = await FamilyFileRepo.findUniqueOrThrow(user, {
    where: { id: familyFileId },
    include: { referents: true }
  })

  const fu = await SocialSupportRepo.prisma.create({
    data: {
      status,
      socialSupportType: SocialSupportType.Followup,
      date,
      structure: connect(structure.id),
      familyFile: connect(familyFileId),
      beneficiary: connect(beneficiaryId),
      createdBy: connect(user.id),
      subjects: prepareSocialSupportSubjectsMutations(subjects),
      documents: prepareDocumentsMutations(documents),
      notifications: {
        createMany: {
          data: familyFile.referents
            .filter(referent => referent.id !== user.id)
            .map(referent => ({
              beneficiaryId,
              recipientId: referent.id,
              type: NotificationType.NewFollowupElement,
              itemCreatedById: user.id
            }))
        }
      },
      ministre,
      numeroPegase,
      synthesis,
      synthesisRichText,
      privateSynthesis,
      privateSynthesisRichText,
      dueDate,
      followup: {
        create: {
          prescribingOrganization,
          prescribingOrganizationContact,
          redirected,
          structureName: redirected ? structureName : null,
          medium,
          place: medium === FollowupMedium.ExternalAppointment ? place : null,
          thirdPersonName:
            medium === FollowupMedium.ThirdParty ? thirdPersonName : null,
          helpRequested,
          firstFollowup,
          classified,
          forwardedToJustice,
          duration
        }
      }
    },
    include: {
      familyFile: {
        select: {
          id: true
        }
      }
    }
  })

  await SocialSupportSubjectService.markUsed(subjects)

  return fu
}

import { SocialSupportRepo } from '~/server/database'
import { SecurityRuleGrantee, SecurityRuleContext } from '~/server/security'
import { getSocialSupportPermissions } from '~/server/security/permissions'

export function getSocialSupportSecurityTarget(
  user: SecurityRuleGrantee,
  id: string
) {
  return SocialSupportRepo.findUniqueOrThrow(user, {
    where: { id },
    include: {
      familyFile: {
        include: {
          referents: true
        }
      }
    }
  })
}

export async function getEditionPermissions(
  ctx: SecurityRuleContext,
  id: string
) {
  const socialSupport = await getSocialSupportSecurityTarget(ctx.user, id)
  return getSocialSupportPermissions({
    ctx,
    familyFile: socialSupport.familyFile,
    socialSupport
  })
}

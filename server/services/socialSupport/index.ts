import { getEditionPermissions } from './permission.service'

export const SocialSupportService = {
  getEditionPermissions
}

import { update } from './update.service'
import { create } from './create.service'
import { remove } from './remove.service'
import { makeActive } from './makeActive.service'

export const PartnerOrganizationService = {
  update,
  create,
  remove,
  makeActive
}

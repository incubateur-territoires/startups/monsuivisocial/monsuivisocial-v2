import { updateSocialSupportSubjects } from './updateSocialSupportSubjects.service'
import { getSocialSupportSubjects } from './getSocialSupportSubjects.service'
import { getActiveSocialSupportSubjects } from './getActiveSocialSupportSubjects.service'
import { getAdminSocialSupportSubjects } from './getAdminSocialSupportSubjects.service'
import { getActiveOrUsedSocialSupportSubjects } from './getActiveOrUsedSocialSupportSubjects.service'

export const StructureService = {
  updateSocialSupportSubjects,
  getSocialSupportSubjects,
  getActiveSocialSupportSubjects,
  getActiveOrUsedSocialSupportSubjects,
  getAdminSocialSupportSubjects
}

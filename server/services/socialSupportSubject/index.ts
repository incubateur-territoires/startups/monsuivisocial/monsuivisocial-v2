import { update } from './update.service'
import { makeInactive } from './makeInactive.service'
import { markUsed } from './markUsed.service'

export const SocialSupportSubjectService = {
  update,
  makeInactive,
  markUsed
}

import { JobResult, dataInclusionStructureServerSchema } from '~/server/schema'
import { downloadStructures } from '~/server/lib/data-inclusion'
import { DataInclusionService, JobResultService } from '~/server/services'

async function importStructures(job: JobResult) {
  const res = await downloadStructures()

  if (res.error !== null) {
    job.errors.push({
      category: 'structure-import-download',
      status: res.error.status,
      error: res.error.msg
    })

    return job
  }

  if (Array.isArray(res.data)) {
    job.result.nTotal = res.data.length
  } else {
    job.errors.push({
      category: 'structure-import-download',
      status: null,
      error: `Data inclusion structure data is badly formatted: should be an array`
    })

    return job
  }

  job.result.nCreated = 0
  job.result.nUpdated = 0
  job.result.nImported = 0

  for (let i = 0; i < res.data.length; i++) {
    const input = res.data[i]

    const validated = dataInclusionStructureServerSchema.safeParse(input)

    if (validated.success === false) {
      job.errors.push({
        category: 'structure-import-format',
        status: null,
        error: `structure ${input.id}: ${validated.error.toString()}`
      })

      continue
    }

    const structure = validated.data

    let exists: boolean
    let shouldBeUpdated: boolean

    try {
      const status = await DataInclusionService.getStatus(
        structure.id,
        structure.updated
      )
      exists = status.exists
      shouldBeUpdated = status.shouldBeUpdated || false
    } catch (err: unknown) {
      job.errors.push({
        category: 'structure-import-other',
        status: null,
        error: `could not check update status for id ${structure.id}, name ${structure.name}: ${err}`
      })
      continue
    }

    const version = res.headers.get('location') || 'unknown'

    if (!exists) {
      try {
        await DataInclusionService.create(structure, version)
        job.result.nCreated++
        job.result.nImported++
      } catch (err: unknown) {
        job.errors.push({
          category: 'structure-import-create',
          status: null,
          error: `in create ${structure.id}: ${err}`
        })
      }
    } else if (shouldBeUpdated) {
      try {
        await DataInclusionService.update(structure, version)
        job.result.nUpdated++
        job.result.nImported++
      } catch (err: unknown) {
        job.errors.push({
          category: 'structure-import-update',
          status: null,
          error: `in update ${structure.id}: ${err}`
        })
      }
    }

    if (i % 1000 === 0) {
      job.status = 'Ongoing'

      await JobResultService.update({
        id: job.id,
        status: job.status,
        errors: job.errors,
        result: job.result
      })
    }
  }

  const count = await DataInclusionService.count()
  job.result.nInDatabase = count

  return job
}

async function handleStructuresImport(iscron = false) {
  const job = await JobResultService.create({
    name: iscron
      ? 'data-inclusion-structure-import-cron'
      : 'data-inclusion-structure-import',
    errors: [],
    result: {
      nTotal: 0,
      nInDatabase: 0,
      nImported: 0,
      nCreated: 0,
      nUpdated: 0
    }
  })

  try {
    await DataInclusionIntegrationService.importStructures(job)
  } catch (err: unknown) {
    job.errors.push({
      category: 'structure-import-other',
      status: null,
      error: `${err}`
    })
  }

  const endDate = new Date()
  const durationInSeconds = (endDate.getTime() - job.startDate.getTime()) / 1000

  job.endDate = endDate
  job.durationInSeconds = durationInSeconds
  job.status = job.errors.length > 0 ? 'Failure' : 'Success'

  return await JobResultService.update({
    id: job.id,
    status: job.status,
    endDate: job.endDate,
    durationInSeconds: job.durationInSeconds,
    errors: job.errors,
    result: job.result
  })
}

export const DataInclusionIntegrationService = {
  importStructures
}

export const DataInclusionIntegrationHandler = {
  handleStructuresImport
}

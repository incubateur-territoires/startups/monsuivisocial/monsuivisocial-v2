export const dataInclusionStructures = [
  {
    accessibilite: null,
    adresse_id: '1',
    antenne: null,
    horaires_ouverture:
      ' Lundi : 9h-12h30 Mardi et vendredi : 9h-12h30 et 14h-18h Samedi : 9h-12h ',
    id: '1',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1-mairie-davailles-sur-seiche',
    nom: "Mairie d'Availles Sur Seiche",
    rna: null,
    siret: null,
    site_web: null,
    source: 'cd35',
    typologie: 'MUNI',
    presentation_resume:
      'Une commune est une collectivité locale gérée de manière autonome par le Maire. Il s’appuie sur ses adjoints et ses conseillers municipaux.\r\n \nLes rôles du Maire et de son équipe sont définis par le Code Général des Collectivités Territoriales et les lois de transferts de compét…',
    date_maj: 1593734400000,
    presentation_detail:
      "Une commune est une collectivité locale gérée de manière autonome par le Maire. Il s’appuie sur ses adjoints et ses conseillers municipaux.\r\n \nLes rôles du Maire et de son équipe sont définis par le Code Général des Collectivités Territoriales et les lois de transferts de compétences.\r\n \nLes compétences : \nLe champ d’intervention de la commune est très vaste. Outre la gestion de son domaine public, elle sert d’intermédiaire entre l’État et les citoyens.\r\n \nLes responsabilités locales : \nLa commune est autonome pour de nombreuses actions :\n\nEn matière d’urbanisme, elle contrôle et planifie son urbanisme à l’aide du PLU (plan local d’urbanisme) et délivre les autorisations de construire,\nDans le domaine sanitaire et social, la commune met en œuvre l’action sociale facultative grâce aux centres communaux d’action sociale (CCAS : gestion des crèches, des foyers de personnes âgées),\nDans le domaine de l’enseignement, la commune a en charge les écoles préélémentaires et élémentaires (création et implantation, gestion et financement, à l’exception de la rémunération des enseignants),\nDans le domaine culturel, la commune crée et entretient des bibliothèques, musées, écoles de musique, salles de spectacle. Elle organise des manifestations culturelles,\nDans le domaine sportif et des loisirs, la commune crée et gère des équipements sportifs, elle subventionne des activités sportives, y compris les clubs sportifs professionnels, elle est en charge des aménagements touristiques.\nEntretien de la voirie communale,\nProtection de l’ordre public local par le biais du pouvoir de police du maire.\r\n\t \n\nLa représentation de l’Etat : \nLes maires et les adjoints accomplissent également des missions au nom de l’État, grâce aux moyens et aux personnels de la commune :\n\nétat civil (enregistrement des naissances, mariages et décès),\nfonctions électorales (organisation des élections…),\nrecensement de la population française (organisé par l' INSEE ),\nElle est aussi chef de file pour fixer les modalités de l’action commune des collectivités territoriales et de leurs établissements publics pour l’exercice des compétences relatives à la mobilité durable,\nl’organisation des services publics de proximité,\nl’aménagement de l’espace et le développement local,\n",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1',
    _di_adresse_surrogate_id: 'cd35-1',
    longitude: -1.1955707073,
    latitude: 47.9610939026,
    complement_adresse: null,
    commune: 'AVAILLES-SUR-SEICHE',
    adresse: '3 rue des Fontaines',
    code_postal: '35130',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'availles.mairie@wanadoo.fr',
    telephone: '0299962292'
  },
  {
    accessibilite: null,
    adresse_id: '10',
    antenne: null,
    horaires_ouverture: null,
    id: '10',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/10-association-hemochromatose-ouest-rennes',
    nom: 'Association Hémochromatose Ouest  - Rennes',
    rna: null,
    siret: null,
    site_web: null,
    source: 'cd35',
    typologie: null,
    presentation_resume:
      " \nL'hémochromatose est la première maladie génétique et familiale en France. Il s'agit d'une maladie mal connue et dont on parle peu. Un service de dépistage est situé à Rennes, Hôpital Pontchaillou : 2, rue Henri Le Guillou, service maladies du foie, tél. 02 99 28 42 97.\nAHO es…",
    date_maj: 1580947200000,
    presentation_detail:
      " \nL'hémochromatose est la première maladie génétique et familiale en France. Il s'agit d'une maladie mal connue et dont on parle peu. Un service de dépistage est situé à Rennes, Hôpital Pontchaillou : 2, rue Henri Le Guillou, service maladies du foie, tél. 02 99 28 42 97.\nAHO est une association de personnes attenintes d'hémochromatose, pour et au service des malades.\nTouchés par la pathologie, les adhérents sont engagés bénévolement pour :\n- Informer, Sensibiliser, Soutenir.\nAssociation adhérente à la MAS (maison associative de la santé - tél. 02 99 53 48 82) et au CISS Bretagne (Collectif interassociatif sur la santé - tél 02 99 53 56 79 - www.lecissbretagne.org).\n ",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-10',
    _di_adresse_surrogate_id: 'cd35-10',
    longitude: -1.6923201395,
    latitude: 48.119365077,
    complement_adresse: null,
    commune: 'RENNES',
    adresse: '2  rue Henri le Guilloux',
    code_postal: '35033',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'president.aho@hotmail.fr',
    telephone: '0299284321'
  },
  {
    accessibilite: null,
    adresse_id: '1001',
    antenne: null,
    horaires_ouverture: 'cf :  renseignements complémentaires',
    id: '1001',
    lien_source: 'https://annuaire.ille-et-vilaine.fr/organisme/1001-city-roul',
    nom: 'City-roul',
    rna: null,
    siret: null,
    site_web: 'http://www.cityroul.com ',
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "City-roul propose de partager l'usage de véhicules, moyennant un abonnement et un prix à l'heure.City-roul gère une flotte de véhicules disponibles sur courte durée, 24 heures sur 24 et 7 jours sur 7. Les abonnés réservent facilement et disposent d'un véhicule stationné sur un p…",
    date_maj: 1546300800000,
    presentation_detail:
      "City-roul propose de partager l'usage de véhicules, moyennant un abonnement et un prix à l'heure.City-roul gère une flotte de véhicules disponibles sur courte durée, 24 heures sur 24 et 7 jours sur 7. Les abonnés réservent facilement et disposent d'un véhicule stationné sur un parking à proximité de chez eux (Colombier, Hoche, gare sud, mail François Mitterand, centre commercial Beauregard, place Bernanos, Chézy-Dinan et sergent-Maginot...). Possibilité de louer à l'heure, à la journée. Depuis mars 2014, possibilité de s'abonner en ligne.Joindre en priorité par téléphone ou par mail.HORAIRES D’OUVERTURE– du lundi au vendredi : 9h-18h30 (en continu)– le samedi : de 10h à 12h30 et de 13h30 à 17hHORAIRES ESTIVALES (du 09/07 au 31/08/2018)– du lundi au jeudi : de 9h à 12h30 et de 14h à 18h – le vendredi : de 9h à 12h30 et de 14h à 17h30– le samedi : fermé FERMETURES EXCEPTIONNELLES– lundi 25 juin de 12h30 à 14h – mercredi 27 juin de 12h30 à 13h30– vendredi 29 juin de 12h30 à 13h30– lundi 2 juillet de 12h à 13h30– samedi 7 juillet, toute la journée – mercredi 15 août, toute la journée ",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1001',
    _di_adresse_surrogate_id: 'cd35-1001',
    longitude: -1.6786071644,
    latitude: 48.104988334,
    complement_adresse: null,
    commune: 'RENNES',
    adresse: '40  place du Colombier',
    code_postal: '35000',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'info@cityroul.com',
    telephone: '0223210747'
  },
  {
    accessibilite: null,
    adresse_id: '1002',
    antenne: null,
    horaires_ouverture: null,
    id: '1002',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1002-service-social-la-poste-rennes',
    nom: 'Service social la Poste - Rennes',
    rna: null,
    siret: null,
    site_web: null,
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "Service social d'entreprise  au service des salariés actifs, des fonctionnaires et retraités de la poste.\nPrendre rendez-vous en fonction du service ou laisser un message.(pas de secrétariat)\nservice social courrier : 27, boulevard du Colombier / 02.22.93.79.01 et  02.22.93.79.0…",
    date_maj: 1580947200000,
    presentation_detail:
      "Service social d'entreprise  au service des salariés actifs, des fonctionnaires et retraités de la poste.\nPrendre rendez-vous en fonction du service ou laisser un message.(pas de secrétariat)\nservice social courrier : 27, boulevard du Colombier / 02.22.93.79.01 et  02.22.93.79.02\nservice social réseau : 27, boulevard du Colombier / 02 .99. 65. 72. 57\nservice social financier : 11, rue Vanneau / 02. 99. 02. 65 .16",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1002',
    _di_adresse_surrogate_id: 'cd35-1002',
    longitude: -1.6775775645,
    latitude: 48.1032408112,
    complement_adresse: null,
    commune: 'RENNES',
    adresse: '27  boulevard du Colombier',
    code_postal: '35032',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: null,
    telephone: null
  },
  {
    accessibilite: null,
    adresse_id: '1003',
    antenne: null,
    horaires_ouverture: 'lundi au vendredi : 9h-12h, 14h-17h.',
    id: '1003',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1003-service-social-maritime-nantes',
    nom: 'Service Social Maritime - Nantes',
    rna: null,
    siret: null,
    site_web: 'http://www.ssm-mer.fr',
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "Service social d'entreprise au service des marins, actifs ou pensionnés et de leurs familles (marins-pêcheurs et marins du commerce).\nEn Ille-et-Vilaine, prendre rendez-vous auprès de la direction des affaires maritimes, 27, quai Duguay Trouin, BP 70, 35406 Saint Malo, tél. 02 9…",
    date_maj: 1580947200000,
    presentation_detail:
      "Service social d'entreprise au service des marins, actifs ou pensionnés et de leurs familles (marins-pêcheurs et marins du commerce).\nEn Ille-et-Vilaine, prendre rendez-vous auprès de la direction des affaires maritimes, 27, quai Duguay Trouin, BP 70, 35406 Saint Malo, tél. 02 99 40 31 50.",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1003',
    _di_adresse_surrogate_id: 'cd35-1003',
    longitude: -1.5642358241,
    latitude: 47.2100428189,
    complement_adresse: null,
    commune: 'NANTES',
    adresse: '54  quai de la Fosse',
    code_postal: '44000',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'direction@ssm-mer.fr',
    telephone: '0240710150'
  },
  {
    accessibilite: null,
    adresse_id: '1004',
    antenne: null,
    horaires_ouverture:
      'permanence téléphonique du lundi au vendredi : 9h-12h, 14h-17h.',
    id: '1004',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1004-association-des-victimes-daccidents-de-la-circulation-dagressions-et-dactes-medicaux-cesson-sevigne',
    nom: "Association des victimes d'accidents de la circulation \" \" d'agressions et d'actes médicaux - Cesson Sévigné",
    rna: null,
    siret: null,
    site_web: null,
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "Association créée par des bénévoles afin de soutenir, accompagner, informer, conseiller les victimes d'accidents de la circulation, d'agressions, d'actes médicaux dans leurs démarches administratives et d'indemnisation.\nEquipe de juristes et de médecins.",
    date_maj: 1628726400000,
    presentation_detail: null,
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1004',
    _di_adresse_surrogate_id: 'cd35-1004',
    longitude: -1.5880947,
    latitude: 48.1144021,
    complement_adresse: null,
    commune: 'CESSON-SEVIGNE',
    adresse: 'La Rigourdière',
    code_postal: '35510',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'aviacaam@hotmail.com',
    telephone: '0633422908'
  },
  {
    accessibilite: null,
    adresse_id: '1005',
    antenne: null,
    horaires_ouverture:
      'de 7h30 à 18h du lundi au vendredi sauf jours fériés et vacances scolaires',
    id: '1005',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1005-sos-urgences-mamans-antenne-de-rennes',
    nom: 'SOS Urgences Mamans " " antenne de Rennes.',
    rna: null,
    siret: null,
    site_web: 'http://www.sosurgencesmamans.com',
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "SOS Urgences Maman propose une garde d'enfant immédiate, occassionnelle et temporaire, afin de venir en aide à des parents confrontés à un problème inattendu. SOS Urgences Mamans ne fontionne ni le week-end ni pendant les vacances scolaires.\nGarde d'enfants de tous âges, en dépa…",
    date_maj: 1609113600000,
    presentation_detail:
      "SOS Urgences Maman propose une garde d'enfant immédiate, occassionnelle et temporaire, afin de venir en aide à des parents confrontés à un problème inattendu. SOS Urgences Mamans ne fontionne ni le week-end ni pendant les vacances scolaires.\nGarde d'enfants de tous âges, en dépannage, au domicile des parents ou de la personne bénévole dans l'agglomération rennaise.\nDes bénévoles se relaient sur Rennes et sa périphérie pour dépanner les parents pris au dépourvu.\nPas d'incription préalable, seule une participation des parents est demandée pour couvrir les frais de déplacements des bénévoles.",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1005',
    _di_adresse_surrogate_id: 'cd35-1005',
    longitude: -1.6719683505,
    latitude: 48.0874468389,
    complement_adresse: null,
    commune: 'RENNES',
    adresse: '187  Rue de Châtillon',
    code_postal: '35000',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: null,
    telephone: '0299305200'
  },
  {
    accessibilite: null,
    adresse_id: '1006',
    antenne: null,
    horaires_ouverture: 'du lundi au vendredi : 9 h-18h',
    id: '1006',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1006-info-sociale-en-ligne-rennes',
    nom: 'Info sociale en ligne - Rennes',
    rna: null,
    siret: null,
    site_web: 'http://annuaire.ille-et-vilaine.fr',
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "Le Département d'Ille-et-Vilaine met à votre disposition un service d'écoute et d'information, Info sociale en ligne, qui peut répondre à toutes questions d'accès aux droits et vous aider dans les démarches (trouver le bon guichet, aider à remplir un dossier, etc...)\nTout citoye…",
    date_maj: 1610064000000,
    presentation_detail:
      "Le Département d'Ille-et-Vilaine met à votre disposition un service d'écoute et d'information, Info sociale en ligne, qui peut répondre à toutes questions d'accès aux droits et vous aider dans les démarches (trouver le bon guichet, aider à remplir un dossier, etc...)\nTout citoyen peut prétendre, selon sa situation, à un certain nombre de droits prévus par la loi : droit à la Sécurité sociale et à une complémentaire santé, droit au logement, prestations familiales, prestations d'aide sociale, allocations chômage, etc.\nISL peut aider les personnes les plus en difficulté à faire valoir leurs droits avant de solliciter d'autres dispositifs sociaux plus spécifiques. Un objectif qui s'inscrit dans les politiques de lutte contre la pauvreté et l'inclusion sociale.\nLes situations de rupture sont très souvent à l'origine de la prise de contact avec ISL : rupture de communication avec un enfant, divorce, séparation, perte d'emploi, rupture dans le domaine de la santé : maladie invalidante, accident du travail...\nInfo sociale en ligne  est également un service ressource pour les professionnels qui garantit une veille juridique  et administrative.\nService gratuit et anonyme.",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1006',
    _di_adresse_surrogate_id: 'cd35-1006',
    longitude: -1.6933435423,
    latitude: 48.1271541422,
    complement_adresse: null,
    commune: 'RENNES',
    adresse: null,
    code_postal: '35000',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'isl@ille-et-vilaine.fr',
    telephone: '0800953545'
  },
  {
    accessibilite: null,
    adresse_id: '1007',
    antenne: null,
    horaires_ouverture: ' mardi, jeudi et vendredi : 9h-12h,  Sur rendez vous ',
    id: '1007',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1007-la-rencontre-association-departementale-dentraide-des-personnes-accueillies-a-laide-sociale-a-lenfance-en-ille-et-vilaine-rennes',
    nom: "La Rencontre - Association Départementale d'Entraide des Personnes Accueillies à l'Aide sociale à  l'Enfance en Ille-et-Vilaine - Rennes",
    rna: null,
    siret: null,
    site_web: 'https://fnadepape.org/',
    source: 'cd35',
    typologie: null,
    presentation_resume:
      "Cette association a pour but de développer l'esprit de solidarité entre les personnes accueillies à la protection de l'enfance du département d'Ille-et-Vilaine (catégories définies dans les statuts de l'association), de leur venir en aide. Elle veut être pour tous une grande fam…",
    date_maj: 1711065600000,
    presentation_detail:
      "Cette association a pour but de développer l'esprit de solidarité entre les personnes accueillies à la protection de l'enfance du département d'Ille-et-Vilaine (catégories définies dans les statuts de l'association), de leur venir en aide. Elle veut être pour tous une grande famille.\nPeuvent y adhérer, tous ceux qui ont passé au moins un an sans interruption dans les services de la protection de l'enfance (DDASS ou Das 35) jusqu'à l'âge de 18 ans et les jeunes ayant bénéficié durant 3 ans, de 18 à 21 ans, sans interruption d'un contrat jeune majeur.\nActivités de l'association :\n\nattribution des aides suivantes : dot de mariage, prime de naissance, secours d'urgence, prêts d'honneur, aide aux malades hospitalisés, aide aux étudiants ;\naide à diverses démarches : logement, travail, relation avec divers organismes, recherche de solutions, soutien moral et financier dans le respect de l'indépendance de la personne.\n",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1007',
    _di_adresse_surrogate_id: 'cd35-1007',
    longitude: -1.687176,
    latitude: 48.121273,
    complement_adresse: null,
    commune: 'Rennes',
    adresse: '4 Rue Perrin de la Touche',
    code_postal: '35000',
    code_insee: '35238',
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: 'adepape35larencontre@orange.fr',
    telephone: '0299541864'
  },
  {
    accessibilite: null,
    adresse_id: '1008',
    antenne: null,
    horaires_ouverture: '8h-12h / 13h30-17h30',
    id: '1008',
    lien_source:
      'https://annuaire.ille-et-vilaine.fr/organisme/1008-association-letape-chantier-dinsertion-mordelles',
    nom: "Association l'Etape - Chantier d'insertion - Mordelles",
    rna: null,
    siret: null,
    site_web: null,
    source: 'cd35',
    typologie: null,
    presentation_resume:
      'Les Ateliers et Chantiers d’Insertion (ACI) sont des structures prestataires de biens et de services qui interviennent dans le champ non marchand. Ils accueillent un public particulièrement éloigné de l’emploi en proposant une première étape de réinsertion par le travail en offr…',
    date_maj: 1654128000000,
    presentation_detail:
      "Les Ateliers et Chantiers d’Insertion (ACI) sont des structures prestataires de biens et de services qui interviennent dans le champ non marchand. Ils accueillent un public particulièrement éloigné de l’emploi en proposant une première étape de réinsertion par le travail en offrant un accompagnement socio professionnel renforcé. Les activités de production proposées sont un support à l’insertion professionnelle.\nVous pouvez déposer votre candidature sur la plateforme de l’inclusion.\nEmbauchées en contrat à durée déterminée d’insertion (CDDI), les personnes bénéficient d’une durée de contrat de 6 mois renouvelable avec une durée maximale de 2 ans. La durée du travail est au minimum de 20 heures hebdomadaires.\nDomaines d'intervention : espaces verts\n\nEntretien et aménagement d'espaces verts communaux\nPetite maçonnerie paysagère\nEntretien patrimoine bâti\n\nTerritoire d'intervention Ouest de Rennes Métropole :\nL'Hermitage, Le Rheu, Mordelles, Chavagne, Cintré, La Chapelle Thouarault, Le Verger, Saint Thurial, Bréal-sous-Montfort.",
    labels_autres: null,
    labels_nationaux: null,
    thematiques: null,
    _di_surrogate_id: 'cd35-1008',
    _di_adresse_surrogate_id: 'cd35-1008',
    longitude: -1.831929,
    latitude: 48.082545,
    complement_adresse: null,
    commune: 'MORDELLES',
    adresse: 'Rue de la Croix Ignon',
    code_postal: '35310',
    code_insee: null,
    _di_geocodage_score: null,
    _di_geocodage_code_insee: null,
    _di_email_is_pii: false,
    courriel: null,
    telephone: '0299673907'
  }
]

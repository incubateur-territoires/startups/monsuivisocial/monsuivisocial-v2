import { RouterOutput } from '~/server/trpc/routers'

export type SplitOutput = RouterOutput['file']['split']

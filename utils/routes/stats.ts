import { AppRoute } from './utils'

export const routes = {
  AppStats: {
    name: 'AppStats',
    title: 'Statistiques',
    path: () => `${AppRoute}/stats`
  }
} as const

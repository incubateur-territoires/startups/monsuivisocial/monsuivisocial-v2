export enum BeneficiaryTab {
  Details = 'Details',
  Documents = 'Documents',
  History = 'History',
  Budget = 'Budget'
}

import { breadcrumbs as beneficiary } from './beneficiary'
import { breadcrumbs as budget } from './budget'
import { breadcrumbs as file } from './file'
import { breadcrumbs as followup } from './followup'
import { breadcrumbs as helpRequest } from './help-request'
import { breadcrumbs as stats } from './stats'
import { breadcrumbs as structure } from './structure'
import { breadcrumbs as user } from './user'

export const breadcrumbs = {
  beneficiary,
  budget,
  file,
  followup,
  helpRequest,
  stats,
  structure,
  user
}

export function getUserCreationMessage(email: string) {
  return `Le nouvel utilisateur ${email} a correctement été créé. Il recevra dans quelques instants un email l'invitant à créer son compte ProConnect. Pensez à l'informer de la réception de ce mail et de l'inviter si nécessaire à vérifier ses spams.`
}

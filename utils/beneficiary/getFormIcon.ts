export const getFormIcon = (nMembers: number) =>
  nMembers > 1 ? 'fr-icon-group-line' : 'fr-icon-user-line'
